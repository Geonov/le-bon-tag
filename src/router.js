import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login';
import Home from './views/Home';
import Map from './views/validation/Map';
//import MapConsult from './views/consult/Map';
import Users from './views/config/Users';
import Contributors from './views/config/Contributors';
import Business from './views/config/Business';
import Rights from './views/config/Rights';
import Themes from './views/config/Themes';
import Objects from './views/config/Objects';
import Footprints from './views/config/Footprints';
import Tags from './views/config/Tags';
import Tiles from './views/config/Tiles';
import Layers from './views/config/Layers';
import Wmts from './views/config/Wmts';
import RattachmentAdmin from './views/config/Rattachment';
import RattachmentAdminForm from './views/config/RattachmentForm';
import Settings from './views/config/Settings';
import About from './views/help/About';
import Wiki from './views/help/Wiki';
import License from './views/help/License';
import Export from './views/export/Export';
import Rattachment from './views/rattachment/Rattachment';
import RattachmentInverse from './views/rattachment/RattachmentInverse';
import { Role } from '@/helpers';
import { authenticationService } from '@/services';

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    // base: process.env.BASE_URL,
    routes: [
        {
            path: '/home',
            name: 'home',
            meta: {
                authorize: [],
            },
            component: Home,
        },
        {
            path: '/validation',
            name: 'map',
            meta: {
                authorize: [],
            },
            component: Map,
        },
        //{
        //    path: '/consult',
        //    name: 'mapconsult',
        //    meta: {
        //        authorize: [],
        //    },
        //    component: MapConsult,
        //},
        {
            path: '/rattachment',
            name: 'rattachment',
            meta: {
                authorize: [Role.rattacher, Role.Admin],
            },
            component: Rattachment,
        },
        {
            path: '/rattachmentinverse',
            name: 'rattachmentinverse',
            meta: {
                authorize: [],
            },
            component: RattachmentInverse,
        },
        {
            path: '/export',
            name: 'export',
            meta: {
                authorize: [],
            },
            component: Export,
        },
        {
            path: '/config/users',
            name: 'users',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Users,
        },
        {
            path: '/config/contributors',
            name: 'contributors',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Contributors,
        },
        {
            path: '/config/business',
            name: 'business',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Business,
        },
        {
            path: '/config/rights',
            name: 'rights',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Rights,
        },
        {
            path: '/config/themes',
            name: 'themes',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Themes,
        },
        {
            path: '/config/objects',
            name: 'objects',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Objects,
        },
        {
            path: '/config/footprints',
            name: 'footprints',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Footprints,
        },
        {
            path: '/config/tags',
            name: 'tags',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Tags,
        },
        {
            path: '/config/tiles',
            name: 'tiles',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Tiles,
        },
        {
            path: '/config/wms',
            name: 'layers',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Layers,
        },
        {
            path: '/config/wmts',
            name: 'wmts',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Wmts,
        },
        {
            path: '/config/rattachment',
            name: 'rattachmentconfig',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: RattachmentAdmin,
        },
        {
            path: '/config/rattachment/:form',
            name: 'rattachmentinsert',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: RattachmentAdminForm,
        },
        {
            path: '/config/rattachment/:form/:id',
            name: 'rattachmentedit',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: RattachmentAdminForm,
        },
        {
            path: '/config/rattachment/:form/',
            name: 'rattachmentduplicate',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: RattachmentAdminForm,
        },
        {
            path: '/config/settings',
            name: 'settings',
            meta: {
                layout: 'config',
                authorize: [Role.Admin],
            },
            component: Settings,
        },
        {
            path: '/config',
            redirect: '/config/users',
        },
        {
            path: '/help/about',
            name: 'about',
            meta: {
                layout: 'help',
                authorize: [],
            },
            component: About,
        },
        {
            path: '/help/wiki',
            name: 'wiki',
            meta: {
                layout: 'help',
                authorize: [],
            },
            component: Wiki,
        },
        {
            path: '/help/license',
            name: 'license',
            meta: {
                layout: 'help',
                authorize: [],
            },
            component: License,
        },
        {
            path: '/help',
            redirect: '/help/about',
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/ogr2ogr',
            meta: {
                authorize: [],
            },
        },
        // otherwise redirect to home
        { path: '*', redirect: '/home' },
    ],
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const { authorize } = to.meta;
    const currentUser = authenticationService.currentUserValue;

    if (authorize) {
        if (!currentUser) {
            // not logged in so redirect to login page with the return url
            return next({ path: '/login', query: { returnUrl: to.path } });
        }

        // check if route is restricted by role
        if (authorize.length && !authorize.includes(currentUser.role)) {
            // role not authorised so redirect to home page
            return next({ path: '/home' });
        }
    }
    next();
});
