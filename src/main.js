import Vue from 'vue';
import Vuelidate from 'vuelidate';
import App from './App.vue';
import { router } from './router';
import VueResource from 'vue-resource';

import HelpLayout from './layouts/Help.vue';
import ConfigLayout from './layouts/Config.vue';
import DefaultLayout from './layouts/Default.vue';

import { library } from '@fortawesome/fontawesome-svg-core';
// import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';
import { headers, handleResponse } from '@/helpers';

library.add(fas);

Vue.config.devtools = true;

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);

Vue.component('help_layout', HelpLayout);
Vue.component('config_layout', ConfigLayout);
Vue.component('default_layout', DefaultLayout);

Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.use(Vuelidate);

let handleOutsideClick;
Vue.directive('click-outside', {
    bind(el, binding, vnode) {
        handleOutsideClick = (e) => {
            e.stopPropagation();
            if (!el.contains(e.target)) {
                vnode.context['handleClickOutside']();
            }
        };
        document.addEventListener('click', handleOutsideClick);
    },
    unbind() {
        document.removeEventListener('click', handleOutsideClick);
    },
});

Vue.http.interceptors.push(function(request) {
    const params = headers().headers;
    // modify headers
    for (var key in params) {
        request.headers.set(key, params[key]);
    }

    // return response callback
    return (response) => {
        handleResponse(response);
    };
});

new Vue({
    http: {
        // root: `${process.env.BASE_URL}:${process.env.PORT}/`,
        // root: 'localhost:8081/',
    },
    router,
    render: (h) => h(App),
}).$mount('#app');
