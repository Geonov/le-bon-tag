import { handleResponse, requestOptions } from '@/helpers';

export const userService = {
    getAll,
    getById,
};

function getAll() {
    return fetch('/api/user', requestOptions.get()).then(handleResponse);
}

function getById(id) {
    return fetch(`/api/user/${id}`, requestOptions.get()).then(handleResponse);
}
