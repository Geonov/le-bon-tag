import { BehaviorSubject } from 'rxjs';

import { requestOptions, handleResponse } from '@/helpers';
import jwt from 'jwt-simple';

const USER_ITEM = 'osm-user';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem(USER_ITEM)));

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() {
        return currentUserSubject.value;
    },
};

function decoded_token(value) {
    if (!value) {
        return null;
    }
    // decode JWT without verify the signature
    return jwt.decode(value, null, true);
}

function login(login, password) {
    return fetch('/api/login', requestOptions.post({ login, password }))
        .then(handleResponse)
        .then((user) => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(USER_ITEM, JSON.stringify(user));
            currentUserSubject.next(user);
            const decoded = decoded_token(user.token);
            return decoded;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem(USER_ITEM);
    currentUserSubject.next(null);
}
