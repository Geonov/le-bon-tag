# Installation de l'application "Le Bon Tag" sur Debian 9 (MODE DEV)

## Pré-requis

L'application nécessite entre autres PostgreSQL, PostGIS, Python 3, node.js, npm.
Ce document décrit l'installation complète de l'application et de ses dépendances pour une utilisation en mode développement (depuis les sources) sous Debian.

## Mise à jour des paquets et du système

```
sudo apt-get update && sudo apt-get upgrade
```

## Clonage du dépôt git LeBonTag

```
sudo apt-get install -y git && sudo git config --global user.email "XXXX"
cd /opt && sudo git clone --branch latest https://gitlab.com/Geonov/le-bon-tag.git
```

Où XXXX est à remplacer par un email.  

## Proxy (optionnel)

Si votre organisme utilise un proxy, vous devez spécifier les variables d'environnement "http\_proxy" et "https\_proxy" afin que Python accède à Internet.

Fichier /etc/environment :

```
http_proxy=http://***:***
https_proxy=http://***:***
```

Fichier contab via "sudo crontab -e" :

```
http_proxy=http://***:***
https_proxy=http://***:***
```

## Dépendances

### Installation de node.js et de npm

```
sudo apt-get install -y curl && sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt-get install -y nodejs
```

### Installation de Python et de ses modules requis

```
sudo apt-get install -y python3 python3-pip libpq-dev
sudo pip3 install requests psycopg2 pyyaml lxml numpy
```

> Note : ne pas installer les modules Python depuis les paquets de la distribution (car obsolètes). 

> Note : pour mettre à jour les modules :  
 `sudo -E pip3 install --upgrade requests psycopg2 pyyaml lxml numpy`

### Installation des dépendances node.js de l'application (dev + prod)

```
cd /opt/le-bon-tag/ && sudo npm install -g npm rimraf
cd /opt/le-bon-tag/ && sudo npm install --unsafe-perm
```

## Base de données

### Installation

Attention, PostGIS >= 2.5 est requis (utilisation de ST_Intersects avec des collections).

Or sous Debian 9, PostGIS 2.3 sera installé depuis les paquets. Il faut donc utiliser une source alternative (https://wiki.postgresql.org/wiki/Apt) :

```
sudo apt-get install curl ca-certificates gnupg && curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && sudo apt-get update
```

> Note : l'application a été testée avec PostgreSQL 9.6, PostgreSQL 12, Postgis 2.5 et Postgis 3.

Pour installer PostgreSQL 9.6 avec Postgis 2.5 :  
`sudo apt-get install -y postgresql-9.6 postgresql-contrib-9.6 postgresql-9.6-pgrouting postgresql-9.6-postgis-2.5`

Pour installer PostgreSQL 12.0 avec Postgis 3 :  
`sudo apt-get install -y postgresql-12 postgresql-contrib postgresql-12-pgrouting postgresql-12-postgis-3`

> Fichier de configuration : /etc/postgresql/XX/main/postgresql.conf  (vérifier le port et listen_addresses = '*' pour ouvrir l'accès vers l'extérieur)  
> Fichier de gestion des droits d'accès : /etc/postgresql/XX/main/pg_hba.conf  
> Fichiers journaux : /var/lib/postgresql/XX/main/pg_log  

Où XX est la version de PostgreSQL.

Pour redémarrer le service en cas de modification de la configuration :  
`sudo systemctl restart postgresql`

### Création

1. Crée le rôle "osm", la base "osm" avec les extensions requises et les schémas :  
`sudo runuser -l postgres -c 'psql -f /opt/le-bon-tag/sql/01_lbt_database_and_schemas.sql'`

1. Crée les  tables dans le schéma "lebontag" de la base "osm" :  
`sudo runuser -l postgres -c 'psql -d osm -f /opt/le-bon-tag/sql/02_lbt_tables.sql'`

1. Insère les données dans le schéma "lebontag" :   
`sudo runuser -l postgres -c 'psql -d osm -f /opt/le-bon-tag/sql/03_lbt_data.sql'`

1. Insère des emprises d'exemple (optionnel) :   
`sudo runuser -l postgres -c 'psql -d osm -f /opt/le-bon-tag/sql/04_lbt_emprises_additionnelles.sql'`

## Installation des outils OSM & GDAL

`sudo apt-get install -y osm2pgsql osmctools gdal-bin python-gdal osmium-tool`

Attention, la commande précédente installe PostgreSQL et PostGIS en dépendances. Si la base de données est sur un autre serveur, vous pouvez désinstaller ces paquets ensuite. Par exemple (nom des paquets à adapter) :

```
sudo apt-get remove postgis postgis-doc postgresql-11 postgresql-11-postgis-2.5 postgresql-11-postgis-2.5-scripts postgresql-client-11 postgresql-client-common postgresql-common
sudo apt-get autoremove
```

## Données OSM de référence

### Téléchargement des données

Les données OSM de référence **doivent** contenir certaines métadonnées (contributeur, date, changeset, etc.) et se téléchargent après connexion avec un compte OSM sur https://osm-internal.download.geofabrik.de/index.html ("OpenStreetMap internal server").

Une fois le compte OSM lié au site "geofabrik.de", il est possible de télécharger les données de la région de son choix, par exemple :
`https://osm-internal.download.geofabrik.de/europe/france/languedoc-roussillon-internal.osm.pbf`

Placer le fichier téléchargé sur le serveur, par exemple dans "/opt/le-bon-tag/osm/source/".

> **Attention : ne jamais supprimer ce fichier, il sera utilisé par l'application pour connaître la date des données de référence et charger les données.**

## Fichiers de configuration

### Fichier /opt/le-bon-tag/.env

Copier le fichier /opt/le-bon-tag/.env.sample vers /opt/le-bon-tag/.env et éditer les paramètres :

```
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_DB=osm
POSTGRES_SCHEMA=lebontag
POSTGRES_USER=osm
POSTGRES_PASSWORD=osm
JWT_SECRET=b4e9cb1d8e532b7d2dae5be291dffab9
LOGGING=true
EPSG_OSM=4326
EPSG_LOCAL=2154
```

* POSTGRES_HOST : Adresse du serveur de base de données  
* POSTGRES_PORT : Port du serveur de base de données
* POSTGRES_DB : Nom de la base de données
* POSTGRES_SCHEMA : Schéma de la base de données
* POSTGRES_USER : Rôle PostgreSQL
* POSTGRES_PASSWORD : Mot de passe du rôle PostgreSQL
* JWT_SECRET : Clé de hachage à générer aléatoirement (pour le jeton de connexion)
* EPSG_OSM : Code EPSG du système de coordonnées des données OSM
* EPSG_LOCAL : Code EPSG du système de coordonnées local

### Fichier /opt/le-bon-tag/server/config.json

Copier le fichier /opt/le-bon-tag/server/config.json.sample vers /opt/le-bon-tag/server/config.json et éditer les paramètres :

```
{
        "port": 80,
        "bodyLimit": "50mb"
}
```

* port : port d'écoute du service node.js
* bodyLimit : taille maximale des fichiers à traiter

> Note : Laisser le port 80 pour que node.js serve directement l'application ou modifier le port (par exemple en 8081) pour utiliser ensuite Nginx ou autre comme reverse proxy (cf. https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-debian-9).

### Fichier /opt/le-bon-tag/config.yaml

Copier le fichier /opt/le-bon-tag/config.yaml.sample vers /opt/le-bon-tag/config.yaml et éditer les paramètres :

```
database:
    host: localhost
    port: 5432
    dbname: osm
    schema: lebontag
    username: osm
    password: osm
    srid: 4326
```

* host : Adresse du serveur de base de données  
* port : Port du serveur de base de données
* dbname : Nom de la base de données
* schema : Schéma de la base de données pour l'application
* username : Rôle PostgreSQL
* password : Mot de passe du rôle PostgreSQL
* srid : Système de coordonnées des données adiff

## Compilation de l'application

`cd /opt/le-bon-tag && sudo npm run lint && sudo npm run build`

## Exécution de l'application (en mode interactif)

Pour tester la bonne installation de l'application : 

`cd /opt/le-bon-tag && sudo node api/index.js`

Ouvrir un navigateur et entrer l'adresse du serveur. La page de connexion de l'application devrait apparaître.

"CTRL + C" pour quitter.

## Exécution de l'application (en mode service)

"pm2" est un gestionnaire de processus permettant de gérer le (re)démarrage automatique de l'application. Les commandes suivantes installent "pm2" et créent le service pour l'application.

```
cd /opt/le-bon-tag/ && sudo npm install -g pm2@latest --unsafe-perm
sudo pm2 start api/index.js -n LeBonTag && sudo pm2 startup systemd && sudo pm2 save
```

## Configuration de l'application

 * Ouvrir l'application via un navigateur.
 * Se connecter avec l'utilisateur "**admin**" et le mot de passe "**lbtOSM34a**" puis cliquer sur "Administration" et "Réglages".  
Cette page permet de spécifier les paramètres de l'application, dont certains sont décrits ci-dessous.

### Données OSM de référence

Cette rubrique spécifie les paramètres des données OSM de référence chargées lors de la mise en service de l'application.
Spécifier les bons chemins des fichiers, en particulier le chemin vers le fichier PBF des données de référence téléchargé précédemment.

+ Chemin et nom du fichier PBF des données OSM de référence à charger : fichier de données OSM téléchargé sur geofabrik.
+ Chemin et nom du fichier POLY de découpe des données OSM de référence : fichier POLY servant à découper le données OSM si vous ne souhaitez pas charger toutes les données. **Le fichier par défaut concerne la Métropole de Montpellier. Laissez vide si vous n'avez pas de fichier POLY.**

### Données OSM d'itinéraire

Cette rubrique spécifie les paramètres des données OSM pgRouting chargées lors de la mise en service de l'application puis mises à jour par l'application.

### Données OSM à valider

Cette rubrique spécifie les paramètres gérant la validation des données OSM.  
La tolérance de géométrie précise la tolérance en mètres au-delà de laquelle la géométrie d'un objet est considérée comme modifiée par rapport à son ancienne version.

### Carte

Cette rubrique spécifie l'emprise géographique de la zone de travail et son apparence sur la carte.

L'emprise (au format GeoJSON, EPSG:4326) est très importante car elle limite les données chargées par l'application à cette zone. Par défaut, l'emprise de Montpellier Méditerranée Métropole est spécifiée.

Pour utiliser une autre emprise, se rendre dans le module "Emprises" de l'application et téléverser une nouvelle emprise. Ensuite revenir à ce paramètre et sélectionner la nouvelle emprise depuis la liste déroulante.

Notes :

* Sous PostGIS, la fonction "ST_AsGeoJSON" peut être utilisée pour récupérer la géométrie d'un objet en GeoJSON, par exemple :

```
SELECT ST_AsGeoJSON(ST_Transform(way,4326),9,4) FROM osm2pgsql.osm_polygon WHERE name = 'Montpellier Méditerranée Métropole';
```

* Le site <https://polygons.openstreetmap.fr/> permet également de générer un GeoJSON à partir de l'ID d'une relation OSM.

### Rétention des fichiers

Cette rubrique spécifie l'âge maximal de certains fichiers générés par l'application.

### Serveur LDAP

Cette rubrique permet de définir la connexion à un serveur OpenLDAP pour la gestion des utilisateurs.  

### Enregistrement

Cliquer sur "Enregistrer" pour sauvegarder les modifications.

## Chargement des données OSM de référence et d'itinéraire

Lors de la mise en service de l'application, les données OSM de référence et d'itinéraire doivent être chargées en base.  
Le script "lbt-load-data.sh" automatise ce chargement :

```
sudo apt-get install -y dos2unix && sudo dos2unix /opt/le-bon-tag/scripts/*.sh && sudo chmod +x /opt/le-bon-tag/scripts/*.sh  
sudo -E /opt/le-bon-tag/scripts/lbt-load-data.sh
```

Ces données seront alors mises à jour avec les données validées dans l'application.  
> **Attention, la durée de chargement peut être élevée.**

## CRON des scripts Python

Plusieurs scripts Python sont à exécuter régulièrement, par exemple une fois par jour.

* "lbt-get-adiff-xxx" télécharge les fichiers adiff depuis l'API Overpass et les charge en base de données.
* "lbt-update-db-by-osc" met à jour la base de données de référence et la base d'itinéraire avec les objets validés depuis l'interface.
* "lbt-cleaning" nettoie les fichiers OSM et journaux générés par l'application.

Pour exécuter ces scripts Python tous les jours, on les déclare dans le CRON :

```
sudo dos2unix /opt/le-bon-tag/cron/*.sh && sudo chmod +x /opt/le-bon-tag/cron/*.sh
echo 'MAILTO=""' > /etc/cron.d/lebontag
echo '0 0 * * * root /opt/le-bon-tag/cron/lbt-cleaning.sh' >> /etc/cron.d/lebontag
echo '0 1 * * * root /opt/le-bon-tag/cron/lbt-update-db-by-osc.sh' >> /etc/cron.d/lebontag  
echo '0 3 * * * root /opt/le-bon-tag/cron/lbt-get-adiff.sh' >> /etc/cron.d/lebontag
```

Pour charger les premières données adiff, on peut exécuter le script immédiatement :  
`sudo -E /opt/le-bon-tag/cron/lbt-get-adiff.sh`

### Mode "démo" (demo.lebontag.fr)

Pour utiliser l'application en mode "démo", ne pas mettre les fichiers précédents dans le CRON mais uniquement "lbt-demo.sh" afin de restaurer régulièrement les paramètres, par exemple toutes les heures :

```
sudo dos2unix /opt/le-bon-tag/cron/*.sh && sudo chmod +x /opt/le-bon-tag/cron/*.sh
echo 'MAILTO=""' > /etc/cron.d/lebontag
echo '0 * * * * root /opt/le-bon-tag/cron/lbt-demo.sh' >> /etc/cron.d/lebontag
```

## Création d'une release

Pour créer manuellement une release de l'application (version compilée) :

`cd /opt/le-bon-tag/ && sudo npm run package`

Le fichier .tgz sera généré dans /opt/le-bon-tag/releases.
