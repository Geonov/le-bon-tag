# LeBonTag

Initié par Montpellier Méditerranée Métropole et développé par [Geonov][6], **LeBonTag** est un outil de validation de données OSM (OpenStreetMap) avant leur import en base interne.

Site web : [https://www.lebontag.fr][3]

## Installation

Deux modes d'installation peuvent être choisis :

 * Depuis les sources (Linux)
 * Depuis les releases (Linux ou Windows)

### Depuis les sources

Lire le fichier **[README-DEV-Linux.md][1]**.

 * Avantages : mises à jour via Git, accès au code
 * Inconvénients : nécessite l'installation des modules de développement et la compilation de l'application

### Depuis les releases

Lire le fichier **[README-PROD-Linux.md][2]** pour Linux ou **[README-PROD-Windows.md][10]** pour Windows.

 * Avantages : utilisable sans compilation, plus léger
 * Inconvénient : ne peut pas être mis à jour (sauf par écrasement)

## Mise à jour

Lire le fichier **[UPDATE.md][8]**.

## Reboot des données

Le "reboot" consiste à ré-initialiser les données de référence à partir d'un export geofabrik et de redémarrer la validation à cette date.

Lire le fichier **[REBOOT.md][9]**.

## Bugs et propositions d'évolution

Les soumissions de bugs et de propositions d'évolution se font sur le [GitLab de l'application][4].  
Les développeurs peuvent également soumettre des demandes de fusion.

## Changelog

Consulter le fichier [CHANGELOG.md][7].

## Démo

Une démonstration de l'application est disponible sur [https://demo.lebontag.fr][5] : 

Utilisateur simple : user / lbtOSM34u  
Administrateur : admin / lbtOSM34a  

> Note : les utilisateurs, paramètres et objets à valider sont ré-initialisés régulièrement.  
> Veuillez vous déconnecter & reconnecter de l'application si les objets à valider n'apparaissent plus (suite à une ré-initialisation).

[1]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/README-DEV-Linux.md
[2]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/README-PROD-Linux.md
[10]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/README-PROD-Windows.md
[3]: https://www.lebontag.fr/
[4]: https://gitlab.com/Geonov/le-bon-tag/issues
[5]: https://demo.lebontag.fr/
[6]: https://www.geonov.fr/
[7]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/CHANGELOG.md
[8]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/UPDATE.md
[9]: https://gitlab.com/Geonov/le-bon-tag/-/blob/master/REBOOT.md
