# Installation de l'application "Le Bon Tag" sur Windows (MODE PROD)

## Pré-requis

L'application nécessite entre autres PostgreSQL avec PostGIS, Python 3, node.js, npm et osm2pgrouting.  
Ce document décrit l'installation complète de l'application et de ses dépendances pour une utilisation en mode production (version compilée) sous Windows.

Les droits d'administration seront nécessaires.

## Application

- Décompresser le fichier "le-bon-tag_x.y.z.tgz" contenant l'application avec un logiciel comme 7zip :

  ![7zip](./img/README-PROD-Windows_01.png)

  Un fichier "le-bon-tag_x.y.z.tar" est généré.

- Décompresser ce fichier à son tour, mais cette fois-ci en spécifiant un sous-répertoire de destination :

  ![7zip](./img/README-PROD-Windows_02.png)

- Les messages d'avertissement de type "Can not create symbolic link" peuvent être ignorés en cliquant sur le bouton "Fermer" :

  ![7zip](./img/README-PROD-Windows_03.png)

- Déplacer le répertoire généré sur le serveur, en évitant les chemins avec des espaces ou des caractères spéciaux, par exemple à la racine de C:\, et le renommer `le-bon-tag` :

  ![Dossier](./img/README-PROD-Windows_04.png)

## Dépendances

### Installation de node.js et de npm

La version **12** de node.js doit être installée : [Lien de téléchargement](https://nodejs.org/download/release/v12.22.5/node-v12.22.5-x64.msi)

Utiliser les paramètres d'installation par défaut.

### Installation de Python et de ses modules requis

La version **3** de Python doit être installée : [Lien de téléchargement](https://www.python.org/ftp/python/3.9.7/python-3.9.7-amd64.exe)

- Cocher "Add Python 3.X to PATH" et cliquer sur "Customize installation" :

  ![Python](./img/README-PROD-Windows_05.png)

- Cliquer sur Next, cocher "Install for all users" et cliquer sur "Install" :

  ![Python](./img/README-PROD-Windows_06.png)
  
- A la fin de l'installation, cliquer sur "Disable path length limit" si l'option est proposée, puis sur "Close" :

  ![Python](./img/README-PROD-Windows_08.png)

- Ouvrir une invite de commandes en tant qu'administrateur et exécuter (mise à jour de "pip") :

  ```
  "C:\Program Files\Python39\python.exe" -m pip install --upgrade pip
  ```

- Toujours avec l'invite de commandes, exécuter (installation des modules requis) :

  ```
  "C:\Program Files\Python39\python.exe" -m pip install requests psycopg2 pyyaml lxml numpy
  ```
  
> Note : pour mettre à jour les modules déjà installés :  
  `"C:\Program Files\Python39\python.exe" -m pip upgrade requests psycopg2 pyyaml lxml numpy`

## Base de données

L'application nécessite une base de données PostgreSQL, avec des extensions comme PostGIS.  
Attention, PostGIS >= 2.5 est requis (utilisation de ST_Intersects avec des collections).  

La base de données peut être sur un autre serveur (recommandé) ou sur le même serveur que l'application.

### Installation

[Lien de téléchargement](https://sbp.enterprisedb.com/getfile.jsp?fileid=1257789)

- Utiliser les paramètres d'installation par défaut, spécifier un mot de passe pour le "super utilisateur" de la base de données et préciser la locale "C" :

  ![PostgreSQL](./img/README-PROD-Windows_09.png)

- A la fin de l'installation, accepter l'exécution de "Stack Builder" afin d'installer l'extension PostGIS (ou refuser et exécuter directement "postgis_3_1_pg13.exe" si déjà téléchargé) :

  ![PostGIS](./img/README-PROD-Windows_07.png)

- Répondre "Oui" aux différentes questions posées à la fin de l'installation de PostGIS.

> Fichier de configuration : `C:\Program Files\PostgreSQL\XX\data\postgresql.conf`  (vérifier le port et `listen_addresses = '*'` pour ouvrir l'accès vers l'extérieur si besoin)  
> Fichier de gestion des droits d'accès : `C:\Program Files\PostgreSQL\XX\data\pg_hba.conf`  
> Fichiers journaux : `C:\Program Files\PostgreSQL\XX\data\log`  
> Où XX est la version de PostgreSQL.

### Création

Les scripts SQL de l'application sont chargés en base depuis l'invite de commandes :

- Crée le rôle "osm", la base "osm" avec les extensions requises et les schémas :

```
SET PGCLIENTENCODING=utf-8 && chcp 65001
"C:\Program Files\PostgreSQL\13\bin\psql" --host=localhost --port=5432 --username=postgres -f C:\le-bon-tag\sql\01_lbt_database_and_schemas.sql
```

- Crée les  tables dans le schéma "lebontag" de la base "osm" :

```
SET PGCLIENTENCODING=utf-8 && chcp 65001
"C:\Program Files\PostgreSQL\13\bin\psql" --host=localhost --port=5432 --username=postgres -d osm -f C:\le-bon-tag\sql\02_lbt_tables.sql
```

- Insère les données dans le schéma "lebontag" :

```
SET PGCLIENTENCODING=utf-8 && chcp 65001
"C:\Program Files\PostgreSQL\13\bin\psql" --host=localhost --port=5432 --username=postgres -d osm -f C:\le-bon-tag\sql\03_lbt_data.sql
```

## Données OSM de référence

### Téléchargement des données

Les données OSM de référence **doivent** contenir certaines métadonnées (contributeur, date, changeset, etc.) et se téléchargent après connexion avec un compte OSM sur `https://osm-internal.download.geofabrik.de/index.html` ("OpenStreetMap internal server").

Une fois le compte OSM lié au site "geofabrik.de", il est possible de télécharger les données de la région de son choix, par exemple :
`https://osm-internal.download.geofabrik.de/europe/france/languedoc-roussillon-latest-internal.osm.pbf`

Placer le fichier téléchargé sur le serveur, par exemple dans "C:\le-bon-tag\osm\source".

> **Attention : ne jamais supprimer ce fichier, il sera utilisé par l'application pour connaître la date des données de référence, charger les données puis mettre à jour les données pgRouting.**

## Fichiers de configuration

### Fichier C:\le-bon-tag\.env

Copier le fichier `C:\le-bon-tag\.env.sample` vers `C:\le-bon-tag\.env` :  
`copy C:\le-bon-tag\.env.sample C:\le-bon-tag\.env`

Et éditer les paramètres si nécessaire (connexion à la base de données) :

```
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_DB=osm
POSTGRES_SCHEMA=lebontag
POSTGRES_USER=osm
POSTGRES_PASSWORD=osm
JWT_SECRET=b4e9cb1d8e532b7d2dae5be291dffab9
LOGGING=true
EPSG_OSM=4326
EPSG_LOCAL=4326
```

* POSTGRES_HOST : Adresse du serveur de base de données  
* POSTGRES_PORT : Port du serveur de base de données
* POSTGRES_DB : Nom de la base de données
* POSTGRES_SCHEMA : Schéma de la base de données
* POSTGRES_USER : Rôle PostgreSQL
* POSTGRES_PASSWORD : Mot de passe du rôle PostgreSQL
* JWT_SECRET : Clé de hachage à générer aléatoirement (pour le jeton de connexion)
* EPSG_OSM : Code EPSG du système de coordonnées des données OSM
* EPSG_LOCAL : Code EPSG du système de coordonnées local. **Attention, sous Windows, "4326" est obligatoire.**

### Fichier /opt/le-bon-tag/api/config.json

Copier le fichier `C:\le-bon-tag\api\config.json.sample` vers `C:\le-bon-tag\api\config.json` et éditer les paramètres si nécessaire (port d'écoute du serveur Web) :

```

{
        "port": 80,
        "bodyLimit": "50mb"
}
```

* port : port d'écoute du service node.js
* bodyLimit : taille maximale des fichiers à traiter

### Fichier /opt/le-bon-tag/config.yaml

Copier le fichier `C:\le-bon-tag\config.yaml.sample` vers `C:\le-bon-tag\config.yaml` et éditer les paramètres si nécessaire (connexion à la base de données) :

```
database:
    host: localhost
    port: 5432
    dbname: osm
    schema: lebontag
    username: osm
    password: osm
    srid: 4326
```

* host : Adresse du serveur de base de données  
* port : Port du serveur de base de données
* dbname : Nom de la base de données
* schema : Schéma de la base de données pour l'application
* username : Rôle PostgreSQL
* password : Mot de passe du rôle PostgreSQL
* srid : Système de coordonnées des données adiff

## Exécution de l'application (en mode interactif)

A ce stade, l'application devrait être fonctionnelle. Pour tester la bonne installation de l'application, exécuter en ligne de commandes : 

`cd C:\le-bon-tag && npm start`

Ouvrir un navigateur et entrer l'adresse du serveur (ou `http://127.0.0.1` en local). La page de connexion de l'application devrait apparaître.

Taper `CTRL + C` en ligne de commandes pour arrêter l'application.

## Exécution de l'application (en mode service)

"pm2" est un gestionnaire de processus permettant de gérer les applications. De plus, pour créer un service Windows, le programme `pm2-installer` doit être utilisé.

A exécuter dans une invite de commandes en tant qu'administrateur :

```
cd C:\le-bon-tag\tools\pm2-installer-main
npm run configure
npm run configure-policy
npm run setup
cd C:\le-bon-tag && npm install -g pm2@latest --unsafe-perm
pm2 start api/index.js -n LeBonTag && pm2 save
```

## Configuration de l'application

 * Ouvrir l'application via un navigateur.
 * Se connecter avec l'utilisateur "**admin**" et le mot de passe "**lbtOSM34a**" puis cliquer sur "Administration" et "Réglages".  
Cette page permet de spécifier les paramètres de l'application, dont certains sont décrits ci-dessous.

### Données OSM de référence

Cette rubrique spécifie les paramètres des données OSM de référence chargées lors de la mise en service de l'application puis mises à jour par l'application.  
Spécifier les bons chemins des fichiers (**avec des `/` et non des `\`**), en particulier le chemin vers le fichier PBF des données de référence téléchargé précédemment.

Par exemple :

```
C:/le-bon-tag/osm/source/languedoc-roussillon-latest-internal.osm.pbf

C:/le-bon-tag/osm/poly/MMM_4326_20190701.poly

C:/le-bon-tag/osm/style/MMM_20190701.style
```

**Attention, pour le paramètre "Système de coordonnées des données de référence après chargement (SRID)", la valeur "4326" est obligatoire sous Windows.**

+ Chemin et nom du fichier PBF des données OSM de référence à charger : fichier de données OSM téléchargé sur geofabrik.
+ Chemin et nom du fichier POLY de découpe des données OSM de référence : fichier POLY servant à découper le données OSM si vous ne souhaitez pas charger toutes les données. **Le fichier par défaut concerne la Métropole de Montpellier. Laissez vide si vous n'avez pas de fichier POLY.**

### Données OSM d'itinéraire

Cette rubrique spécifie les paramètres des données OSM pgRouting chargées lors de la mise en service de l'application puis mises à jour par l'application.  
Spécifier le bon chemin du fichier (**avec des `/` et non des `\`**), par exemple :

```
C:/le-bon-tag/osm/mapconfig/mapconfig_MMM_20191107.xml
```

### Données OSM à valider

Cette rubrique spécifie les paramètres gérant la validation des données OSM.  
La tolérance de géométrie précise la tolérance en mètres au-delà de laquelle la géométrie d'un objet est considérée comme modifiée par rapport à son ancienne version.

### Carte

Cette rubrique spécifie l'emprise géographique de la zone de travail et son apparence sur la carte.

L'emprise (au format GeoJSON, EPSG:4326) est très importante car elle limite les données chargées par l'application à cette zone. Par défaut, l'emprise de Montpellier Méditerranée Métropole est spécifiée.

Pour utiliser une autre emprise, se rendre dans le module "Emprises" de l'application et téléverser une nouvelle emprise. Ensuite revenir à ce paramètre et sélectionner la nouvelle emprise depuis la liste déroulante.

Notes :

* Sous PostGIS, la fonction "ST_AsGeoJSON" peut être utilisée pour récupérer la géométrie d'un objet en GeoJSON, par exemple :

```
SELECT ST_AsGeoJSON(ST_Transform(way,4326),9,4) FROM osm2pgsql.osm_polygon WHERE name = 'Montpellier Méditerranée Métropole';
```

* Le site <https://polygons.openstreetmap.fr/> permet également de générer un GeoJSON à partir de l'ID d'une relation OSM.

### Rétention des fichiers

Cette rubrique spécifie l'âge maximal de certains fichiers générés par l'application.

### Serveur LDAP

Cette rubrique permet de définir la connexion à un serveur OpenLDAP pour la gestion des utilisateurs.  

### Enregistrement

Cliquer sur "Enregistrer" pour sauvegarder les modifications.

## Chargement des données OSM de référence et d'itinéraire

Lors de la mise en service de l'application, les données OSM de référence et d'itinéraire doivent être chargées en base.  
Le script "lbt-load-data.bat" automatise ce chargement :

```
cd "C:\le-bon-tag\scripts"
lbt-load-data.bat
```

Ces données seront alors mises à jour avec les données validées dans l'application.  
> **Attention, la durée de chargement peut être élevée.**

## Planification des scripts Python

Plusieurs scripts Python sont à exécuter régulièrement, par exemple une fois par jour, pour maintenir les données à jour :

* "lbt-get-adiff-xxx" télécharge les fichiers adiff depuis l'API Overpass et les charge en base de données.
* "lbt-update-db-by-osc" met à jour la base de données de référence et la base d'itinéraire avec les objets validés depuis l'interface.
* "lbt-cleaning" nettoie les fichiers OSM et journaux générés par l'application.

Pour exécuter ces scripts Python tous les jours sous Windows, il faut créer des tâches planifiées à l'aide du planificateur de tâches.  
Les planifications à mettre en place sont :

```
Fichier c:\le-bon-tag\cron\lbt-cleaning.bat : tous les jours à 00H00
Fichier c:\le-bon-tag\cron\lbt-update-db-by-osc.bat : tous les jours à 01H00
Fichier c:\le-bon-tag\cron\lbt-get-adiff.bat : tous les jours à 03H00
```

Pour charger les premières données adiff, on peut exécuter le script immédiatement manuellement :

```
cd c:\le-bon-tag\cron\
lbt-get-adiff.bat
```
