FROM node:current-slim
WORKDIR /opt/le-bon-tag
COPY package.json .
RUN npm install
EXPOSE 8080
CMD [ "npm", "start" ]
COPY . .
ADD .env.sample .env
ADD config.yaml.sample config.yaml
