const path = require('path');

const host = '127.0.0.1';
const port = 8082;

module.exports = {
    lintOnSave: true,
    // baseUrl: `http://${host}:${port}/`,
    publicPath: '/',
    configureWebpack: {
        resolve: {
            extensions: ['.js', '.vue'],
            alias: {
                '@': path.resolve(__dirname, 'src/'),
            },
        },
        externals: {
            // global app config object
            config: JSON.stringify({
                apiUrl: `${process.env.BASE_URL}:${process.env.PORT}`,
            }),
        },
    },
    devServer: {
        port,
        host,
        hotOnly: true,
        watchOptions: {
            ignored: /public/,
        },
        disableHostCheck: true,
        clientLogLevel: 'warning',
        inline: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
        },
        proxy: {
            '/api': {
                target: {
                    host: `${process.env.BASE_URL}`,
                    port: `${process.env.PORT}`,
                },
                // ws: true,
                changeOrigin: false,
                pathRewrite: {
                    '^/api': '/api',
                },
            },
        },
    },
    css: { sourceMap: true },
};
