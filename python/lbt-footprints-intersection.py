#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, os, requests, psycopg2, gzip, time, logging, sys, re, subprocess, shutil, platform
    from pathlib import Path
    from datetime import date, datetime, timezone
    from copy import deepcopy
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
except Exception as e:
    print(e)
    sys.exit(1)

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Date actuelle
# 2020-01-16 09:18:39
end_date = datetime.utcnow().replace(microsecond=0)
end_date_str = end_date.strftime("%Y%m%d")

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-footprints-intersection/' + str(end_date.year) + str('%02d' % end_date.month) + '/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + 'footprints-intersection' + end_date_str + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Recalcule les intersections des objets avec les emprises
try:
    logging.info('Refresh lbt_osmdiff_footprint...')

    cursor.execute(sql.SQL("TRUNCATE {0}.lbt_osmdiff_footprint;").format(sql.Identifier(lbtSchema)))

    cursor.execute(sql.SQL("DROP INDEX IF EXISTS {0}.osmdiff_footprint_odidv_idx;").format(sql.Identifier(lbtSchema)))
    cursor.execute(sql.SQL("DROP INDEX IF EXISTS {0}.osmdiff_footprint_odid_idx;").format(sql.Identifier(lbtSchema)))
    cursor.execute(sql.SQL("DROP INDEX IF EXISTS {0}.osmdiff_footprint_fpid_idx;").format(sql.Identifier(lbtSchema)))

    cursor.execute(sql.SQL("WITH footprints AS ( SELECT fp_id,(st_dump(ST_CollectionExtract(ST_MakeValid(fp_geom),3))).geom FROM {0}.lbt_footprint ) INSERT INTO {0}.lbt_osmdiff_footprint (od_id,od_new_version,fp_id) SELECT d.od_id,d.od_new_version,f.fp_id FROM {0}.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_new_geom, f.geom) WHERE d.od_new_geom IS NOT NULL UNION SELECT d.od_id,d.od_new_version,f.fp_id FROM {0}.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_old_geom, f.geom) WHERE d.od_old_geom IS NOT NULL;").format(sql.Identifier(lbtSchema)))

    cursor.execute(sql.SQL("CREATE INDEX osmdiff_footprint_odidv_idx ON {0}.lbt_osmdiff_footprint USING btree (od_id,od_new_version);").format(sql.Identifier(lbtSchema)))
    cursor.execute(sql.SQL("CREATE INDEX osmdiff_footprint_odid_idx ON {0}.lbt_osmdiff_footprint USING btree (od_id);").format(sql.Identifier(lbtSchema)))
    cursor.execute(sql.SQL("CREATE INDEX osmdiff_footprint_fpid_idx ON {0}.lbt_osmdiff_footprint USING btree (fp_id);").format(sql.Identifier(lbtSchema)))

    conn.commit()
    cursor.close()
    conn.close()
    logging.info('Ended.')
except Exception as e:
    logging.critical('lbt_osmdiff_footprint refresh error : ' + str(e))
    sys.exit(1)

logging.info('Python script ended.')
