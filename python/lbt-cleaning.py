#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, os, shutil, psycopg2, gzip, time, logging, sys
    from datetime import date, timedelta, datetime
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
except Exception as e:
    print(e)
    sys.exit(1)

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Variable(s) & constante(s)
vToday = date.today()
vYesterday = vToday + timedelta(days=-1)

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-cleaning/' + str(vToday.year) + str('%02d' % vToday.month) + '/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + str(vToday) + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Récupère les paramètres stockés en base de données
try:
    cursor.execute(sql.SQL("SELECT s_name,s_value FROM {0}.lbt_setting;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    drows = dict(rows)
    export_retention = drows['export_retention']
    log_retention = drows['log_retention']
    osm_retention = drows['osm_retention']
except Exception as e:
    logging.critical('LeBonTag settings fetching error : ' + str(e))
    sys.exit(1)

now = time.time()
logging.info('Datetime: ' + str(now) + ' (' + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(now))) + ')')

# Nettoyage des contributeurs inutilisés (allège le JSON à charger par l'application)
try:
    cursor.execute(sql.SQL("DELETE FROM {0}.lbt_contributor WHERE ct_id NOT IN ( SELECT DISTINCT (od_new_ct_id) od_new_ct_id FROM {0}.lbt_osmdiff WHERE od_new_ct_id IS NOT NULL UNION SELECT DISTINCT (od_old_ct_id) od_old_ct_id FROM {0}.lbt_osmdiff WHERE od_old_ct_id IS NOT NULL ) AND ct_internal IS FALSE AND ct_score = 0;").format(sql.Identifier(lbtSchema)))
    conn.commit()
except Exception as e:
    logging.critical('Failing to delete unused contributors: ' + str(e))
    sys.exit(1)

# Nettoyage des changesets inutilisés
try:
    cursor.execute(sql.SQL("DELETE FROM {0}.lbt_osmchangeset WHERE ocs_id NOT IN ( SELECT DISTINCT (od_new_changeset) od_new_changeset FROM {0}.lbt_osmdiff WHERE od_new_changeset IS NOT NULL UNION SELECT DISTINCT (od_old_changeset) od_old_changeset FROM {0}.lbt_osmdiff WHERE od_old_changeset IS NOT NULL );").format(sql.Identifier(lbtSchema)))
    conn.commit()
except Exception as e:
    logging.critical('Failing to delete unused changesets: ' + str(e))
    sys.exit(1)

# Ferme la connexion PG
try:
    logging.info('Close database connection.')
    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Error when closing PG : ' + str(e))
    sys.exit(1)

# Fichiers de log
try:
    if log_retention is not None:
        logging.info('log_retention: ' + log_retention)
        path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../log/'
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                filename = os.path.join(root,filename)
                if os.stat(filename).st_mtime < now - int(log_retention) * 86400 and filename.endswith('.log'):
                    os.remove(filename)
                    logging.info('Deleted log file: ' + filename)
except Exception as e:
    logging.critical('Failing to delete log files: ' + str(e))
    sys.exit(1)

# Fichiers OSM
try:
    if osm_retention is not None:
        logging.info('osm_retention: ' + osm_retention)
        path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../osm/adiff/'
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                filename = os.path.join(root,filename)
                if os.stat(filename).st_mtime < now - int(osm_retention) * 86400 and filename.endswith('.xml.gz'):
                    os.remove(filename)
                    logging.info('Deleted adiff OSM file: ' + filename)
        path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../osm/changeset/'
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                filename = os.path.join(root,filename)
                if os.stat(filename).st_mtime < now - int(osm_retention) * 86400 and filename.endswith('.xml'):
                    os.remove(filename)
                    logging.info('Deleted changeset OSM file: ' + filename)
        path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../osm/osc_update/'
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                filename = os.path.join(root,filename)
                if os.stat(filename).st_mtime < now - int(osm_retention) * 86400 and filename.endswith('.osc'):
                    os.remove(filename)
                    logging.info('Deleted OSC OSM file: ' + filename)
except Exception as e:
    logging.critical('Failing to delete OSM files: ' + str(e))
    sys.exit(1)

# Fichiers d'export
try:
    if export_retention is not None:
        logging.info('export_retention: ' + export_retention)
        path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../public/ogr2ogr/'
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                filename = os.path.join(root,filename)
                if os.stat(filename).st_mtime < now - int(export_retention) * 86400 and filename.endswith('.zip'):
                    os.remove(filename)
                    logging.info('Deleted export file: ' + filename)
except Exception as e:
    logging.critical('Failing to delete export files: ' + str(e))
    sys.exit(1)

# ===
# Fin
# ===

logging.info('Python script ended.')
