#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, psycopg2, logging, os, sys, subprocess, shutil, platform, itertools
    from pathlib import Path
    from datetime import date
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from textwrap import indent, wrap
except Exception as e:
    print(e)
    sys.exit(1)

# Classe de retour pour erreurs subprocess
class CalledProcessError(subprocess.CalledProcessError):
    """A more verbose version of :py:class:`subprocess.CalledProcessError`. Replaces the standard string representation of a :py:class:`subprocess.CalledProcessError` with one that has more output and error information and is formatted to be more readable in a stack trace."""
    def __str__(self):
        errors = self.stderr.decode(sys.stderr.encoding).split("\n")
        outputs = self.stdout.decode(sys.stdout.encoding).split("\n")
        p, q = ("  ", "| ")
        lines = itertools.chain(
            wrap(f"{super().__str__()}"),
            ["Output:"],
            *(
                wrap(output, initial_indent=p, subsequent_indent=p + q)
                for output in outputs
            ),
            ["Errors:"],
            *(
                wrap(error, initial_indent=p, subsequent_indent=p + q)
                for error in errors
            ),
        )
        lines = indent("\n".join(lines), p + q)
        return f"\n{lines}"

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Variable(s)
vToday = date.today()

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-load-data/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + str(vToday) + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Environnement
platform = platform.system()
logging.info('OS: ' + platform)
current_directory = os.getcwd().replace(os.sep, '/')
logging.info('Current directory: ' + current_directory)
work_directory = str(Path(current_directory).parent.absolute()).replace(os.sep, '/')
logging.info('Work directory: ' + work_directory)

# Constantes
osm_ref_data_file = work_directory + '/osm/source/lbt-osm-ref-data.osm'
osm_pgr_data_file = work_directory + '/osm/source/lbt-osm-pgr-data.osm'

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Récupère les paramètres stockés en base de données
try:
    cursor.execute(sql.SQL("SELECT s_name,s_value FROM {0}.lbt_setting;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    drows = dict(rows)
    db_ref_srid = int(drows['db_ref_srid'])
    db_ref_schema = drows['db_ref_schema']
    db_ref_prefix = drows['db_ref_prefix']
    db_pgr_schema = drows['db_pgr_schema']
    db_pgr_prefix = drows['db_pgr_prefix']
    osm_mapconfigfile = drows['osm_mapconfigfile']
    osm_pbffile = drows['osm_pbffile']
    osm_polyfile = drows['osm_polyfile']
    osm_stylefile = drows['osm_stylefile']

except Exception as e:
    logging.critical('LeBonTag settings fetching error : ' + str(e))
    sys.exit(1)

# Découpe le fichier PBF des données de référence et le convertit en OSM
try:
    logging.info('Split/copy PBF file...')
    if osm_pbffile != None:
        if ((osm_polyfile != None) and (osm_polyfile != '')):
            #proc = subprocess.run(['osmconvert',osm_pbffile,'-B=' + osm_polyfile,'--complete-ways','--complex-ways','--out-osm','-o=' + osm_ref_data_file])
            #with open(osm_ref_data_file,'w') as fout:
                #proc = subprocess.Popen(['osmconvert',osm_pbffile,'-B=' + osm_polyfile,'--complete-ways','--complex-ways','--out-osm'],stdout=fout)
            if platform == 'Windows':
                os.system(work_directory + '/tools/osmconvert/osmconvert64-0.8.8p.exe ' + osm_pbffile + ' -B=' + osm_polyfile + ' --complete-ways --complex-ways --out-osm -o=' + osm_ref_data_file)
            else:
                os.system('osmconvert ' + osm_pbffile + ' -B=' + osm_polyfile + ' --complete-ways --complex-ways --out-osm -o=' + osm_ref_data_file)
        else:
            #proc = subprocess.run(['osmconvert',osm_pbffile,'--complete-ways','--complex-ways','--out-osm','-o=' + osm_ref_data_file])
            #with open(osm_ref_data_file,'w') as fout:
                #proc = subprocess.Popen(['osmconvert',osm_pbffile,'--complete-ways','--complex-ways','--out-osm'],stdout=fout)
            if platform == 'Windows':
                os.system(work_directory + '/tools/osmconvert/osmconvert64-0.8.8p.exe ' + osm_pbffile + ' --complete-ways --complex-ways --out-osm -o=' + osm_ref_data_file)
            else:
                os.system('osmconvert ' + osm_pbffile + ' --complete-ways --complex-ways --out-osm -o=' + osm_ref_data_file)
except Exception as e:
    logging.critical('PBF file splitting or conversion error : ' + str(e))
    sys.exit(1)

# Charge les données de référence OSM, avec ou sans fichier de style spécifique ("default.style" sera utilisé par défaut par le programme)
# Sous Windows, force le chargement en 4326 (sinon erreur de dépendance avec proj4)
# Suppression de '--middle-schema='+str(db_ref_schema) non compatible avec certaines versions (SDIS34)
try:

    logging.info('Load OSM file with osm2pgsql...')

    logging.info('Change search_path for osm2pgsql.')
    cursor.execute(sql.SQL("ALTER ROLE {0} SET search_path TO {1}, public;").format(sql.Identifier(lbtUser),sql.Identifier(db_ref_schema)))
    conn.commit()

    try:
        if os.path.isfile(osm_ref_data_file):
            if osm_stylefile is not None:
                if platform == 'Windows':
                    proc = logging.info(subprocess.run([work_directory + '/tools/osm2pgsql/osm2pgsql.exe','--create','--slim','--proj',str(db_ref_srid),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--style',osm_stylefile,'--input-reader','osm',osm_ref_data_file],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
                else:
                    proc = logging.info(subprocess.run(['osm2pgsql','--create','--slim','--proj',str(db_ref_srid),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--style',osm_stylefile,'--input-reader','osm',osm_ref_data_file],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
            else:
                if platform == 'Windows':
                    proc = logging.info(subprocess.run([work_directory + '/tools/osm2pgsql/osm2pgsql.exe','--create','--slim','--proj',str(db_ref_srid),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--input-reader','osm',osm_ref_data_file],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
                else:
                    proc = logging.info(subprocess.run(['osm2pgsql','--create','--slim','--proj',str(db_ref_srid),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--input-reader','osm',osm_ref_data_file],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
    except subprocess.CalledProcessError as cpe:
        logging.critical(CalledProcessError(cpe.returncode, cpe.cmd, output=cpe.output, stderr=cpe.stderr))

    logging.info('Change search_path for osm2pgsql.')
    cursor.execute(sql.SQL("ALTER ROLE {0} SET search_path TO public, {1};").format(sql.Identifier(lbtUser),sql.Identifier(db_ref_schema)))
    conn.commit()

    # PATCH : Fusion des grandes ways découpées par osm2pgsql
    logging.info('SQL Patch for osm2pgsql...')
    cursor.execute(sql.SQL("WITH duplicates AS ( DELETE FROM {0}.{1} WHERE osm_id IN (SELECT osm_id FROM {0}.{1} GROUP BY osm_id, name, tags HAVING COUNT(osm_id) > 1 AND osm_id > 0) RETURNING * ) INSERT INTO {0}.{1} (osm_id, name, tags, way) SELECT osm_id, name, tags, ST_LineMerge(ST_Union(way)) as way FROM duplicates GROUP BY osm_id, name, tags;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_line')))
    conn.commit()

except Exception as e:
    logging.critical('OSM file loading error : ' + str(e))
    sys.exit(1)

# SQL pgRouting
try:
    logging.info('Create pgRouting tables')
    
    cursor.execute(sql.SQL("DROP TABLE IF EXISTS {0}.%s_ways_vertices CASCADE;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("CREATE TABLE {0}.%s_ways_vertices (gid bigserial NOT NULL,osm_id int8,lon float8,lat float8,junction boolean,the_geom public.geometry(point,4326),CONSTRAINT %s_ways_vertices_pkey PRIMARY KEY (gid)) WITH (autovacuum_enabled=false);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("WITH trueVertices AS ( SELECT (ST_DumpPoints(ST_Transform(way,4326))).geom AS the_geom FROM {0}.%s_line WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed' UNION ALL SELECT (ST_DumpPoints(ST_ExteriorRing((ST_Dump(ST_Transform(way,4326))).geom))).geom AS the_geom FROM {0}.%s_polygon WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed') INSERT INTO {1}.%s_ways_vertices (junction,the_geom) SELECT true, the_geom FROM trueVertices GROUP BY the_geom HAVING count(*) >= 2;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("WITH falseVertices AS ( SELECT (ST_DumpPoints(ST_Transform(way,4326))).geom AS the_geom FROM {0}.%s_line WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed' UNION ALL SELECT (ST_DumpPoints(ST_ExteriorRing((ST_Dump(ST_Transform(way,4326))).geom))).geom AS the_geom FROM {0}.%s_polygon WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed') INSERT INTO {1}.%s_ways_vertices (junction,the_geom) SELECT false, the_geom FROM falseVertices GROUP BY the_geom HAVING count(*) < 2;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("CREATE INDEX ways_vertices_the_geom_idx ON {0}.%s_ways_vertices USING gist (the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways_vertices SET lon=ST_X(the_geom),lat=ST_Y(the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("DROP TABLE IF EXISTS {0}.%s_ways CASCADE;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("CREATE TABLE {0}.%s_ways (gid bigserial NOT NULL,osm_id int8,tag_id int4,\"length\" float8,length_m float8,\"name\" text,\"source\" int8,target int8,source_osm int8,target_osm int8,\"cost\" float8,reverse_cost float8,cost_s float8,reverse_cost_s float8,\"rule\" text,one_way int4,oneway text,x1 float8,y1 float8,x2 float8,y2 float8,maxspeed_forward float8,maxspeed_backward float8,priority float8 DEFAULT 1,the_geom public.geometry(linestring, 4326),CONSTRAINT %s_ways_pkey PRIMARY KEY (gid)) WITH (autovacuum_enabled=false);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("INSERT INTO {0}.%s_ways (osm_id, \"name\", the_geom) SELECT ol.osm_id,ol.tags->'name',(ST_Dump(ST_Split(ST_Transform(ol.way,4326),ST_Collect(jt.the_geom)))).geom FROM {1}.%s_line AS ol JOIN {0}.%s_ways_vertices AS jt ON jt.the_geom && ST_Transform(ol.way,4326) WHERE jt.junction IS TRUE AND ( ol.tags @> 'highway=>road' OR ol.tags @> 'highway=>motorway' OR ol.tags @> 'highway=>motorway_link' OR ol.tags @> 'highway=>motorway_junction' OR ol.tags @> 'highway=>trunk' OR ol.tags @> 'highway=>trunk_link' OR ol.tags @> 'highway=>primary' OR ol.tags @> 'highway=>primary_link' OR ol.tags @> 'highway=>secondary' OR ol.tags @> 'highway=>secondary_link' OR ol.tags @> 'highway=>tertiary' OR ol.tags @> 'highway=>tertiary_link' OR ol.tags @> 'highway=>residential' OR ol.tags @> 'highway=>living_street' OR ol.tags @> 'highway=>service' OR ol.tags @> 'highway=>track' OR ol.tags @> 'highway=>pedestrian' OR ol.tags @> 'highway=>bus_guideway' OR ol.tags @> 'highway=>path' OR ol.tags @> 'highway=>cycleway' OR ol.tags @> 'highway=>footway' OR ol.tags @> 'highway=>bridleway' OR ol.tags @> 'highway=>byway' OR ol.tags @> 'highway=>steps' OR ol.tags @> 'highway=>unclassified' OR ol.tags @> 'junction=>roundabout' OR ol.tags @> 'highway=>construction' OR ol.tags @> 'highway=>proposed' ) GROUP BY ol.osm_id,ol.way,ol.tags;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("WITH simplifiedPolygons AS ( SELECT osm_id, tags, ST_ExteriorRing((ST_Dump(way)).geom) AS way FROM {0}.%s_polygon ) INSERT INTO {1}.%s_ways (osm_id, \"name\", the_geom) SELECT ol.osm_id,ol.tags->'name',(ST_Dump(ST_Split(ST_Transform(ol.way,4326),ST_Collect(jt.the_geom)))).geom FROM simplifiedPolygons AS ol JOIN {1}.%s_ways_vertices AS jt ON jt.the_geom && ST_Transform(ol.way,4326) WHERE jt.junction IS TRUE AND ( ol.tags @> 'highway=>road' OR ol.tags @> 'highway=>motorway' OR ol.tags @> 'highway=>motorway_link' OR ol.tags @> 'highway=>motorway_junction' OR ol.tags @> 'highway=>trunk' OR ol.tags @> 'highway=>trunk_link' OR ol.tags @> 'highway=>primary' OR ol.tags @> 'highway=>primary_link' OR ol.tags @> 'highway=>secondary' OR ol.tags @> 'highway=>secondary_link' OR ol.tags @> 'highway=>tertiary' OR ol.tags @> 'highway=>tertiary_link' OR ol.tags @> 'highway=>residential' OR ol.tags @> 'highway=>living_street' OR ol.tags @> 'highway=>service' OR ol.tags @> 'highway=>track' OR ol.tags @> 'highway=>pedestrian' OR ol.tags @> 'highway=>bus_guideway' OR ol.tags @> 'highway=>path' OR ol.tags @> 'highway=>cycleway' OR ol.tags @> 'highway=>footway' OR ol.tags @> 'highway=>bridleway' OR ol.tags @> 'highway=>byway' OR ol.tags @> 'highway=>steps' OR ol.tags @> 'highway=>unclassified' OR ol.tags @> 'junction=>roundabout' OR ol.tags @> 'highway=>construction' OR ol.tags @> 'highway=>proposed' ) GROUP BY ol.osm_id,ol.way,ol.tags;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("CREATE INDEX ways_the_geom_idx ON {0}.%s_ways USING gist (the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET \"length\" = ST_Length(the_geom),length_m = ST_length(geography(ST_Transform(the_geom, 4326))),x1 = ST_X(ST_StartPoint(ST_Transform(the_geom, 4326))),y1 = ST_Y(ST_StartPoint(ST_Transform(the_geom, 4326))),x2 = ST_X(ST_EndPoint(ST_Transform(the_geom, 4326))),y2 = ST_Y(ST_EndPoint(ST_Transform(the_geom, 4326))),one_way = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 1 WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 2 WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN -1 WHEN l.oneway is null THEN 0 END, oneway = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 'YES' WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 'NO' WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN 'REVERSED' WHEN l.oneway is null THEN 'UNKNOWN' END, maxspeed_forward = CASE WHEN l.tags -> 'maxspeed:forward' IS NOT NULL AND l.tags -> 'maxspeed:forward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:forward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, maxspeed_backward = CASE WHEN l.tags -> 'maxspeed:backward' IS NOT NULL AND l.tags -> 'maxspeed:backward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:backward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, priority = CASE WHEN l.tags -> 'priority' IS NOT NULL THEN 1 ELSE 0 END FROM {1}.%s_line l WHERE w.osm_id = l.osm_id;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),))
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET \"length\" = ST_Length(the_geom),length_m = ST_length(geography(ST_Transform(the_geom, 4326))),x1 = ST_X(ST_StartPoint(ST_Transform(the_geom, 4326))),y1 = ST_Y(ST_StartPoint(ST_Transform(the_geom, 4326))),x2 = ST_X(ST_EndPoint(ST_Transform(the_geom, 4326))),y2 = ST_Y(ST_EndPoint(ST_Transform(the_geom, 4326))),one_way = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 1 WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 2 WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN -1 WHEN l.oneway is null THEN 0 END, oneway = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 'YES' WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 'NO' WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN 'REVERSED' WHEN l.oneway is null THEN 'UNKNOWN' END, maxspeed_forward = CASE WHEN l.tags -> 'maxspeed:forward' IS NOT NULL AND l.tags -> 'maxspeed:forward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:forward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, maxspeed_backward = CASE WHEN l.tags -> 'maxspeed:backward' IS NOT NULL AND l.tags -> 'maxspeed:backward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:backward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, priority = CASE WHEN l.tags -> 'priority' IS NOT NULL THEN 1 ELSE 0 END FROM {1}.%s_polygon l WHERE w.osm_id = l.osm_id;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),))
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways SET cost = CASE WHEN oneway = 'REVERSED' THEN -length ELSE length END, reverse_cost = CASE WHEN oneway = 'YES' THEN -length ELSE length END, cost_s = CASE WHEN one_way = -1 THEN -ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_forward::float * 5.0 / 18.0) ELSE ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) END, reverse_cost_s = CASE WHEN one_way = 1 THEN -ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) ELSE ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) END WHERE maxspeed_backward !=0 AND maxspeed_forward != 0;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))  
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET source = v.gid FROM {0}.%s_ways_vertices v WHERE w.x1 = v.lon AND w.y1 = v.lat;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET target = v.gid FROM {0}.%s_ways_vertices v WHERE w.x2 = v.lon AND w.y2 = v.lat;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("ALTER TABLE {0}.%s_ways ADD CONSTRAINT %s_ways_source_fkey FOREIGN KEY (\"source\") REFERENCES {0}.%s_ways_vertices(gid);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
    cursor.execute(sql.SQL("ALTER TABLE {0}.%s_ways ADD CONSTRAINT %s_ways_target_fkey FOREIGN KEY (target) REFERENCES {0}.%s_ways_vertices(gid);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))

    conn.commit()
except Exception as e:
    logging.critical('PostgreSQL error : ' + str(e))
    sys.exit(1)

# Ferme la connexion PG
try:
    logging.info('Close database connection.')
    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Error when closing PG : ' + str(e))
    sys.exit(1)
