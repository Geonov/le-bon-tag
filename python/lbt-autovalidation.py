#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, os, requests, psycopg2, gzip, time, logging, sys, re, subprocess, shutil, platform, json
    from pathlib import Path
    from datetime import date, datetime, timezone
    from copy import deepcopy
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
except Exception as e:
    print(e)
    sys.exit(1)

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Date actuelle
# 2020-01-16 09:18:39
end_date = datetime.utcnow().replace(microsecond=0)
end_date_str = end_date.strftime("%Y%m%d")

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-autovalidation/' + str(end_date.year) + str('%02d' % end_date.month) + '/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + 'autovalidation_' + end_date_str + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Passe tous les objets osmdiff actuellement en l'état 'tovalidate' à l'état 'valid' pour les contributeurs à validation automatique
try:
    logging.info('Update lbt_osmdiff with autovalidated contributors...')
    cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_status = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'valid') WHERE od_new_ct_id IN (SELECT ct_id FROM {0}.lbt_contributor WHERE ct_auto_validation is true) AND od_status = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'tovalidate');").format(sql.Identifier(lbtSchema)))
    updated_rows = cursor.rowcount
    conn.commit()
    logging.info(str(updated_rows) + ' row(s) updated.')
except Exception as e:
    logging.critical('Autovalidation (SQL UPDATE) error : ' + str(e))
    sys.exit(1)

# Passe tous les objets osmdiff actuellement en l'état 'tovalidate' à l'état 'valid' pour les objets appartenant à des groupes d'objets à validation automatique
try:
    logging.info('Get JSON queries for objects groups with auto-validation active...')

    # Quel est le premier item de la requête
    def getFirstOperator(parsedQuery):
        firstOperator = next(iter(parsedQuery))
        if firstOperator == '[op.or]':
            return '[op.or]'
        elif firstOperator == '[op.and]':
            return '[op.and]'
        else:
            return False

    # Convertit la requête en SQL (fonction inutilisée mais conservée pour référence)
    def translateQuerySql(parsedQuery):
        # Récupère le 1er élément
        firstOp = getFirstOperator(parsedQuery)
        if firstOp == '[op.or]':
            sqlOp = 'OR'
        elif firstOp == '[op.and]':
            sqlOp = 'AND'
        else:
            sqlOp = None
        query = ''
        if type(parsedQuery[firstOp]) is list and sqlOp != None:
            for i in range(len(parsedQuery[firstOp])):
                node = parsedQuery[firstOp][i]
                key = list(node.keys())[0]
                val = node[key]
                if (i > 0):
                    query += ' ' + sqlOp + ' '
                if type(val) is str:
                    query += key + '=' + val
                elif type(val) is dict:
                    if '[op.ne]' in val:
                        opVal = val['[op.ne]']
                        query += key + '!=' + opVal
                    elif '[op.gt]' in val:
                        opVal = val['[op.gt]']
                        query += key + '>' + opVal
                    elif '[op.gte]' in val:
                        opVal = val['[op.gte]']
                        query += key + '>=' + opVal
                    elif '[op.lt]' in val:
                        opVal = val['[op.lt]']
                        query += key + '<' + opVal
                    elif '[op.lte]' in val:
                        opVal = val['[op.lte]']
                        query += key + '<=' + opVal
                elif type(val) is list:
                    query += '(' + translateQuerySql(node) + ')'
                else:
                    logging.critical('Type not handled: ' + str(type(val)))
        return query

    # Convertit la requête en SQL avec Hstore
    def translateQuery(parsedQuery):
        # Récupère le 1er élément
        firstOp = getFirstOperator(parsedQuery)
        if firstOp == '[op.or]':
            sqlOp = 'OR'
        elif firstOp == '[op.and]':
            sqlOp = 'AND'
        else:
            sqlOp = None
        query = ''
        if type(parsedQuery[firstOp]) is list and sqlOp != None:
            for i in range(len(parsedQuery[firstOp])):
                node = parsedQuery[firstOp][i]
                key = list(node.keys())[0]
                val = node[key]
                if (i > 0):
                    query += ' ' + sqlOp + ' '
                if type(val) is str:
                    if val == '*':
                        query += '(od_new_tags || od_old_tags) ? \'' + key + '\''
                    else:
                        query += '(od_new_tags || od_old_tags) @> \'' + key + '=>' + val + '\''
                elif type(val) is dict:
                    if '[op.ne]' in val:
                        opVal = val['[op.ne]']
                        query += 'not (od_new_tags || od_old_tags) @> \'' + key + '=>' + opVal + '\''
                    elif '[op.gt]' in val:
                        opVal = val['[op.gt]']
                        query += '1 = 1'
                    elif '[op.gte]' in val:
                        opVal = val['[op.gte]']
                        query += '1 = 1'
                    elif '[op.lt]' in val:
                        opVal = val['[op.lt]']
                        query += '1 = 1'
                    elif '[op.lte]' in val:
                        opVal = val['[op.lte]']
                        query += '1 = 1'
                elif type(val) is list:
                    query += '(' + translateQuery(node) + ')'
                else:
                    logging.critical('Type not handled: ' + str(type(val)))
        return query

    # Récupère les requêtes des groupes d'objets à validation auto
    logging.info('Update lbt_osmdiff with autovalidated groups...')
    cursor.execute(sql.SQL("SELECT og_query_json FROM {0}.lbt_objectgroup WHERE og_active IS TRUE AND og_auto IS TRUE;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()

    for row in rows:
        # Charge la requête dans un dict
        parsedQuery = json.loads(row[0])
        logging.info('Original query: ' + str(parsedQuery))
        # Convertit la requête
        sqlQuery = translateQuery(parsedQuery)
        logging.info('SQL query: ' + str(sqlQuery))
        # Mise à jour en validation de tous les objets concernés par cette requête
        cursor.execute(sql.SQL("UPDATE {schema}.lbt_osmdiff SET od_status = (SELECT st_id FROM {schema}.lbt_status WHERE st_name = 'valid') WHERE " + sqlQuery + " AND od_status = (SELECT st_id FROM {schema}.lbt_status WHERE st_name = 'tovalidate');").format(schema=sql.Identifier(lbtSchema)))
        updated_rows = cursor.rowcount
        conn.commit()
        logging.info(str(updated_rows) + ' row(s) updated.')

    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Autovalidation (SQL FETCH og_query_json) error : ' + str(e))
    sys.exit(1)

logging.info('Python script ended.')
