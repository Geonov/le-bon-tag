#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Script de mise à jour v0.23
# Modifie les fragments OSC dans la table osm_diff en ajoutant tous les nodes en mode "modify" pour les ways (automatique à partir de la v0.22.6)

# Imports
try:
    import yaml, os, requests, psycopg2, gzip, time, logging, sys, re, subprocess, shutil, platform
    from pathlib import Path
    from datetime import date, datetime, timezone
    from copy import deepcopy
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
except Exception as e:
    print(e)
    sys.exit(1)

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + 'update_0.23.0.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Environnement
platform = platform.system()
logging.info('OS: ' + platform)
current_directory = os.getcwd().replace(os.sep, '/')
logging.info('Current directory: ' + current_directory)
work_directory = str(Path(current_directory).parent.absolute()).replace(os.sep, '/')
logging.info('Work directory: ' + work_directory)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Récupère les paramètres stockés en base de données
try:
    cursor.execute(sql.SQL("SELECT s_name,s_value FROM {0}.lbt_setting;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    drows = dict(rows)
    app_build = drows['app_build']
    app_name = drows['app_name']
    app_release_date = drows['app_release_date']
    db_ref_srid = int(drows['db_ref_srid'])
    db_ref_schema = drows['db_ref_schema']
    db_ref_prefix = drows['db_ref_prefix']
    geom_tolerance = drows['geom_tolerance']
    osm_pbffile = drows['osm_pbffile']
    osm_footprint = drows['osm_footprint']
    osm_overpass_url = drows['osm_overpass_url']
    osm_api_url = drows['osm_api_url']
    osm_diff_ways_date = drows['osm_diff_ways_date']
    osm_diff_ways_enable = drows['osm_diff_ways_enable']
    api_max_size = drows['api_max_size']
except Exception as e:
    logging.critical('LeBonTag settings fetching error : ' + str(e))
    sys.exit(1)

try:
    cursor.execute(sql.SQL("SELECT od_id,od_osc FROM {0}.lbt_osmdiff WHERE od_osc IS NOT NULL AND od_id_nodes IS NOT NULL AND od_element='way' AND od_action='modify';").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    if cursor.rowcount > 0:
        for row in rows:
            od_id = row[0]
            xmlFragment = row[1].encode('UTF-8')
            root = ET.fromstring(xmlFragment)
            topNodes = ET.SubElement(root, 'modify')
            for nd in root.findall(".//nd"):
                id = nd.get('ref')
                lat = nd.get('lat')
                lon = nd.get('lon')
                if id is not None and lat is not None and lon is not None:
                    topNodesElement = ET.SubElement(topNodes, 'node', attrib={"id": id, "lat": lat, "lon": lon})
            od_osc = ET.tostring(root, encoding='UTF-8', method="xml", pretty_print=False).decode('UTF-8')
            od_osc = re.sub('\s+',' ',od_osc)
            od_osc = od_osc.strip()
            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_osc = %s WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_osc,od_id,))

except Exception as e:
    logging.critical('Error with osc update : ' + str(od_id) + ' : ' + str(e))
    sys.exit(1)

try:
    conn.commit()
except Exception as e:
    logging.critical('Error when committing osc update : ' + str(e))
    sys.exit(1)

# ===
# Fin
# ===

# Ferme la connexion PG
try:
    logging.info('Close database connection.')
    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Error when closing PG : ' + str(e))
    sys.exit(1)

logging.info('Python script ended.')
