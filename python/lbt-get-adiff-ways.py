#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, os, requests, psycopg2, gzip, time, logging, sys, re, subprocess, platform
    from pathlib import Path
    from datetime import datetime, timezone, timedelta
    from copy import deepcopy
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
except Exception as e:
    print(e)
    sys.exit(1)

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Date actuelle en UTC
# 2020-01-16 09:18:39
# On retire 2 heures à la date UTC (pour s'assurer que l'API a eu le temps de se mettre à jour à cette date)
# end_date = datetime.utcnow().replace(microsecond=0)
end_date = datetime.utcnow().replace(microsecond=0) - timedelta(hours=2)
#Nomme le fichier téléchargé avec l'heure sans minutes, donc pas plus d'un appel par heure à l'API
#end_date_str = end_date.strftime("%Y%m%d-%H%M%S")
end_date_str = end_date.strftime("%Y%m%d-%H")

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-get-adiff/' + str(end_date.year) + str('%02d' % end_date.month) + '/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + 'ways_' + end_date_str + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Environnement
platform = platform.system()
logging.info('OS: ' + platform)
current_directory = os.getcwd().replace(os.sep, '/')
logging.info('Current directory: ' + current_directory)
work_directory = str(Path(current_directory).parent.absolute()).replace(os.sep, '/')
logging.info('Work directory: ' + work_directory)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Récupère les paramètres stockés en base de données
try:
    cursor.execute(sql.SQL("SELECT s_name,s_value FROM {0}.lbt_setting;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    drows = dict(rows)
    app_build = drows['app_build']
    app_name = drows['app_name']
    app_release_date = drows['app_release_date']
    db_ref_srid = int(drows['db_ref_srid'])
    db_ref_schema = drows['db_ref_schema']
    db_ref_prefix = drows['db_ref_prefix']
    geom_tolerance = drows['geom_tolerance']
    osm_pbffile = drows['osm_pbffile']
    osm_footprint = drows['osm_footprint']
    osm_overpass_url = drows['osm_overpass_url']
    osm_api_url = drows['osm_api_url']
    osm_diff_ways_date = drows['osm_diff_ways_date']
    osm_diff_ways_enable = drows['osm_diff_ways_enable']
    api_max_size = drows['api_max_size']
    osm_diff_ways_attempts = drows['osm_diff_ways_attempts']
except Exception as e:
    logging.critical('LeBonTag settings fetching error : ' + str(e))
    sys.exit(1)

# Faut-il charger les ways ?
try:
    if osm_diff_ways_enable == '0':
        logging.info('Ways loading disabled in settings. Exiting.')
        sys.exit(0)
except Exception as e:
    logging.critical('Settings error : ' + str(e))
    sys.exit(1)

# Headers pour les appels API
vHeaders = {}
vHeaders['User-Agent'] = app_name + '/' + app_build + ' (' + app_release_date + ')'
vHeaders['Referer'] = 'https://www.lebontag.fr/'
vHeaders['From'] = 'contact@geonov.fr'

# Date des dernières données de diff chargées (si 1er chargement, on utilise la date des données de référence dont le chemin du fichier est en paramètre)
# 2019-07-01T20:15:02Z via le PBF / 2019-07-01 20:15:02 en base
if osm_diff_ways_date:
    start_date = datetime.strptime(osm_diff_ways_date,'%Y-%m-%d %H:%M:%S')
else:
    try:
        if platform == 'Windows':
            start_date_from_pbf = subprocess.run([work_directory + '/tools/osmconvert/osmconvert64-0.8.8p.exe', osm_pbffile, "--out-timestamp"], stdout=subprocess.PIPE).stdout.replace(b'\r\n', b'').decode('utf-8')
        else:
            start_date_from_pbf = subprocess.run(["osmconvert", osm_pbffile, "--out-timestamp"], stdout=subprocess.PIPE).stdout.replace(b'\n', b'').decode('utf-8')
        start_date = datetime.strptime(start_date_from_pbf,'%Y-%m-%dT%H:%M:%SZ')
    except Exception as e:
        logging.critical('LeBonTag osm_diff_ways_date fetching error : ' + str(e))
        sys.exit(1)

# Calcule la BBOX de l'emprise
osm_bbox = None
if osm_footprint is not None:
    cursor.execute(sql.SQL("SELECT CONCAT_WS(',',ST_ymin(fp_geom), ST_xmin(fp_geom), ST_ymax(fp_geom), ST_xmax(fp_geom)) FROM {0}.lbt_footprint WHERE fp_id = %s;").format(sql.Identifier(lbtSchema)),(osm_footprint,))
    # < LBT 0.21
    # cursor.execute(sql.SQL("SELECT CONCAT_WS(',',ST_ymin(ST_GeomFromGeoJSON(%s)), ST_xmin(ST_GeomFromGeoJSON(%s)), ST_ymax(ST_GeomFromGeoJSON(%s)), ST_xmax(ST_GeomFromGeoJSON(%s)));"),(osm_footprint,osm_footprint,osm_footprint,osm_footprint,))
    row = cursor.fetchone()
    if row is not None:
        osm_bbox = row[0]

# Récupère la GEOM de l'emprise
osm_footprint_geom = None
if osm_footprint is not None:
    cursor.execute(sql.SQL("SELECT ST_MakeValid(fp_geom) FROM {0}.lbt_footprint WHERE fp_id = %s;").format(sql.Identifier(lbtSchema)),(osm_footprint,))
    # < LBT 0.21
    # cursor.execute(sql.SQL("SELECT ST_GeomFromGeoJSON(%s);"),(osm_footprint,))
    row = cursor.fetchone()
    if row is not None:
        osm_footprint_geom = row[0]

# Dictionnaires et listes (pour les contributeurs, les tags et les changesets)
contributorsDict = {}
tagsList = []
changesetsList = []

# Lignes et polygones
# Si l'un de ces tags est présent et que le tag area n'est pas à false, la way est un polygone
polygon_keys = ['aeroway','amenity','building','harbour','historic','landuse','leisure','man_made','military','natural','office','place','power','public_transport','shop','sport','tourism','water','waterway','wetland']

# Si la différence de date est supérieure à 24H (par exemple en cas de problème de chargement la fois précédente), on limite l'appel à l'API à une période de 24H
delta_date = end_date - start_date
if delta_date.total_seconds()/3600 > 24:
    logging.warning('The dates differ by more than 24H. Limits API call to 24H.')
    end_date = start_date + timedelta(hours=24) + timedelta(hours=int(osm_diff_ways_attempts))
    end_date_str = end_date.strftime("%Y%m%d-%H")

# ADIFF depuis la dernière MAJ
try:
    logging.info('ADIFF file parsing from ' + str(start_date) + ' to ' + str(end_date))
    # Répertoire de sortie du ADIFF
    adiffDirectory = '../osm/adiff/' + str(end_date.year) + str('%02d' % end_date.month) + '/'
    if not os.path.exists(adiffDirectory):
        os.makedirs(adiffDirectory)
    adiffFile = adiffDirectory + 'ways_' + end_date_str + '.xml.gz'
    # Appel de l'API puis lecture du XML (6 tentatives)
    numOfRequests = 6
    fileValid = None
    for i in range(numOfRequests):
        rData = '[out:xml][maxsize:' + str(api_max_size) + '][adiff:"' + str(start_date) + '","' + str(end_date) + '"];way(' + osm_bbox + ');out geom meta;'
        logging.info('Overpass request data : ' + rData)
        r = requests.post(osm_overpass_url,data=rData,allow_redirects=True,verify=True,headers=vHeaders,timeout=(300,300))
        if r.status_code == 200:
            r.encoding = 'UTF-8'
            # Lit la donnée avec le parser HTML (contrôle si l'API a renvoyé du HTML, donc est en erreur)
            root_html = ET.fromstring(bytes(r.text, encoding='UTF-8'),ET.HTMLParser())
            if len(root_html.findall(".//title")) != 0:
                fileValid = 0
                logging.warning(': adiff contains "title" element (html), retrying in 300 seconds')
                time.sleep(300)
            else:
                # Lit la donnée XML déjà en mémoire
                root = ET.fromstring(bytes(r.text, encoding='UTF-8'))
                # OK si pas d'élément "remark" sinon le XML est sans doute vide (exemple "<remark> runtime error: Query run out of memory using about 3072 MB of RAM. </remark>")
                if len(root.findall(".//remark")) == 0:
                    fileValid = 1
                    logging.info('200: adiff ' + 'ways_' + end_date_str + '.xml.gz successfully fetched')
                    break
                else:
                    fileValid = 0
                    logging.warning(': adiff contains "remark" element, retrying in 300 seconds')
                    time.sleep(300)
        else:
            fileValid = 0
            logging.warning(str(r.status_code) + ': adiff not fetched, retrying in 300 seconds')
            time.sleep(300)

    # On stoppe si toujours pas de fichier valide après la boucle 6 tentatives
    if fileValid == 0:
        raise Exception('adiff file not fetched after 6 attempts')

    # Compresse et écrit le fichier
    with gzip.open(adiffFile, 'wb') as f:
        f.write(r.content)

    # Si aucune way dans le fichier en mémoire (aucune donnée), on arrête le traitement et on ne met pas à jour la date de traitement
    totalWays = len(root.findall(".//action[@type='create']/way")) + len(root.findall(".//action[@type='delete']/new/way")) + len(root.findall(".//action[@type='modify']/new/way"))
    logging.info(str(totalWays) + ' way(s) found')
    if totalWays < 1:
        if delta_date.total_seconds()/3600 > 24:
            cursor.execute(sql.SQL("UPDATE {0}.lbt_setting SET s_value = %s WHERE s_name = 'osm_diff_ways_attempts';").format(sql.Identifier(lbtSchema)),(int(osm_diff_ways_attempts)+2,))
            conn.commit()
            cursor.close()
            conn.close() 
        logging.warning('No ways found. Processing stops. The update date remains unchanged.')
        sys.exit(1)
    else:
        cursor.execute(sql.SQL("UPDATE {0}.lbt_setting SET s_value = %s WHERE s_name = 'osm_diff_ways_attempts';").format(sql.Identifier(lbtSchema)),(0,))
        conn.commit()

    # ===========
    # ways créées
    # ===========
    logging.info('Parse created ways...')
    logging.info(str(len(root.findall(".//action[@type='create']/way"))) + ' created ways found')

    try:

        for object in root.findall(".//action[@type='create']/way"):
            od_id = object.get('id')
            od_action = 'create'
            od_element = 'way'
            od_new_version = object.get('version')
            od_new_timestamp = object.get('timestamp')
            od_new_changeset = object.get('changeset')
            od_new_ct_id = object.get('uid')
            od_new_username = object.get('user')
            od_new_tags = ''
            od_new_geom = None
            #od_new_lat = object.get('lat')
            #od_new_lon = object.get('lon')
            od_old_version = None
            od_old_timestamp = None
            od_old_changeset = None
            od_old_ct_id = None
            od_old_username = None
            od_old_tags = ''
            od_old_geom = None
            #od_old_lat = None
            #od_old_lon = None
            od_added_tags = ''
            od_deleted_tags = ''
            od_modified_tags = ''
            od_unchanged_tags = ''
            od_name = None
            od_changed_geom = True
            od_id_nodes = []

            # Si l'objet est déjà dans LeBonTag, on ne fait rien, sinon on continue
            cursor.execute(sql.SQL("SELECT od_id FROM {0}.lbt_osmdiff WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
            row = cursor.fetchone()
            if row is None:

                # Tags et géométrie
                is_polygon_tag = 0
                is_polygon_forced = -1
                osm_coord = ''
                # Récupère les nouveaux tags (à faire d'abord, pour savoir si l'objet est un polygone)
                tagsCount = 0
                for tag in object.findall(".//tag"):
                    tagsCount += 1
                    k = tag.get('k')
                    v = tag.get('v')
                    if k in polygon_keys:
                        is_polygon_tag = 1
                    if k == 'area' and v in ['yes','1','true']:
                        is_polygon_forced = 1
                    elif k == 'area' and v in ['no','0','false']:
                        is_polygon_forced = 0
                    kvtag = "=>".join(['"'+k.replace('"','\\"')+'"','"'+v.replace('"','\\"')+'"'])
                    tagsList.append(k)
                    if tagsCount == 1:
                        od_new_tags = "".join([od_new_tags,kvtag])
                    else:
                        od_new_tags = ",".join([od_new_tags,kvtag])
                    if k == 'name':
                        od_name = v
                # Géométrie depuis les nœuds
                ndCount = 0
                insideWay = False
                for nd in object.findall(".//nd"):
                    osm_lat = nd.get('lat')
                    osm_lon = nd.get('lon')

                    #Si au moins l'un des nœuds est dans l'emprise, il faut garder l'objet (insideWay = True)
                    #20210224 : librairie Shapely remplacée par du SQL
                    if osm_lat is not None and osm_lon is not None:
                        node_newgeom_inside = None
                        cursor.execute(sql.SQL("SELECT ST_Within(ST_SetSRID(ST_MakePoint(%s,%s),%s),%s);"),(osm_lon,osm_lat,lbtSrid,osm_footprint_geom,))
                        row = cursor.fetchone()
                        if row is not None:
                            node_newgeom_inside = row[0]
                        if node_newgeom_inside is True:
                            insideWay = True

                        ndCount += 1
                        lonlat = " ".join([osm_lon,osm_lat])
                        osm_coord = ",".join([osm_coord,lonlat])

                # Polygone ou ligne ?
                # Et forcément > 1 nœud + dans l'emprise
                if ndCount > 1 and insideWay is True:
                    # Si les points de début et de fin ont des coordonnées différentes en X ou en Y, ça ne peut pas être un polygone
                    if object.find(".//nd[1]").get('lat') != object.find(".//nd[last()]").get('lat') or object.find(".//nd[1]").get('lon') != object.find(".//nd[last()]").get('lon'):
                        is_polygon_forced = 0
                    # Polygone
                    if (is_polygon_tag == 1 and is_polygon_forced != 0) or (is_polygon_tag == 0 and is_polygon_forced == 1):
                        polygonWay = True
                    # Ligne
                    elif (is_polygon_tag == 0 and is_polygon_forced != 1) or (is_polygon_tag == 1 and is_polygon_forced == 0):
                        polygonWay = False

                    # Génère le fragment OSC XML pour cet élément + la liste des nodes
                    # + Ajoute tous les nodes en mode modify 2024/01/22
                    top = ET.Element('osmChange')
                    top.set('version', '0.6')
                    top.set('generator','LeBonTag')
                    topNodes = ET.SubElement(top, 'modify')
                    topChild = ET.SubElement(top, 'create')
                    topChildElement = ET.SubElement(topChild, 'way', attrib=object.attrib)
                    for nd in object.findall(".//nd"):
                        nodeId = int(nd.get('ref'))
                        if nodeId not in od_id_nodes:
                            od_id_nodes.append(nodeId)
                        topChildElement.append(deepcopy(nd))
                        topNodesElement = ET.SubElement(topNodes, 'node', attrib={"id": nd.get('ref'), "lat": nd.get('lat'), "lon": nd.get('lon')})
                    for tag in object.findall(".//tag"):
                        topChildElement.append(deepcopy(tag))
                    od_osc = ET.tostring(top, encoding='UTF-8', method="xml", pretty_print=False).decode('UTF-8')
                    od_osc = re.sub('\s+',' ',od_osc)
                    od_osc = od_osc.strip()
                    od_id_nodes.sort(key=int)

                    # Ajoute le contributeur au dictionnaire et le changeset à la liste
                    contributorsDict[int(od_new_ct_id)] = od_new_username
                    changesetsList.append(od_new_changeset)

                    # Est-ce que l'objet existe déjà en base de référence (line ou polygon) ?
                    # Si non, on ajoute l'objet dans lbt_osmdiff
                    # Si oui, on ne fait rien (l'objet existe déjà forcément en version 1 ou supérieure)
                    cursor.execute(sql.SQL("SELECT osm_id FROM {0}.{1} WHERE osm_id = %s UNION SELECT osm_id FROM {0}.{2} WHERE osm_id = %s;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_line'),sql.Identifier(db_ref_prefix + '_polygon')),(od_id,od_id,))
                    row = cursor.fetchone()
                    if row is None:
                        # UPSERT dans la base (la way vient d'être créée mais UPSERT par sécurité au cas où le chargement aurait déjà été fait)
                        cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmdiff (od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc,od_id_nodes) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (od_id,od_new_version) DO UPDATE SET od_name = EXCLUDED.od_name,od_action = EXCLUDED.od_action,od_element = EXCLUDED.od_element,od_new_version = EXCLUDED.od_new_version,od_new_timestamp = EXCLUDED.od_new_timestamp,od_new_changeset = EXCLUDED.od_new_changeset,od_new_ct_id = EXCLUDED.od_new_ct_id,od_new_tags = EXCLUDED.od_new_tags,od_new_geom = EXCLUDED.od_new_geom,od_old_version = EXCLUDED.od_old_version,od_old_timestamp = EXCLUDED.od_old_timestamp,od_old_changeset = EXCLUDED.od_old_changeset,od_old_ct_id = EXCLUDED.od_old_ct_id,od_old_tags = EXCLUDED.od_old_tags,od_old_geom = EXCLUDED.od_old_geom,od_added_tags = EXCLUDED.od_added_tags,od_deleted_tags = EXCLUDED.od_deleted_tags,od_modified_tags = EXCLUDED.od_modified_tags,od_unchanged_tags = EXCLUDED.od_unchanged_tags,od_changed_geom = EXCLUDED.od_changed_geom,od_osc = EXCLUDED.od_osc,od_id_nodes = EXCLUDED.od_id_nodes;").format(sql.Identifier(lbtSchema)),(od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc,od_id_nodes,))
                        # UPDATE la géométrie (polygone ou ligne selon les cas)
                        if polygonWay is True:
                            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_new_geom = ST_GeomFromText('POLYGON((%s))',%s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(AsIs(osm_coord[1:]),lbtSrid,od_id,))
                        elif polygonWay is False:
                            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_new_geom = ST_GeomFromText('LINESTRING(%s)',%s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(AsIs(osm_coord[1:]),lbtSrid,od_id,))
                        # UPDATE "od_mapillary" à partir du tag "mapillary"
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_mapillary = replace(od_new_tags -> 'mapillary','https://www.mapillary.com/map/im/','') WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Calcule les bounds
                        cursor.execute(sql.SQL("WITH extent_req AS ( SELECT ST_Extent(ST_Collect(od_new_geom,od_old_geom)) AS geom FROM {0}.lbt_osmdiff WHERE od_id = %s) UPDATE {0}.lbt_osmdiff SET od_extent = (SELECT CONCAT_WS(';',ST_XMin(geom), ST_YMin(geom), ST_XMax(geom), ST_YMax(geom)) FROM extent_req) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))
                        # Met à jour l'état de l'objet (pour la fonction d'export)
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_state = (SELECT state_id FROM {0}.lbt_state WHERE state_name = 'added') WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Comparaison : les tags ajoutés sont forcément tous les tags (il n'y a pas de tags supprimés, modifiés ou conservés)
                        if od_new_tags is not None:
                            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_added_tags = (SELECT delete(od_new_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) FROM {0}.lbt_osmdiff WHERE od_id = %s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))

                        # Log des osm id
                        logging.info(od_id)

    except Exception as e:
        logging.critical('Error with created ways : ' + str(od_id) + ' : ' + str(e))
        sys.exit(1)

    # ===============
    # ways supprimées
    # ===============
    logging.info('Parse deleted ways...')
    logging.info(str(len(root.findall(".//action[@type='delete']/new/way"))) + ' deleted ways found')

    try:

        for object in root.findall(".//action[@type='delete']/new/way"):
            od_id = object.get('id')
            od_action = 'delete'
            od_element = 'way'
            od_new_version = object.get('version')
            od_new_timestamp = object.get('timestamp')
            od_new_changeset = object.get('changeset')
            od_new_ct_id = object.get('uid')
            od_new_username = object.get('user')
            od_new_tags = ''
            od_new_geom = None
            #od_new_lat = None
            #od_new_lon = None
            od_old_version = None
            od_old_timestamp = None
            od_old_changeset = None
            od_old_ct_id = None
            od_old_username = None
            od_old_tags = ''
            od_old_geom = None
            #od_old_lat = None
            #od_old_lon = None
            od_added_tags = ''
            od_deleted_tags = ''
            od_modified_tags = ''
            od_unchanged_tags = ''
            od_name = None
            od_changed_geom = True

            # Si l'objet est déjà dans LeBonTag avec l'action "delete", on ne fait rien, sinon on continue
            cursor.execute(sql.SQL("SELECT od_id FROM {0}.lbt_osmdiff WHERE od_id = %s AND od_action = 'delete';").format(sql.Identifier(lbtSchema)),(od_id,))
            row = cursor.fetchone()
            if row is None:

                # Si une version précédente de l'objet est présente dans osmdiff (par exemple un create ou un modify), on l'historise puis on la DELETE. Les références à ces objets sont supprimées de lbt_validation en cascade (ils ne sont plus à valider).
                cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmdiff_history (SELECT * FROM {0}.lbt_osmdiff WHERE od_id = %s) ON CONFLICT (od_id,od_new_version) DO NOTHING;").format(sql.Identifier(lbtSchema)),(od_id,))
                cursor.execute(sql.SQL("DELETE FROM {0}.lbt_osmdiff WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))

                # Récupère les propriétés de l'ancienne version dans la BDD de référence (line ou polygon)
                cursor.execute(sql.SQL("SELECT (tags -> 'osm_version')::INTEGER AS od_old_version, CASE WHEN (tags -> 'osm_timestamp') = '' THEN NULL ELSE (tags -> 'osm_timestamp')::TIMESTAMP END AS od_old_timestamp, (tags -> 'osm_changeset')::BIGINT AS od_old_changeset, (tags -> 'osm_uid')::BIGINT AS od_old_ct_id, tags -> 'name' AS od_name, delete(tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) AS od_old_tags, ST_Transform(way,%s) AS od_old_geom FROM {0}.{1} WHERE osm_id = %s UNION SELECT (tags -> 'osm_version')::INTEGER AS od_old_version, CASE WHEN (tags -> 'osm_timestamp') = '' THEN NULL ELSE (tags -> 'osm_timestamp')::TIMESTAMP END AS od_old_timestamp, (tags -> 'osm_changeset')::BIGINT AS od_old_changeset, (tags -> 'osm_uid')::BIGINT AS od_old_ct_id, tags -> 'name' AS od_name, delete(tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) AS od_old_tags, ST_Transform(way,%s) AS od_old_geom FROM {0}.{2} WHERE osm_id = %s;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_line'),sql.Identifier(db_ref_prefix + '_polygon')),(lbtSrid,od_id,lbtSrid,od_id,))
                row = cursor.fetchone()

                # Si rien n'est trouvé c'est que l'objet n'est pas en base de référence (par exemple hors emprise) donc pas besoin de valider sa suppression, sinon on continue.
                if row is not None:
                    od_old_version = row[0]
                    od_old_timestamp = row[1]
                    od_old_changeset = row[2]
                    od_old_ct_id = row[3]
                    od_name = row[4]
                    od_old_tags = row[5]
                    od_old_geom = row[6]

                    #Est-ce que l'ancienne géométrie de la way intersecte l'emprise ? Si oui, on continue, si non, on arrête
                    #20210224 : librairie Shapely remplacée par du SQL
                    way_oldgeom_inside = None
                    cursor.execute(sql.SQL("SELECT ST_Intersects(%s,%s);"),(od_old_geom,osm_footprint_geom,))
                    row = cursor.fetchone()
                    if row is not None:
                        way_oldgeom_inside = row[0]
                    if way_oldgeom_inside is True:

                        # Génère le fragment OSC XML pour cet élément
                        # On ne met pas les nœuds, on ne sait pas s'ils sont supprimés ou pas
                        top = ET.Element('osmChange')
                        top.set('version', '0.6')
                        top.set('generator','LeBonTag')
                        topChild = ET.SubElement(top, 'delete')
                        topChildElement = ET.SubElement(topChild, 'way', attrib=object.attrib)
                        od_osc = ET.tostring(top, encoding='UTF-8', method="xml", pretty_print=False).decode('UTF-8')
                        od_osc = re.sub('\s+',' ',od_osc)
                        od_osc = od_osc.strip()

                        # Ajoute le contributeur au dictionnaire et le changeset à la liste
                        contributorsDict[int(od_new_ct_id)] = od_new_username
                        changesetsList.append(od_new_changeset)

                        # INSERT dans la base
                        cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmdiff (od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);").format(sql.Identifier(lbtSchema)),(od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc,))
                        # Calcule les bounds
                        cursor.execute(sql.SQL("WITH extent_req AS ( SELECT ST_Extent(ST_Collect(od_new_geom,od_old_geom)) AS geom FROM {0}.lbt_osmdiff WHERE od_id = %s) UPDATE {0}.lbt_osmdiff SET od_extent = (SELECT CONCAT_WS(';',ST_XMin(geom), ST_YMin(geom), ST_XMax(geom), ST_YMax(geom)) FROM extent_req) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))
                        # Met à jour l'état de l'objet (pour la fonction d'export)
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_state = (SELECT state_id FROM {0}.lbt_state WHERE state_name = 'deleted') WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Comparaison : les anciens tags sont forcément les tags supprimés (il n'y a pas de tags ajoutés, modifiés ou conservés)
                        if od_old_tags is not None:
                            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_deleted_tags = od_old_tags WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))

                        # Log des osm id
                        logging.info(od_id)

    except Exception as e:
        logging.critical('Error with deleted ways : ' + str(od_id) + ' : ' + str(e))
        sys.exit(1)

    # ==============
    # ways modifiées
    # ==============
    logging.info('Parse modified ways...')
    logging.info(str(len(root.findall(".//action[@type='modify']/new/way"))) + ' modified ways found')

    try:

        for object in root.findall(".//action[@type='modify']/new/way"):
            od_id = object.get('id')
            od_action = 'modify'
            od_element = 'way'
            od_new_version = object.get('version')
            od_new_timestamp = object.get('timestamp')
            od_new_changeset = object.get('changeset')
            od_new_ct_id = object.get('uid')
            od_new_username = object.get('user')
            od_new_tags = ''
            od_new_geom = None
            #od_new_lat = object.get('lat')
            #od_new_lon = object.get('lon')
            od_old_version = None
            od_old_timestamp = None
            od_old_changeset = None
            od_old_ct_id = None
            od_old_username = None
            od_old_tags = ''
            od_old_geom = None
            #od_old_lat = None
            #od_old_lon = None
            od_added_tags = ''
            od_deleted_tags = ''
            od_modified_tags = ''
            od_unchanged_tags = ''
            od_name = None
            od_changed_geom = None
            od_id_nodes = []

            # Si l'objet est déjà dans LeBonTag avec l'action "modify" et la même nouvelle version, on ne fait rien, sinon on continue
            cursor.execute(sql.SQL("SELECT od_id FROM {0}.lbt_osmdiff WHERE od_id = %s AND od_action = 'modify' AND od_new_version = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_new_version,))
            row = cursor.fetchone()
            if row is None:

                # Si une version précédente de l'objet est présente dans osmdiff (par exemple un create ou un modify), on l'historise puis on la DELETE. Les références à ces objets sont supprimées de lbt_validation en cascade (ils ne sont plus à valider).
                cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmdiff_history (SELECT * FROM {0}.lbt_osmdiff WHERE od_id = %s) ON CONFLICT (od_id,od_new_version) DO NOTHING;").format(sql.Identifier(lbtSchema)),(od_id,))
                cursor.execute(sql.SQL("DELETE FROM {0}.lbt_osmdiff WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))

                # Récupère les propriétés de l'ancienne version dans la BDD de référence (line ou polygon)
                cursor.execute(sql.SQL("SELECT (tags -> 'osm_version')::INTEGER AS od_old_version, CASE WHEN (tags -> 'osm_timestamp') = '' THEN NULL ELSE (tags -> 'osm_timestamp')::TIMESTAMP END AS od_old_timestamp, (tags -> 'osm_changeset')::BIGINT AS od_old_changeset, (tags -> 'osm_uid')::BIGINT AS od_old_ct_id, tags -> 'name' AS od_name, delete(tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) AS od_old_tags, ST_Transform(way,%s) AS od_old_geom FROM {0}.{1} WHERE osm_id = %s UNION SELECT (tags -> 'osm_version')::INTEGER AS od_old_version, CASE WHEN (tags -> 'osm_timestamp') = '' THEN NULL ELSE (tags -> 'osm_timestamp')::TIMESTAMP END AS od_old_timestamp, (tags -> 'osm_changeset')::BIGINT AS od_old_changeset, (tags -> 'osm_uid')::BIGINT AS od_old_ct_id, tags -> 'name' AS od_name, delete(tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) AS od_old_tags, ST_Transform(way,%s) AS od_old_geom FROM {0}.{2} WHERE osm_id = %s;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_line'),sql.Identifier(db_ref_prefix + '_polygon')),(lbtSrid,od_id,lbtSrid,od_id,))
                row = cursor.fetchone()

                # Si l'ancienne version est trouvée, on récupère ses attributs, sinon on considère que la way est nouvelle
                if row is not None:
                    od_old_version = row[0]
                    od_old_timestamp = row[1]
                    od_old_changeset = row[2]
                    od_old_ct_id = row[3]
                    # Garde le nouveau nom, pas l'ancien
                    # od_name = row[4]
                    od_old_tags = row[5]
                    od_old_geom = row[6]

                    #Est-ce que l'ancienne géométrie de la way intersecte l'emprise ?
                    #20210224 : librairie Shapely remplacée par du SQL
                    insideWay = False
                    way_oldgeom_inside = None
                    cursor.execute(sql.SQL("SELECT ST_Intersects(%s,%s);"),(od_old_geom,osm_footprint_geom,))
                    row = cursor.fetchone()
                    if row is not None:
                        way_oldgeom_inside = row[0]
                    if way_oldgeom_inside is True:
                        insideWay = True

                elif row is None:
                    od_action = 'create'
                    od_changed_geom = True
                    insideWay = False

                # Tags et géométrie de la nouvelle version
                is_polygon_tag = 0
                is_polygon_forced = -1
                osm_coord = ''
                # Récupère les nouveaux tags (à faire d'abord, pour savoir si l'objet est un polygone)
                tagsCount = 0
                for tag in object.findall(".//tag"):
                    tagsCount += 1
                    k = tag.get('k')
                    v = tag.get('v')
                    if k in polygon_keys:
                        is_polygon_tag = 1
                    if k == 'area' and v in ['yes','1','true']:
                        is_polygon_forced = 1
                    elif k == 'area' and v in ['no','0','false']:
                        is_polygon_forced = 0
                    kvtag = "=>".join(['"'+k.replace('"','\\"')+'"','"'+v.replace('"','\\"')+'"'])
                    tagsList.append(k)
                    if tagsCount == 1:
                        od_new_tags = "".join([od_new_tags,kvtag])
                    else:
                        od_new_tags = ",".join([od_new_tags,kvtag])
                    if k == 'name':
                        od_name = v
                # Géométrie depuis les nœuds
                ndCount = 0

                for nd in object.findall(".//nd"):
                    osm_lat = nd.get('lat')
                    osm_lon = nd.get('lon')

                    #Si au moins l'un des nœuds est dans l'emprise, il faut garder l'objet (insideWay = True)
                    #20210224 : librairie Shapely remplacée par du SQL
                    if osm_lat is not None and osm_lon is not None:
                        node_newgeom_inside = None
                        cursor.execute(sql.SQL("SELECT ST_Within(ST_SetSRID(ST_MakePoint(%s,%s),%s),%s);"),(osm_lon,osm_lat,lbtSrid,osm_footprint_geom,))
                        row = cursor.fetchone()
                        if row is not None:
                            node_newgeom_inside = row[0]
                        if node_newgeom_inside is True:
                            insideWay = True

                        ndCount += 1
                        lonlat = " ".join([osm_lon,osm_lat])
                        osm_coord = ",".join([osm_coord,lonlat])

                # Polygone ou ligne ?
                # Et forcément > 1 nœud + dans l'emprise
                if ndCount > 1 and insideWay is True:
                    # Si les points de début et de fin ont des coordonnées différentes en X ou en Y, ça ne peut pas être un polygone
                    if object.find(".//nd[1]").get('lat') != object.find(".//nd[last()]").get('lat') or object.find(".//nd[1]").get('lon') != object.find(".//nd[last()]").get('lon'):
                        is_polygon_forced = 0
                    # Polygone
                    if (is_polygon_tag == 1 and is_polygon_forced != 0) or (is_polygon_tag == 0 and is_polygon_forced == 1):
                        polygonWay = True
                    # Ligne
                    elif (is_polygon_tag == 0 and is_polygon_forced != 1) or (is_polygon_tag == 1 and is_polygon_forced == 0):
                        polygonWay = False

                    # Génère le fragment OSC XML pour cet élément + la liste des nodes
                    # + Ajoute tous les nodes en mode modify 2024/01/22                    
                    top = ET.Element('osmChange')
                    top.set('version', '0.6')
                    top.set('generator','LeBonTag')
                    if od_action == 'modify':
                        topChild = ET.SubElement(top, 'modify')
                    elif od_action == 'create':
                        topChild = ET.SubElement(top, 'create')
                    topNodes = ET.SubElement(top, 'modify')
                    topChildElement = ET.SubElement(topChild, 'way', attrib=object.attrib)
                    for nd in object.findall(".//nd"):
                        nodeId = int(nd.get('ref'))
                        if nodeId not in od_id_nodes:
                            od_id_nodes.append(nodeId)
                        topChildElement.append(deepcopy(nd))
                        topNodesElement = ET.SubElement(topNodes, 'node', attrib={"id": nd.get('ref'), "lat": nd.get('lat'), "lon": nd.get('lon')})
                    for tag in object.findall(".//tag"):
                        topChildElement.append(deepcopy(tag))
                    od_osc = ET.tostring(top, encoding='UTF-8', method="xml", pretty_print=False).decode('UTF-8')
                    od_osc = re.sub('\s+',' ',od_osc)
                    od_osc = od_osc.strip()
                    od_id_nodes.sort(key=int)

                    # Ajoute le contributeur au dictionnaire et le changeset à la liste
                    contributorsDict[int(od_new_ct_id)] = od_new_username
                    changesetsList.append(od_new_changeset)

                    # INSERT dans la base
                    cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmdiff (od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc,od_id_nodes) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);").format(sql.Identifier(lbtSchema)),(od_id,od_name,od_action,od_element,od_new_version,od_new_timestamp,od_new_changeset,od_new_ct_id,od_new_tags,od_new_geom,od_old_version,od_old_timestamp,od_old_changeset,od_old_ct_id,od_old_tags,od_old_geom,od_added_tags,od_deleted_tags,od_modified_tags,od_unchanged_tags,od_changed_geom,od_osc,od_id_nodes,))
                    # UPDATE la géométrie (polygone ou ligne selon les cas)
                    if polygonWay is True:
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_new_geom = ST_GeomFromText('POLYGON((%s))',%s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(AsIs(osm_coord[1:]),lbtSrid,od_id,))
                    elif polygonWay is False:
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_new_geom = ST_GeomFromText('LINESTRING(%s)',%s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(AsIs(osm_coord[1:]),lbtSrid,od_id,))
                    # UPDATE "od_mapillary" à partir du tag "mapillary"
                    cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_mapillary = replace(od_new_tags -> 'mapillary','https://www.mapillary.com/map/im/','') WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
                    # Calcule les bounds
                    cursor.execute(sql.SQL("WITH extent_req AS ( SELECT ST_Extent(ST_Collect(od_new_geom,od_old_geom)) AS geom FROM {0}.lbt_osmdiff WHERE od_id = %s) UPDATE {0}.lbt_osmdiff SET od_extent = (SELECT CONCAT_WS(';',ST_XMin(geom), ST_YMin(geom), ST_XMax(geom), ST_YMax(geom)) FROM extent_req) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))

                    if od_action == 'modify':
                        # Compare l'ancienne et la nouvelle géométrie dans la tolérance spécifiée en paramètre. od_changed_geom = true si elles sont différentes.
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_changed_geom = (SELECT CASE WHEN ST_HausdorffDistance(ST_Transform(od_old_geom,%s), ST_Transform(od_new_geom,%s)) < %s THEN false WHEN ST_HausdorffDistance(ST_Transform(od_old_geom,%s), ST_Transform(od_new_geom,%s)) >= %s THEN true END FROM {0}.lbt_osmdiff WHERE od_id = %s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(db_ref_srid,db_ref_srid,geom_tolerance,db_ref_srid,db_ref_srid,geom_tolerance,od_id,od_id,))
                        # 20240625
                        # Si le nombre de nodes est différent entre old et new geom, force od_changed_geom à true (par exemple pour les ways où un nouveau noeud a été rajouté pour créer une intersection)
                        cursor.execute(sql.SQL("WITH nodes_count AS (SELECT od_id, sum(ST_NPoints(od_new_geom)) as new_count, sum(ST_NPoints(od_old_geom)) as old_count FROM {0}.lbt_osmdiff WHERE od_id = %s GROUP BY od_id) UPDATE {0}.lbt_osmdiff odf SET od_changed_geom = true FROM nodes_count WHERE odf.od_id = nodes_count.od_id AND nodes_count.new_count != nodes_count.old_count;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Met à jour l'état de l'objet (pour la fonction d'export)
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_state = (SELECT state_id FROM {0}.lbt_state WHERE state_name = 'modified_nogeom') WHERE od_id = %s AND od_changed_geom IS FALSE;").format(sql.Identifier(lbtSchema)),(od_id,))
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_state = (SELECT state_id FROM {0}.lbt_state WHERE state_name = 'modified_geom') WHERE od_id = %s AND od_changed_geom IS TRUE;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Comparaison :
                        # Les tags ajoutés sont les nouveaux moins les anciens (qu'importe leur valeur)
                        # Les tags supprimés sont les anciens moins les nouveaux (qu'importe leur valeur)
                        # Les tags modifiés sont (les nouveaux moins les anciens mais uniquement de même valeur) moins les tags ajoutés
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_added_tags = (SELECT delete(od_new_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) - akeys(od_old_tags) FROM {0}.lbt_osmdiff WHERE od_id = %s), od_deleted_tags = (SELECT delete(od_old_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) - akeys(od_new_tags) FROM {0}.lbt_osmdiff WHERE od_id = %s), od_modified_tags = (SELECT (delete(od_new_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) - delete(od_old_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area'])) - (delete(od_new_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) - akeys(od_old_tags)) FROM {0}.lbt_osmdiff WHERE od_id = %s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,od_id,od_id,))
                        # Les tags inchangés sont les anciens moins les supprimés moins les modifiés (qu'importe leur valeur)
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_unchanged_tags = (SELECT delete(od_old_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) - akeys(od_deleted_tags) - akeys(od_modified_tags) FROM {0}.lbt_osmdiff WHERE od_id = %s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))
                        # Si aucun tag ajouté/supprimé/modifié et si géométrie changée en deçà de la tolérance, l'objet n'est pas à valider donc il est supprimé
                        cursor.execute(sql.SQL("DELETE FROM {0}.lbt_osmdiff WHERE od_id = %s AND od_added_tags = '' AND od_modified_tags = '' AND od_deleted_tags = '' AND od_changed_geom IS FALSE;").format(sql.Identifier(lbtSchema)),(od_id,))

                    elif od_action == 'create':
                        # Met à jour l'état de l'objet (pour la fonction d'export)
                        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_state = (SELECT state_id FROM {0}.lbt_state WHERE state_name = 'added') WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,))
                        # Comparaison : les tags ajoutés sont forcément tous les tags (il n'y a pas de tags supprimés, modifiés ou conservés)
                        if od_new_tags is not None:
                            cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_added_tags = (SELECT delete(od_new_tags,ARRAY['osm_uid','osm_user','osm_version','osm_changeset','osm_timestamp','way_area']) FROM {0}.lbt_osmdiff WHERE od_id = %s) WHERE od_id = %s;").format(sql.Identifier(lbtSchema)),(od_id,od_id,))

                    # Log des osm id
                    logging.info(od_id)

    except Exception as e:
        logging.critical('Error with modified ways : ' + str(od_id) + ' : ' + str(e))
        sys.exit(1)

    # Met à jour la date de traitement des ways
    try:
        cursor.execute(sql.SQL("UPDATE {0}.lbt_setting SET s_value = %s WHERE s_name = 'osm_diff_ways_date';").format(sql.Identifier(lbtSchema)),(end_date,))
        conn.commit()
    except Exception as e:
        logging.critical('Error when committing ADIFF from ' + str(end_date) + ': ' + str(e))
        sys.exit(1)

except Exception as e:
    logging.critical('ADIFF or OSC error : ' + str(e))
    sys.exit(1)

# =============
# Contributeurs
# =============

logging.info('Contributors...')

try:
    # Convertit le dictionnaire des contributeurs en liste pour l'insérer/mettre à jour en base
    cursor.executemany(sql.SQL("INSERT INTO {0}.lbt_contributor (ct_id, ct_name, ct_vg_id) VALUES (%s,%s,(SELECT vg_id FROM {0}.lbt_vigilance WHERE vg_name = 'yellow')) ON CONFLICT (ct_id) DO UPDATE SET ct_name = EXCLUDED.ct_name;").format(sql.Identifier(lbtSchema)), contributorsDict.items())
except Exception as e:
    logging.critical('Error whith contributors : ' + str(e))
    sys.exit(1)

try:
    conn.commit()
except Exception as e:
    logging.critical('Error when committing contributors : ' + str(e))
    sys.exit(1)

# ====
# Tags
# ====

logging.info('Tags...')

try:
    # Supprime les tags en doublon dans la liste et les insère en base s'ils n'existent pas encore
    tagsList = list(set(tagsList))
    cursor.executemany(sql.SQL("INSERT INTO {0}.lbt_tag (tg_name) VALUES (%(value)s) ON CONFLICT (tg_name) DO NOTHING;").format(sql.Identifier(lbtSchema)), [dict(value=v) for v in tagsList])
except Exception as e:
    logging.critical('Error whith tags : ' + str(e))
    sys.exit(1)

try:
    conn.commit()
except Exception as e:
    logging.critical('Error when committing tags : ' + str(e))
    sys.exit(1)

# ==========
# Changesets
# ==========

logging.info('Changesets...')

try:
    # Supprime les changesets en doublon dans la liste, les interroge, crée les fichiers et les insère en base
    changesetsList = list(set(changesetsList))
    csBaseDirectory = '../osm/changeset/'
    for changeset in changesetsList:
        # Ventile les changesets dans des répertoires (id du changeset moins les 5 derniers caractères)
        csDirectory = csBaseDirectory + changeset.zfill(6)[:-5] + '/'
        if not os.path.exists(csDirectory):
            os.makedirs(csDirectory)
        changesetFile = csDirectory + changeset + '.xml'
        if not os.path.isfile(changesetFile):
            numOfRequests = 6
            for i in range(numOfRequests):
                url = osm_api_url + '/changeset/' + changeset
                csr = requests.get(url,allow_redirects=True,headers=vHeaders,timeout=(60,60))
                if csr.status_code == 200:
                    logging.info('200: changeset ' + changeset + '.xml successfully fetched')
                    csr.encoding = 'UTF-8'
                    break
                logging.warning(str(r.status_code) + ': changeset ' + changeset + '.xml not fetched, retrying in 10 seconds')
                time.sleep(10)
            destFile = open(changesetFile, 'wb')
            destFile.write(csr.content)
            destFile.close()
            root = ET.fromstring(bytes(csr.text, encoding='UTF-8'))
        else:
            tree = ET.parse(changesetFile)
            root = tree.getroot()
        changesetData = root.find("./changeset")
        ocs_id = changesetData.get('id')
        ocs_created_at = changesetData.get('created_at')
        ocs_closed_at = changesetData.get('closed_at')
        ocs_open = changesetData.get('open')
        ocs_uid = changesetData.get('uid')
        ocs_min_lat = changesetData.get('min_lat')
        ocs_min_lon = changesetData.get('min_lon')
        ocs_max_lat = changesetData.get('max_lat')
        ocs_max_lon = changesetData.get('max_lon')
        ocs_comments_count = changesetData.get('comments_count')
        ocs_tags = ''
        ocsTagsCount = 0
        for tag in changesetData.findall(".//tag"):
            ocsTagsCount += 1
            k = tag.get('k')
            v = tag.get('v')
            kvtag = "=>".join(['"'+k.replace('"','\\"')+'"','"'+v.replace('"','\\"')+'"'])
            if ocsTagsCount == 1:
                ocs_tags = "".join([ocs_tags,kvtag])
            else:
                ocs_tags = ",".join([ocs_tags,kvtag])
        cursor.execute(sql.SQL("INSERT INTO {0}.lbt_osmchangeset (ocs_id,ocs_created_at,ocs_closed_at,ocs_open,ocs_ct_id,ocs_min_lat,ocs_min_lon,ocs_max_lat,ocs_max_lon,ocs_comments_count,ocs_tags) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (ocs_id) DO NOTHING;").format(sql.Identifier(lbtSchema)),(ocs_id,ocs_created_at,ocs_closed_at,ocs_open,ocs_uid,ocs_min_lat,ocs_min_lon,ocs_max_lat,ocs_max_lon,ocs_comments_count,ocs_tags))
except Exception as e:
    logging.critical('Error whith changesets : ' + str(e))
    sys.exit(1)

try:
    conn.commit()
except Exception as e:
    logging.critical('Error when committing changesets : ' + str(e))
    sys.exit(1)

# ===
# Fin
# ===

# Ferme la connexion PG
try:
    logging.info('Close database connection.')
    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Error when closing PG : ' + str(e))
    sys.exit(1)

logging.info('Python script ended.')
