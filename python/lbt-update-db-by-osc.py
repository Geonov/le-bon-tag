#!/usr/bin/python3
# -*- encoding: UTF-8 -*-

# Imports
try:
    import yaml, os, shutil, psycopg2, gzip, time, logging, sys, subprocess, platform, itertools
    from pathlib import Path
    from datetime import date, timedelta, datetime
    from psycopg2 import sql
    from psycopg2.extensions import AsIs
    from lxml import etree as ET
    from textwrap import indent, wrap
except Exception as e:
    print(e)
    sys.exit(1)

# Classe de retour pour erreurs subprocess
class CalledProcessError(subprocess.CalledProcessError):
    """A more verbose version of :py:class:`subprocess.CalledProcessError`. Replaces the standard string representation of a :py:class:`subprocess.CalledProcessError` with one that has more output and error information and is formatted to be more readable in a stack trace."""
    def __str__(self):
        errors = self.stderr.decode(sys.stderr.encoding).split("\n")
        outputs = self.stdout.decode(sys.stdout.encoding).split("\n")
        p, q = ("  ", "| ")
        lines = itertools.chain(
            wrap(f"{super().__str__()}"),
            ["Output:"],
            *(
                wrap(output, initial_indent=p, subsequent_indent=p + q)
                for output in outputs
            ),
            ["Errors:"],
            *(
                wrap(error, initial_indent=p, subsequent_indent=p + q)
                for error in errors
            ),
        )
        lines = indent("\n".join(lines), p + q)
        return f"\n{lines}"

# Paramètres config.yaml
try:
    with open("../config.yaml", 'rt', encoding='UTF-8') as yml:
        config = yaml.safe_load(yml)
        lbtHost = config['database']['host']
        lbtPort = config['database']['port']
        lbtDb = config['database']['dbname']
        lbtUser = config['database']['username']
        lbtPassword = config['database']['password']
        lbtSchema = config['database']['schema']
        lbtSrid = int(config['database']['srid'])
except Exception as e:
    print(e)
    sys.exit(1)

# Variable(s) & constante(s)
vToday = date.today()
vYesterday = vToday + timedelta(days=-1)

# Fichier journal (DEBUG/INFO/WARNING/ERROR/CRITICAL)
try:
    logDirectory = '../log/lbt-update-db-by-osc/' + str(vToday.year) + str('%02d' % vToday.month) + '/'
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)
    logFilename = logDirectory + str(vToday) + '.log'
    logging.basicConfig(filename=logFilename,filemode='w',format='%(asctime)s|%(levelname)s|%(message)s',datefmt='%d/%m/%Y %H:%M:%S',level=logging.INFO)
    logging.info('Start Python script.')
except Exception as e:
    print(e)
    sys.exit(1)

# Environnement
platform = platform.system()
logging.info('OS: ' + platform)
current_directory = os.getcwd().replace(os.sep, '/')
logging.info('Current directory: ' + current_directory)
work_directory = str(Path(current_directory).parent.absolute()).replace(os.sep, '/')
logging.info('Work directory: ' + work_directory)

# Ouvre la connexion PG sur LeBonTag
try:
    logging.info('Open database connection.')
    connect_str = "host=%s port=%s dbname=%s user=%s password=%s" % (lbtHost,lbtPort,lbtDb,lbtUser,lbtPassword)
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as e:
    logging.critical('PG connection error : ' + str(e))
    sys.exit(1)

# Récupère les paramètres stockés en base de données
try:
    cursor.execute(sql.SQL("SELECT s_name,s_value FROM {0}.lbt_setting;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()
    drows = dict(rows)
    db_ref_update = drows['db_ref_update']
    db_ref_srid = int(drows['db_ref_srid'])
    db_ref_schema = drows['db_ref_schema']
    db_ref_prefix = drows['db_ref_prefix']
    db_pgr_update = drows['db_pgr_update']
    db_pgr_schema = drows['db_pgr_schema']
    db_pgr_prefix = drows['db_pgr_prefix']
    osm_mapconfigfile = drows['osm_mapconfigfile']
    osm_pbffile = drows['osm_pbffile']
    osm_polyfile = drows['osm_polyfile']
    osm_stylefile = drows['osm_stylefile']
except Exception as e:
    logging.critical('LeBonTag settings fetching error : ' + str(e))
    sys.exit(1)

# Faut-il mettre à jour les bases ?
try:
    if db_ref_update == '0' and db_pgr_update == '0':
        logging.info('No database to update. Exiting.')
        sys.exit(0)
except Exception as e:
    logging.critical('Settings error : ' + str(e))
    sys.exit(1)

# Récupère les OSC de tous les objets validés à intégrer en base et crée l'OSC final de mise à jour via osmium (supprime les doublons et ordonne correctement les objets) pour la base osm2pgsql
try:
    logging.info('Retrieve all OSC fragments from validated objects.')

    cursor.execute(sql.SQL("SELECT od_id,od_osc,od_new_timestamp,od_status,od_id_nodes,od_id_members FROM {0}.lbt_osmdiff WHERE od_osc IS NOT NULL AND od_status = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'valid') ORDER BY od_new_timestamp ASC;").format(sql.Identifier(lbtSchema)))
    rows = cursor.fetchall()

    if cursor.rowcount > 0:

        # Initialisation du fichier final (vide)
        xmlDocumentDirectory = '../osm/osc_update/' + str(vToday.year) + str('%02d' % vToday.month) + '/'
        if not os.path.exists(xmlDocumentDirectory):
            os.makedirs(xmlDocumentDirectory)
        xmlFile = xmlDocumentDirectory + str(vYesterday) + '.osc'
        top = ET.Element('osmChange')
        top.set('version', '0.6')
        top.set('generator','LeBonTag')
        xmlDocument = ET.ElementTree(top)
        xmlDocument.write(xmlFile, encoding='UTF-8', xml_declaration=True, method='xml')

        # Écrit le fragment XML (osc) de l'objet validé dans un fichier temporaire et le tri/fusionne avec le fichier final via osmium
        for row in rows:
            logging.info(str(row[0]) + ': Write OSC fragment')
            od_id = row[0]
            xmlFragment = row[1].encode('UTF-8')
            od_new_timestamp = row[2]
            od_status = row[3]
            od_id_nodes = row[4]
            od_id_members = row[5]
            # Cherche les nodes modifiés présents dans le OSC (pour les ways)
            # Pour chaque node, récupère et ajoute ses tags depuis la base de référence (pour ne pas les perdre lors de la mise à jour)
            # Modifie le fragment XML avec les tags récupérés
            if od_id_nodes is not None and len(od_id_nodes):
                root = ET.fromstring(xmlFragment)
                for node in root.findall(".//modify/node"):
                    nodeId = int(node.get('id'))
                    # Requête le node dans la base de référence (table point)
                    cursor.execute(sql.SQL("SELECT osm_id, hstore_to_json(tags) as tags FROM {0}.{1} WHERE osm_id = %s;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_point')),(nodeId,))
                    row = cursor.fetchone()
                    if row is not None and len(row):
                        # Ajoute chaque tag récupéré depuis la requête dans le OSC
                        for k,v in row[1].items():
                            nodeTag = ET.SubElement(node, 'tag')
                            nodeTag.set('k',str(k))
                            nodeTag.set('v',str(v))
                xmlFragment = ET.tostring(root, encoding='UTF-8', method="xml", pretty_print=False)
            # Fichier temporaire du fragment à traiter
            xmlFragmentFile = open('../tmp/fragment.osc','wb')
            xmlFragmentFile.write(xmlFragment)
            xmlFragmentFile.close()

            # Fusionne et tri le fichier fragment et le fichier final dans un fichier temporaire
            try:
                logging.info(str(od_id) + ': Merge OSC fragment with final OSC')
                if platform == 'Windows':
                    # proc = subprocess.Popen([work_directory + '/tools/osmium/osmium.exe','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile])
                    proc = logging.info(subprocess.run([work_directory + '/tools/osmium/osmium.exe','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile],check=True,capture_output=True))
                else:
                    # proc = subprocess.Popen(['osmium','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile])
                    proc = logging.info(subprocess.run(['osmium','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile],check=True,capture_output=True))                
            except subprocess.CalledProcessError as cpe:
                logging.critical(CalledProcessError(cpe.returncode, cpe.cmd, output=cpe.output, stderr=cpe.stderr))
                sys.exit(1)                

            os.remove('../tmp/fragment.osc')
            os.remove(xmlFile)

            # Le fichier temporaire devient le fichier final
            shutil.move('../tmp/merge.osc',xmlFile)

            # Pour les relations, traite chacun des membres qui peuvent être des nodes ou des ways (on ne gère pas les relations dans les relations)
            if od_id_members is not None and len(od_id_members):
                cursor.execute(sql.SQL("SELECT od_id,od_new_timestamp,od_osc,od_id_nodes FROM {0}.lbt_osmdiff WHERE od_id = ANY (%s) AND od_new_timestamp <= %s AND od_status != (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'integrated') UNION ALL SELECT od_id,od_new_timestamp,od_osc,od_id_nodes FROM {0}.lbt_osmdiff_history WHERE od_id = ANY (%s) AND od_new_timestamp <= %s AND od_status != (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'integrated') ORDER BY od_id,od_new_timestamp;").format(sql.Identifier(lbtSchema)),(od_id_members,od_new_timestamp,od_id_members,od_new_timestamp,))
                membersRows = cursor.fetchall()
                if membersRows is not None:
                    for membersRow in membersRows:
                        od_id = membersRow[0]
                        od_new_timestamp = membersRow[1]
                        xmlFragment = membersRow[2].encode('UTF-8')
                        od_id_nodes = membersRow[3]
                        # Cherche les nodes modifiés présents dans le OSC (pour les ways)
                        # Pour chaque node, récupère et ajoute ses tags depuis la base de référence (pour ne pas les perdre lors de la mise à jour)
                        # Modifie le fragment XML avec les tags récupérés
                        if od_id_nodes is not None and len(od_id_nodes):
                            root = ET.fromstring(xmlFragment)
                            for node in root.findall(".//modify/node"):
                                nodeId = int(node.get('id'))
                                # Requête le node dans la base de référence (table point)
                                cursor.execute(sql.SQL("SELECT osm_id, hstore_to_json(tags) as tags FROM {0}.{1} WHERE osm_id = %s;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_ref_prefix + '_point')),(nodeId,))
                                row = cursor.fetchone()
                                if row is not None and len(row):
                                    # Ajoute chaque tag récupéré depuis la requête dans le OSC
                                    for k,v in row[1].items():
                                        nodeTag = ET.SubElement(node, 'tag')
                                        nodeTag.set('k',str(k))
                                        nodeTag.set('v',str(v))
                            xmlFragment = ET.tostring(root, encoding='UTF-8', method="xml", pretty_print=False)
                        # On écrit le fragment OSC de chaque membre (node ou way)
                        # Fichier temporaire du fragment à traiter
                        xmlFragmentFile = open('../tmp/fragment.osc','wb')
                        xmlFragmentFile.write(xmlFragment)
                        xmlFragmentFile.close()

                        # Fusionne et tri le fichier fragment et le fichier final dans un fichier temporaire
                        try:
                            logging.info(str(od_id) + ': Merge OSC fragment with final OSC [relation]')
                            if platform == 'Windows':
                                #proc = subprocess.Popen([work_directory + '/tools/osmium/osmium.exe','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile])
                                proc = logging.info(subprocess.run([work_directory + '/tools/osmium/osmium.exe','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile],check=True,capture_output=True))
                            else:
                                #proc = subprocess.Popen(['osmium','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile])
                                proc = logging.info(subprocess.run(['osmium','merge-changes','-s','-F','osc','-f','osc','-O','-o','../tmp/merge.osc','../tmp/fragment.osc',xmlFile],check=True,capture_output=True))
                        except subprocess.CalledProcessError as cpe:
                            logging.critical(CalledProcessError(cpe.returncode, cpe.cmd, output=cpe.output, stderr=cpe.stderr))
                            sys.exit(1)                    

                        os.remove('../tmp/fragment.osc')
                        os.remove(xmlFile)

                        # Le fichier temporaire devient le fichier final
                        shutil.move('../tmp/merge.osc',xmlFile)

    else:
        logging.info('No validated objects to update. Exiting.')
        sys.exit(0)

except Exception as e:
    logging.critical('OSC fetching error : ' + str(e))
    sys.exit(1)


# Si la base de référence doit être mise à jour :
if db_ref_update == '1':

    try:
        logging.info('Change search_path for osm2pgsql.')
        cursor.execute(sql.SQL("ALTER ROLE {0} SET search_path TO {1}, public;").format(sql.Identifier(lbtUser),sql.Identifier(db_ref_schema)))
        conn.commit()
    except Exception as e:
        logging.critical('PostgreSQL error : ' + str(e))
        sys.exit(1)

    # Met à jour la BDD cible via osm2pgsql
    # 26/08/2022 Ajout logging.info(xxx,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # 30/08/2022 Ajout classe CalledProcessError
    # '--middle-schema='+str(db_ref_schema)
    # Depuis 1.9.0, existe aussi '--schema='+str(db_ref_schema) ?
    try:
        logging.info('Update target database via osm2pgsql.')
        if osm_stylefile is not None:
            if platform == 'Windows':
                proc = logging.info(subprocess.run([work_directory + '/tools/osm2pgsql/osm2pgsql.exe','--append','--slim','--proj',str(db_ref_srid),'--middle-schema='+str(db_ref_schema),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--style',osm_stylefile,xmlFile],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
            else:
                proc = logging.info(subprocess.run(['osm2pgsql','--append','--slim','--proj',str(db_ref_srid),'--middle-schema='+str(db_ref_schema),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,'--style',osm_stylefile,xmlFile],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
        else:
            if platform == 'Windows':
                proc = logging.info(subprocess.run([work_directory + '/tools/osm2pgsql/osm2pgsql.exe','--append','--slim','--proj',str(db_ref_srid),'--middle-schema='+str(db_ref_schema),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,xmlFile],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
            else:
                proc = logging.info(subprocess.run(['osm2pgsql','--append','--slim','--proj',str(db_ref_srid),'--middle-schema='+str(db_ref_schema),'--hstore-all','--hstore-add-index','--multi-geometry','--keep-coastlines','--extra-attributes','--host',lbtHost,'--port',str(lbtPort),'--database',lbtDb,'--username',lbtUser,'--prefix',db_ref_prefix,xmlFile],env=dict(os.environ,PGPASSWORD=lbtPassword),check=True,capture_output=True))
    except subprocess.CalledProcessError as cpe:
        logging.critical(CalledProcessError(cpe.returncode, cpe.cmd, output=cpe.output, stderr=cpe.stderr))
        sys.exit(1)

    try:
        logging.info('Change search_path for osm2pgsql.')
        cursor.execute(sql.SQL("ALTER ROLE {0} SET search_path TO public, {1};").format(sql.Identifier(lbtUser),sql.Identifier(db_ref_schema)))
        conn.commit()
    except Exception as e:
        logging.critical('PostgreSQL error : ' + str(e))
        sys.exit(1)

    # Met à jour le statut des objets mis à jour dans la base (Validé -> Intégré)
    try:
        logging.info('Update validated objects to integrated objects.')
        cursor.execute(sql.SQL("UPDATE {0}.lbt_osmdiff SET od_status = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'integrated') WHERE od_osc IS NOT NULL AND od_status = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'valid');").format(sql.Identifier(lbtSchema)))
        cursor.execute(sql.SQL("UPDATE {0}.lbt_validation SET val_st_id = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'integrated') WHERE val_st_id = (SELECT st_id FROM {0}.lbt_status WHERE st_name = 'valid');").format(sql.Identifier(lbtSchema)))     
        conn.commit()
    except Exception as e:
        logging.critical('PostgreSQL error : ' + str(e))
        sys.exit(1)

# Si la base pgRouting doit être mise à jour :
if db_pgr_update == '1':

    # 30/08/2022 : osm2pgrouting est remplacé par du SQL direct
    # A FAIRE : compléter les champs de la table osm2pgrouting.ways

    try:
        logging.info('Create pgRouting tables')

        cursor.execute(sql.SQL("DROP TABLE IF EXISTS {0}.%s_ways_vertices CASCADE;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("CREATE TABLE {0}.%s_ways_vertices (gid bigserial NOT NULL,osm_id int8,lon float8,lat float8,junction boolean,the_geom public.geometry(point,4326),CONSTRAINT %s_ways_vertices_pkey PRIMARY KEY (gid)) WITH (autovacuum_enabled=false);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("WITH trueVertices AS ( SELECT (ST_DumpPoints(ST_Transform(way,4326))).geom AS the_geom FROM {0}.%s_line WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed' UNION ALL SELECT (ST_DumpPoints(ST_ExteriorRing((ST_Dump(ST_Transform(way,4326))).geom))).geom AS the_geom FROM {0}.%s_polygon WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed') INSERT INTO {1}.%s_ways_vertices (junction,the_geom) SELECT true, the_geom FROM trueVertices GROUP BY the_geom HAVING count(*) >= 2;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("WITH falseVertices AS ( SELECT (ST_DumpPoints(ST_Transform(way,4326))).geom AS the_geom FROM {0}.%s_line WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed' UNION ALL SELECT (ST_DumpPoints(ST_ExteriorRing((ST_Dump(ST_Transform(way,4326))).geom))).geom AS the_geom FROM {0}.%s_polygon WHERE tags @> 'highway=>road' OR tags @> 'highway=>motorway' OR tags @> 'highway=>motorway_link' OR tags @> 'highway=>motorway_junction' OR tags @> 'highway=>trunk' OR tags @> 'highway=>trunk_link' OR tags @> 'highway=>primary' OR tags @> 'highway=>primary_link' OR tags @> 'highway=>secondary' OR tags @> 'highway=>secondary_link' OR tags @> 'highway=>tertiary' OR tags @> 'highway=>tertiary_link' OR tags @> 'highway=>residential' OR tags @> 'highway=>living_street' OR tags @> 'highway=>service' OR tags @> 'highway=>track' OR tags @> 'highway=>pedestrian' OR tags @> 'highway=>bus_guideway' OR tags @> 'highway=>path' OR tags @> 'highway=>cycleway' OR tags @> 'highway=>footway' OR tags @> 'highway=>bridleway' OR tags @> 'highway=>byway' OR tags @> 'highway=>steps' OR tags @> 'highway=>unclassified' OR tags @> 'junction=>roundabout' OR tags @> 'highway=>construction' OR tags @> 'highway=>proposed') INSERT INTO {1}.%s_ways_vertices (junction,the_geom) SELECT false, the_geom FROM falseVertices GROUP BY the_geom HAVING count(*) < 2;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("CREATE INDEX ways_vertices_the_geom_idx ON {0}.%s_ways_vertices USING gist (the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways_vertices SET lon=ST_X(the_geom),lat=ST_Y(the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("DROP TABLE IF EXISTS {0}.%s_ways CASCADE;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("CREATE TABLE {0}.%s_ways (gid bigserial NOT NULL,osm_id int8,tag_id int4,\"length\" float8,length_m float8,\"name\" text,\"source\" int8,target int8,source_osm int8,target_osm int8,\"cost\" float8,reverse_cost float8,cost_s float8,reverse_cost_s float8,\"rule\" text,one_way int4,oneway text,x1 float8,y1 float8,x2 float8,y2 float8,maxspeed_forward float8,maxspeed_backward float8,priority float8 DEFAULT 1,the_geom public.geometry(linestring, 4326),CONSTRAINT %s_ways_pkey PRIMARY KEY (gid)) WITH (autovacuum_enabled=false);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("INSERT INTO {0}.%s_ways (osm_id, \"name\", the_geom) SELECT ol.osm_id,ol.tags->'name',(ST_Dump(ST_Split(ST_Transform(ol.way,4326),ST_Collect(jt.the_geom)))).geom FROM {1}.%s_line AS ol JOIN {0}.%s_ways_vertices AS jt ON jt.the_geom && ST_Transform(ol.way,4326) WHERE jt.junction IS TRUE AND ( ol.tags @> 'highway=>road' OR ol.tags @> 'highway=>motorway' OR ol.tags @> 'highway=>motorway_link' OR ol.tags @> 'highway=>motorway_junction' OR ol.tags @> 'highway=>trunk' OR ol.tags @> 'highway=>trunk_link' OR ol.tags @> 'highway=>primary' OR ol.tags @> 'highway=>primary_link' OR ol.tags @> 'highway=>secondary' OR ol.tags @> 'highway=>secondary_link' OR ol.tags @> 'highway=>tertiary' OR ol.tags @> 'highway=>tertiary_link' OR ol.tags @> 'highway=>residential' OR ol.tags @> 'highway=>living_street' OR ol.tags @> 'highway=>service' OR ol.tags @> 'highway=>track' OR ol.tags @> 'highway=>pedestrian' OR ol.tags @> 'highway=>bus_guideway' OR ol.tags @> 'highway=>path' OR ol.tags @> 'highway=>cycleway' OR ol.tags @> 'highway=>footway' OR ol.tags @> 'highway=>bridleway' OR ol.tags @> 'highway=>byway' OR ol.tags @> 'highway=>steps' OR ol.tags @> 'highway=>unclassified' OR ol.tags @> 'junction=>roundabout' OR ol.tags @> 'highway=>construction' OR ol.tags @> 'highway=>proposed' ) GROUP BY ol.osm_id,ol.way,ol.tags;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("WITH simplifiedPolygons AS ( SELECT osm_id, tags, ST_ExteriorRing((ST_Dump(way)).geom) AS way FROM {0}.%s_polygon ) INSERT INTO {1}.%s_ways (osm_id, \"name\", the_geom) SELECT ol.osm_id,ol.tags->'name',(ST_Dump(ST_Split(ST_Transform(ol.way,4326),ST_Collect(jt.the_geom)))).geom FROM simplifiedPolygons AS ol JOIN {1}.%s_ways_vertices AS jt ON jt.the_geom && ST_Transform(ol.way,4326) WHERE jt.junction IS TRUE AND ( ol.tags @> 'highway=>road' OR ol.tags @> 'highway=>motorway' OR ol.tags @> 'highway=>motorway_link' OR ol.tags @> 'highway=>motorway_junction' OR ol.tags @> 'highway=>trunk' OR ol.tags @> 'highway=>trunk_link' OR ol.tags @> 'highway=>primary' OR ol.tags @> 'highway=>primary_link' OR ol.tags @> 'highway=>secondary' OR ol.tags @> 'highway=>secondary_link' OR ol.tags @> 'highway=>tertiary' OR ol.tags @> 'highway=>tertiary_link' OR ol.tags @> 'highway=>residential' OR ol.tags @> 'highway=>living_street' OR ol.tags @> 'highway=>service' OR ol.tags @> 'highway=>track' OR ol.tags @> 'highway=>pedestrian' OR ol.tags @> 'highway=>bus_guideway' OR ol.tags @> 'highway=>path' OR ol.tags @> 'highway=>cycleway' OR ol.tags @> 'highway=>footway' OR ol.tags @> 'highway=>bridleway' OR ol.tags @> 'highway=>byway' OR ol.tags @> 'highway=>steps' OR ol.tags @> 'highway=>unclassified' OR ol.tags @> 'junction=>roundabout' OR ol.tags @> 'highway=>construction' OR ol.tags @> 'highway=>proposed' ) GROUP BY ol.osm_id,ol.way,ol.tags;").format(sql.Identifier(db_ref_schema),sql.Identifier(db_pgr_schema)),(AsIs(db_ref_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("CREATE INDEX ways_the_geom_idx ON {0}.%s_ways USING gist (the_geom);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))        
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET \"length\" = ST_Length(the_geom),length_m = ST_length(geography(ST_Transform(the_geom, 4326))),x1 = ST_X(ST_StartPoint(ST_Transform(the_geom, 4326))),y1 = ST_Y(ST_StartPoint(ST_Transform(the_geom, 4326))),x2 = ST_X(ST_EndPoint(ST_Transform(the_geom, 4326))),y2 = ST_Y(ST_EndPoint(ST_Transform(the_geom, 4326))),one_way = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 1 WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 2 WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN -1 WHEN l.oneway is null THEN 0 END, oneway = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 'YES' WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 'NO' WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN 'REVERSED' WHEN l.oneway is null THEN 'UNKNOWN' END, maxspeed_forward = CASE WHEN l.tags -> 'maxspeed:forward' IS NOT NULL AND l.tags -> 'maxspeed:forward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:forward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, maxspeed_backward = CASE WHEN l.tags -> 'maxspeed:backward' IS NOT NULL AND l.tags -> 'maxspeed:backward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:backward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, priority = CASE WHEN l.tags -> 'priority' IS NOT NULL THEN 1 ELSE 0 END FROM {1}.%s_line l WHERE w.osm_id = l.osm_id;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),))
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET \"length\" = ST_Length(the_geom),length_m = ST_length(geography(ST_Transform(the_geom, 4326))),x1 = ST_X(ST_StartPoint(ST_Transform(the_geom, 4326))),y1 = ST_Y(ST_StartPoint(ST_Transform(the_geom, 4326))),x2 = ST_X(ST_EndPoint(ST_Transform(the_geom, 4326))),y2 = ST_Y(ST_EndPoint(ST_Transform(the_geom, 4326))),one_way = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 1 WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 2 WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN -1 WHEN l.oneway is null THEN 0 END, oneway = CASE WHEN l.oneway = 'yes' OR l.oneway = 'true' or l.oneway = '1' THEN 'YES' WHEN l.oneway = 'no' OR l.oneway = 'false' or l.oneway = '0' THEN 'NO' WHEN l.oneway = '-1' OR l.oneway = 'reverse' THEN 'REVERSED' WHEN l.oneway is null THEN 'UNKNOWN' END, maxspeed_forward = CASE WHEN l.tags -> 'maxspeed:forward' IS NOT NULL AND l.tags -> 'maxspeed:forward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:forward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, maxspeed_backward = CASE WHEN l.tags -> 'maxspeed:backward' IS NOT NULL AND l.tags -> 'maxspeed:backward' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed:backward')::DOUBLE PRECISION WHEN l.tags -> 'maxspeed' IS NOT NULL AND l.tags -> 'maxspeed' ~ '^\d+(\.\d+)?$' THEN (l.tags->'maxspeed')::DOUBLE PRECISION ELSE 50 END, priority = CASE WHEN l.tags -> 'priority' IS NOT NULL THEN 1 ELSE 0 END FROM {1}.%s_polygon l WHERE w.osm_id = l.osm_id;").format(sql.Identifier(db_pgr_schema),sql.Identifier(db_ref_schema)),(AsIs(db_pgr_prefix),AsIs(db_ref_prefix),))        
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways SET cost = CASE WHEN oneway = 'REVERSED' THEN -length ELSE length END, reverse_cost = CASE WHEN oneway = 'YES' THEN -length ELSE length END, cost_s = CASE WHEN one_way = -1 THEN -ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_forward::float * 5.0 / 18.0) ELSE ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) END, reverse_cost_s = CASE WHEN one_way = 1 THEN -ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) ELSE ST_length(geography(ST_Transform(the_geom, 4326))) / (maxspeed_backward::float * 5.0 / 18.0) END WHERE maxspeed_backward !=0 AND maxspeed_forward != 0;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET source = v.gid FROM {0}.%s_ways_vertices v WHERE w.x1 = v.lon AND w.y1 = v.lat;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("UPDATE {0}.%s_ways w SET target = v.gid FROM {0}.%s_ways_vertices v WHERE w.x2 = v.lon AND w.y2 = v.lat;").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("ALTER TABLE {0}.%s_ways ADD CONSTRAINT %s_ways_source_fkey FOREIGN KEY (\"source\") REFERENCES {0}.%s_ways_vertices(gid);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))
        cursor.execute(sql.SQL("ALTER TABLE {0}.%s_ways ADD CONSTRAINT %s_ways_target_fkey FOREIGN KEY (target) REFERENCES {0}.%s_ways_vertices(gid);").format(sql.Identifier(db_pgr_schema)),(AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),AsIs(db_pgr_prefix),))

        conn.commit()
    except Exception as e:
        logging.critical('PostgreSQL error : ' + str(e))
        sys.exit(1)

# Ferme la connexion PG
try:
    logging.info('Close database connection.')
    cursor.close()
    conn.close()
except Exception as e:
    logging.critical('Error when closing PG : ' + str(e))
    sys.exit(1)

logging.info('Python script ended.')
