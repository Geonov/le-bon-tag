-- Table lebontag.lbt_userrole

CREATE TABLE lebontag.lbt_userrole (
	ur_id serial PRIMARY KEY,
	ur_name TEXT UNIQUE NOT NULL,
	ur_desc_fr TEXT
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_userrole OWNER TO osm;

CREATE INDEX userrole_id_idx ON lebontag.lbt_userrole USING btree (ur_id);
CREATE INDEX userrole_name_idx ON lebontag.lbt_userrole USING btree (ur_name);

-- Table lebontag.lbt_usertype

CREATE TABLE lebontag.lbt_usertype (
	ut_id serial PRIMARY KEY,
	ut_name TEXT UNIQUE NOT NULL,
	ut_desc_fr TEXT
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_usertype OWNER TO osm;

CREATE INDEX usertype_id_idx ON lebontag.lbt_usertype USING btree (ut_id);
CREATE INDEX usertype_name_idx ON lebontag.lbt_usertype USING btree (ut_name);

-- Table lebontag.lbt_user
CREATE TABLE lebontag.lbt_user (
	u_id serial PRIMARY KEY,
	u_login TEXT UNIQUE NOT NULL,
	u_first_name TEXT,
	u_last_name TEXT,
	u_email TEXT,
	u_ur_id INTEGER NOT NULL references lebontag.lbt_userrole(ur_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	u_ut_id INTEGER NOT NULL references lebontag.lbt_usertype(ut_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	u_password TEXT
	--u_type TEXT CHECK (u_type IN ('internal','ldap')) NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_user OWNER to osm;

CREATE INDEX user_id_idx ON lebontag.lbt_user USING btree (u_id);
CREATE INDEX user_login_idx ON lebontag.lbt_user USING btree (u_login);

-- Table lebontag.lbt_vigilance

CREATE TABLE lebontag.lbt_vigilance
(
	vg_id serial PRIMARY KEY,
	vg_name TEXT UNIQUE NOT NULL,
	vg_desc_fr TEXT,
	vg_range int4range,
	vg_color TEXT,
	EXCLUDE USING GIST (vg_range WITH &&)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_vigilance OWNER TO osm;

CREATE INDEX vigilance_id_idx ON lebontag.lbt_vigilance USING btree (vg_id);
CREATE INDEX vigilance_range_idx ON lebontag.lbt_vigilance USING GIST (vg_range);

-- Table lebontag.lbt_contributor

CREATE TABLE lebontag.lbt_contributor
(
	ct_id INTEGER PRIMARY KEY,
	ct_name TEXT UNIQUE NOT NULL,
	ct_score INTEGER DEFAULT 0,
	ct_internal BOOLEAN DEFAULT false,
	ct_vg_id INTEGER NOT NULL references lebontag.lbt_vigilance(vg_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	ct_auto_validation BOOLEAN DEFAULT FALSE
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_contributor OWNER TO osm;

CREATE INDEX contributor_id_idx ON lebontag.lbt_contributor USING btree (ct_id);
CREATE INDEX contributor_name_idx ON lebontag.lbt_contributor USING btree (ct_name);

-- Mise à jour de la vigilance d'un contributeur (via un trigger) sauf pour les contributeurs internes
-- TEST OK
CREATE OR REPLACE FUNCTION lebontag.update_vigilance() RETURNS trigger AS $$

	BEGIN
	
	UPDATE lebontag.lbt_contributor
	SET ct_vg_id =
	(SELECT vg_id FROM lebontag.lbt_vigilance vg WHERE
	(SELECT ct.ct_score FROM lebontag.lbt_contributor ct WHERE ct.ct_id = NEW.ct_id) <@ vg.vg_range)
	WHERE ct_id = NEW.ct_id AND ct_internal IS FALSE;
	
	RETURN NEW;
	
	END;

$$ LANGUAGE 'plpgsql';

-- TRIGGER
-- TEST OK
CREATE TRIGGER ct_vg_id_update AFTER UPDATE ON lebontag.lbt_contributor FOR EACH ROW
WHEN (OLD.ct_score IS DISTINCT FROM NEW.ct_score)
EXECUTE PROCEDURE lebontag.update_vigilance();

-- Table lebontag.lbt_businessgroup

CREATE TABLE lebontag.lbt_businessgroup
(
	bg_id serial PRIMARY KEY,
	bg_name TEXT UNIQUE NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_businessgroup OWNER TO osm;

CREATE INDEX businessgroup_id_idx ON lebontag.lbt_businessgroup USING btree (bg_id);
CREATE INDEX businessgroup_name_idx ON lebontag.lbt_businessgroup USING btree (bg_name);

-- Table lebontag.lbt_user_businessgroup

CREATE TABLE lebontag.lbt_user_businessgroup
(
	u_id INTEGER references lebontag.lbt_user(u_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	bg_id INTEGER references lebontag.lbt_businessgroup(bg_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	PRIMARY KEY (u_id, bg_id)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_user_businessgroup OWNER TO osm;

CREATE INDEX user_businessgroup_uid_idx ON lebontag.lbt_user_businessgroup USING btree (u_id);
CREATE INDEX user_businessgroup_bgid_idx ON lebontag.lbt_user_businessgroup USING btree (bg_id);

-- Table lebontag.lbt_theme

CREATE TABLE lebontag.lbt_theme
(
	th_id serial PRIMARY KEY,
	th_name TEXT UNIQUE NOT NULL,
	th_icon TEXT NOT NULL,
	th_color TEXT NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_theme OWNER TO osm;

CREATE INDEX theme_id_idx ON lebontag.lbt_theme USING btree (th_id);
CREATE INDEX theme_name_idx ON lebontag.lbt_theme USING btree (th_name);

-- Table lebontag.lbt_objectgroup
-- Le champ og_query n'est plus utilisé mais est conservé pour compatibilité

CREATE TABLE lebontag.lbt_objectgroup
(
	og_id serial PRIMARY KEY,
	og_name TEXT UNIQUE NOT NULL,
	og_query HSTORE,
	og_th_id INTEGER references lebontag.lbt_theme(th_id) ON DELETE SET NULL ON UPDATE RESTRICT,
	og_query_json TEXT,
	og_active BOOLEAN default TRUE,
	og_auto BOOLEAN default FALSE
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_objectgroup OWNER TO osm;

CREATE INDEX objectgroup_id_idx ON lebontag.lbt_objectgroup USING btree (og_id);
CREATE INDEX objectgroup_name_idx ON lebontag.lbt_objectgroup USING btree (og_name);

-- Table lebontag.lbt_tag

CREATE TABLE lebontag.lbt_tag (
	tg_id SERIAL PRIMARY KEY,
	tg_name TEXT UNIQUE NOT NULL,
	tg_name_fr TEXT DEFAULT NULL,
	tg_deleted BOOLEAN DEFAULT FALSE
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_tag OWNER TO osm;

CREATE INDEX tag_id_idx ON lebontag.lbt_tag USING btree (tg_id);
CREATE INDEX tag_name_idx ON lebontag.lbt_tag USING btree (tg_name);
CREATE INDEX tag_namefr_idx ON lebontag.lbt_tag USING btree (tg_name_fr);

-- Table lbt_objectgroup_tag
-- La table lbt_objectgroup_tag n'est plus utilisée (désactivée dans l'interface mais de nombreuses traces dans le code) donc conservée pour compatibilité

CREATE TABLE lebontag.lbt_objectgroup_tag
(
	og_id INTEGER REFERENCES lebontag.lbt_objectgroup(og_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	tg_id INTEGER REFERENCES lebontag.lbt_tag(tg_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	PRIMARY KEY (og_id,tg_id)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_objectgroup_tag OWNER to osm;

CREATE INDEX objectgroup_tag_ogid_idx ON lebontag.lbt_objectgroup_tag USING btree (og_id);
CREATE INDEX objectgroup_tag_tgid_idx ON lebontag.lbt_objectgroup_tag USING btree (tg_id);

-- Table lbt_right

CREATE TABLE lebontag.lbt_right
(
	r_id SERIAL PRIMARY KEY,
	bg_id INTEGER references lebontag.lbt_businessgroup(bg_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	og_id INTEGER references lebontag.lbt_objectgroup(og_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	r_verif_geom BOOLEAN DEFAULT TRUE,
	r_verif_all_tags BOOLEAN DEFAULT FALSE,
	UNIQUE (bg_id,og_id)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_right OWNER TO osm;

CREATE INDEX right_bgid_idx ON lebontag.lbt_right USING btree (bg_id);
CREATE INDEX right_ogid_idx ON lebontag.lbt_right USING btree (og_id);

-- Table lbt_right_tag

CREATE TABLE lebontag.lbt_right_tag
(
	r_id INTEGER references lebontag.lbt_right(r_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	tg_id INTEGER references lebontag.lbt_tag(tg_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	UNIQUE (r_id,tg_id)
);

ALTER TABLE lebontag.lbt_right_tag OWNER TO osm;

CREATE INDEX right_tag_rid_idx ON lebontag.lbt_right_tag USING btree (r_id);
CREATE INDEX right_tag_tgid_idx ON lebontag.lbt_right_tag USING btree (tg_id);

-- Table lebontag.lbt_status

CREATE TABLE lebontag.lbt_status
(
	st_id serial PRIMARY KEY,
	st_name TEXT UNIQUE,
	st_desc_fr TEXT
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_status OWNER TO osm;

CREATE INDEX status_id_idx ON lebontag.lbt_status USING btree (st_id);
CREATE INDEX status_name_idx ON lebontag.lbt_status USING btree (st_name);

-- Table lebontag.lbt_state

CREATE TABLE lebontag.lbt_state
(
	state_id serial PRIMARY KEY,
	state_name TEXT UNIQUE,
	state_desc_fr TEXT
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_state OWNER TO osm;

CREATE INDEX state_id_idx ON lebontag.lbt_state USING btree (state_id);
CREATE INDEX state_name_idx ON lebontag.lbt_state USING btree (state_name);

-- Table lebontag.lbt_osmdiff

CREATE TABLE lebontag.lbt_osmdiff
(
	od_id BIGINT,
	od_name TEXT,
	od_action CHARACTER VARYING(6) NOT NULL,
	od_element CHARACTER VARYING(8) NOT NULL,
	od_new_version INTEGER,
	od_new_timestamp TIMESTAMP WITH TIME ZONE,
	od_new_changeset BIGINT,
	od_new_ct_id BIGINT,
	od_new_tags HSTORE,
	od_new_geom GEOMETRY,
	od_old_version INTEGER,
	od_old_timestamp TIMESTAMP WITH TIME ZONE,
	od_old_changeset BIGINT,
	od_old_ct_id BIGINT,
	od_old_tags HSTORE,
	od_old_geom GEOMETRY,
	od_added_tags HSTORE NOT NULL DEFAULT '',
	od_deleted_tags HSTORE NOT NULL DEFAULT '',
	od_modified_tags HSTORE NOT NULL DEFAULT '',
	od_unchanged_tags HSTORE NOT NULL DEFAULT '',
	od_changed_geom BOOLEAN,
	od_status INTEGER NOT NULL DEFAULT 1 REFERENCES lebontag.lbt_status(st_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	od_state INTEGER REFERENCES lebontag.lbt_state(state_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	od_comment TEXT,
	od_osc XML,
	od_id_nodes BIGINT[],
	od_id_members BIGINT[],
	od_extent TEXT,
	od_mapillary TEXT,
	CONSTRAINT enforce_dims_od_new_geom CHECK (st_ndims(od_new_geom) = 2),
	CONSTRAINT enforce_srid_od_new_geom CHECK (st_srid(od_new_geom) = 4326),
	CONSTRAINT enforce_dims_od_old_geom CHECK (st_ndims(od_old_geom) = 2),
	CONSTRAINT enforce_srid_od_old_geom CHECK (st_srid(od_old_geom) = 4326),
	PRIMARY KEY (od_id,od_new_version)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_osmdiff OWNER TO osm;

CREATE INDEX osmdiff_id_idx ON lebontag.lbt_osmdiff USING btree (od_id);
CREATE INDEX osmdiff_id_version_idx ON lebontag.lbt_osmdiff USING btree (od_id,od_new_version);
CREATE INDEX osmdiff_new_geom_idx ON lebontag.lbt_osmdiff USING gist (od_new_geom);
CREATE INDEX osmdiff_old_geom_idx ON lebontag.lbt_osmdiff USING gist (od_old_geom);
CREATE INDEX osmdiff_od_new_tags_idx ON lebontag.lbt_osmdiff USING gin (od_new_tags);
CREATE INDEX osmdiff_od_old_tags_idx ON lebontag.lbt_osmdiff USING gin (od_old_tags);
CREATE INDEX osmdiff_od_added_tags_idx ON lebontag.lbt_osmdiff USING gin (od_added_tags);
CREATE INDEX osmdiff_od_deleted_tags_idx ON lebontag.lbt_osmdiff USING gin (od_deleted_tags);
CREATE INDEX osmdiff_od_modified_tags_idx ON lebontag.lbt_osmdiff USING gin (od_modified_tags);
CREATE INDEX osmdiff_od_unchanged_tags_idx ON lebontag.lbt_osmdiff USING gin (od_unchanged_tags);

ALTER TABLE lebontag.lbt_osmdiff ALTER COLUMN od_new_geom SET STORAGE EXTERNAL;

-- Table lebontag.lbt_osmdiff_history

CREATE TABLE lebontag.lbt_osmdiff_history
(
	od_id BIGINT,
	od_name TEXT,
	od_action CHARACTER VARYING(6) NOT NULL,
	od_element CHARACTER VARYING(8) NOT NULL,
	od_new_version INTEGER,
	od_new_timestamp TIMESTAMP WITH TIME ZONE,
	od_new_changeset BIGINT,
	od_new_ct_id BIGINT,
	od_new_tags HSTORE,
	od_new_geom GEOMETRY,
	od_old_version INTEGER,
	od_old_timestamp TIMESTAMP WITH TIME ZONE,
	od_old_changeset BIGINT,
	od_old_ct_id BIGINT,
	od_old_tags HSTORE,
	od_old_geom GEOMETRY,
	od_added_tags HSTORE NOT NULL DEFAULT '',
	od_deleted_tags HSTORE NOT NULL DEFAULT '',
	od_modified_tags HSTORE NOT NULL DEFAULT '',
	od_unchanged_tags HSTORE NOT NULL DEFAULT '',
	od_changed_geom BOOLEAN,
	od_status INTEGER NOT NULL DEFAULT 1 REFERENCES lebontag.lbt_status(st_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	od_state INTEGER REFERENCES lebontag.lbt_state(state_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	od_comment TEXT,
	od_osc XML,
	od_id_nodes BIGINT[],
	od_id_members BIGINT[],
	od_extent TEXT,
	od_mapillary TEXT,
	CONSTRAINT enforce_dims_od_new_geom CHECK (st_ndims(od_new_geom) = 2),
	CONSTRAINT enforce_srid_od_new_geom CHECK (st_srid(od_new_geom) = 4326),
	CONSTRAINT enforce_dims_od_old_geom CHECK (st_ndims(od_old_geom) = 2),
	CONSTRAINT enforce_srid_od_old_geom CHECK (st_srid(od_old_geom) = 4326),
	PRIMARY KEY (od_id,od_new_version)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_osmdiff_history OWNER TO osm;

CREATE INDEX osmdiffhist_id_idx ON lebontag.lbt_osmdiff_history USING btree (od_id);
CREATE INDEX osmdiffhist_id_version_idx ON lebontag.lbt_osmdiff USING btree (od_id,od_new_version);
CREATE INDEX osmdiffhist_new_geom_idx ON lebontag.lbt_osmdiff_history USING gist (od_new_geom);
CREATE INDEX osmdiffhist_old_geom_idx ON lebontag.lbt_osmdiff_history USING gist (od_old_geom);

-- Table lbt_validation

CREATE TABLE lebontag.lbt_validation
(
	val_id SERIAL PRIMARY KEY,
	val_od_id BIGINT,
	val_od_version INTEGER NOT NULL,
	val_u_id INTEGER NOT NULL REFERENCES lebontag.lbt_user(u_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	val_st_id INTEGER NOT NULL REFERENCES lebontag.lbt_status(st_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	val_created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	val_comment text,
	val_tags text[],
	FOREIGN KEY (val_od_id,val_od_version) REFERENCES lebontag.lbt_osmdiff(od_id,od_new_version) ON DELETE CASCADE ON UPDATE CASCADE
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_validation OWNER TO osm;

CREATE INDEX val_id_idx ON lebontag.lbt_validation USING btree (val_id);
CREATE INDEX val_od_id_idx ON lebontag.lbt_validation USING btree (val_od_id);

-- Table lebontag.lbt_wms

CREATE TABLE lebontag.lbt_wms
(
	wms_id serial PRIMARY KEY,
	wms_name TEXT UNIQUE NOT NULL,
	wms_url TEXT NOT NULL,
	wms_layer TEXT NOT NULL,
	wms_format TEXT NOT NULL,
	--wms_srs TEXT, 'EPSG:3857'
	wms_attribution TEXT,
	wms_maxzoom INTEGER,
	wms_alpha BOOLEAN,
	wms_type TEXT NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_wms OWNER TO osm;

CREATE INDEX wms_id_idx ON lebontag.lbt_wms USING btree (wms_id);
CREATE INDEX wms_name_idx ON lebontag.lbt_wms USING btree (wms_name);

-- Table lebontag.lbt_wmts

CREATE TABLE lebontag.lbt_wmts
(
	wmts_id serial PRIMARY KEY,
	wmts_name TEXT UNIQUE NOT NULL,
	wmts_url TEXT NOT NULL,
	wmts_layer TEXT NOT NULL,
	wmts_format TEXT NOT NULL,
	wmts_matrix_set TEXT NOT NULL,
	wmts_style TEXT NOT NULL,
	wmts_attribution TEXT,
	wmts_maxzoom INTEGER,
	wmts_type TEXT NOT NULL,
	wmts_protocol TEXT NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_wmts OWNER TO osm;

CREATE INDEX wmts_id_idx ON lebontag.lbt_wmts USING btree (wmts_id);
CREATE INDEX wmts_name_idx ON lebontag.lbt_wmts USING btree (wmts_name);

-- Table lebontag.lbt_tile

CREATE TABLE lebontag.lbt_tile
(
	tile_id serial PRIMARY KEY,
	tile_name TEXT UNIQUE NOT NULL,
	tile_url TEXT NOT NULL,
	tile_layer TEXT NOT NULL,
	tile_format TEXT NOT NULL,
	tile_attribution TEXT,
	tile_maxzoom INTEGER,
	tile_alpha BOOLEAN,
	tile_visible BOOLEAN
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_tile OWNER TO osm;

CREATE INDEX tile_id_idx ON lebontag.lbt_tile USING btree (tile_id);
CREATE INDEX tile_name_idx ON lebontag.lbt_tile USING btree (tile_name);

-- Table lebontag.lbt_setting

CREATE TABLE lebontag.lbt_setting
(
	s_id serial PRIMARY KEY,
	s_name TEXT UNIQUE NOT NULL,
	s_value TEXT
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_setting OWNER TO osm;

CREATE INDEX s_id_idx ON lebontag.lbt_setting USING btree (s_id);
CREATE INDEX s_name_idx ON lebontag.lbt_setting USING btree (s_name);

-- Table lebontag.lbt_osmchangeset

CREATE TABLE lebontag.lbt_osmchangeset
(
	ocs_id BIGINT PRIMARY KEY,
	ocs_created_at TIMESTAMP WITH TIME ZONE,
	ocs_closed_at TIMESTAMP WITH TIME ZONE,
	ocs_open BOOLEAN,
	ocs_ct_id BIGINT,
	ocs_min_lat DECIMAL,
	ocs_min_lon DECIMAL,
	ocs_max_lat DECIMAL,
	ocs_max_lon DECIMAL,
	ocs_comments_count INTEGER,
	ocs_tags HSTORE
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_osmchangeset OWNER TO osm;

CREATE INDEX osmchangeset_id_idx ON lebontag.lbt_osmchangeset USING btree (ocs_id);
CREATE INDEX osmchangeset_tags_idx ON lebontag.lbt_osmchangeset USING gin (ocs_tags);

-- Table lebontag.lbt_export

CREATE TABLE lebontag.lbt_export
(
	exp_id SERIAL PRIMARY KEY,
	exp_name TEXT NOT NULL,
	exp_query JSON,
	exp_shared_query BOOLEAN DEFAULT 'FALSE',
	exp_u_id INTEGER NOT NULL REFERENCES lebontag.lbt_user(u_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	UNIQUE (exp_name,exp_u_id)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_export OWNER TO osm;

CREATE INDEX export_id_idx ON lebontag.lbt_export USING btree (exp_id);

-- Table lebontag.lbt_rattachment_session

CREATE TABLE lebontag.lbt_rattachment_session
(
	rtsession_id SERIAL PRIMARY KEY,
	rtsession_name TEXT UNIQUE NOT NULL,
	rtsession_host TEXT NOT NULL,
	rtsession_port INTEGER NOT NULL,
	rtsession_db TEXT NOT NULL,
	rtsession_user TEXT,
	rtsession_password TEXT,
	rtsession_tablefil TEXT NOT NULL,
	rtsession_tablefil_attr TEXT NOT NULL,
	rtsession_tableattr TEXT NOT NULL,
	rtsession_tablegeom TEXT NOT NULL,
	rtsession_schema_temp TEXT NOT NULL,
	rtsession_fid TEXT NOT NULL,
	rtsession_fgeom TEXT NOT NULL,
	rtsession_flist TEXT NOT NULL,
	rtsession_tolerance DECIMAL NOT NULL,
	rtsession_epsg INTEGER NOT NULL,
	rtsession_tableattr_temp TEXT NOT NULL,
	rtsession_tablegeom_temp TEXT NOT NULL,
	rtsession_startdate TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	rtsession_enddate TIMESTAMP WITH TIME ZONE,
	rtsession_close BOOLEAN DEFAULT 'FALSE',
	rtsession_isreverse BOOLEAN DEFAULT 'FALSE',
	rtsession_tablefil_where TEXT,
	rtsession_isfollowedbyreverse BOOLEAN DEFAULT 'TRUE'
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_rattachment_session OWNER TO osm;

CREATE INDEX rattachment_id_idx ON lebontag.lbt_rattachment_session USING btree (rtsession_id);

-- Table lbt_footprint

CREATE TABLE lebontag.lbt_footprint
(
	fp_id SERIAL PRIMARY KEY,
	fp_name TEXT UNIQUE NOT NULL,
	fp_geom GEOMETRY NOT NULL,
	CONSTRAINT enforce_dims_fp_geom CHECK (st_ndims(fp_geom) = 2),
	CONSTRAINT enforce_srid_fp_geom CHECK (st_srid(fp_geom) = 4326)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_footprint OWNER TO osm;

CREATE INDEX footprint_id_idx ON lebontag.lbt_footprint USING btree (fp_id);
CREATE INDEX footprint_geom_idx ON lebontag.lbt_footprint USING gist (fp_geom);

ALTER TABLE lebontag.lbt_footprint ALTER COLUMN fp_geom SET STORAGE EXTERNAL;

-- Table lbt_user_footprint

CREATE TABLE lebontag.lbt_user_footprint
(
	u_id INTEGER references lebontag.lbt_user(u_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	fp_id INTEGER references lebontag.lbt_footprint(fp_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	UNIQUE (u_id,fp_id)
);

ALTER TABLE lebontag.lbt_user_footprint OWNER TO osm;

CREATE INDEX user_footprint_uid_idx ON lebontag.lbt_user_footprint USING btree (u_id);
CREATE INDEX user_footprint_fpid_idx ON lebontag.lbt_user_footprint USING btree (fp_id);

-- Table lbt_osmdiff_footprint

CREATE TABLE lebontag.lbt_osmdiff_footprint
(
	od_id BIGINT,
	od_new_version INTEGER,
	fp_id INTEGER,
	UNIQUE (od_id,od_new_version,fp_id)
);

ALTER TABLE lebontag.lbt_osmdiff_footprint ADD CONSTRAINT lbt_osmdiff_footprint_odidodnew_fkey FOREIGN KEY (od_id,od_new_version) REFERENCES lebontag.lbt_osmdiff(od_id,od_new_version) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE lebontag.lbt_osmdiff_footprint ADD CONSTRAINT lbt_osmdiff_footprint_fpid_fkey FOREIGN KEY (fp_id) REFERENCES lebontag.lbt_footprint(fp_id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE lebontag.lbt_osmdiff_footprint OWNER TO osm;

CREATE INDEX osmdiff_footprint_odidv_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id,od_new_version);
CREATE INDEX osmdiff_footprint_odid_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id);
CREATE INDEX osmdiff_footprint_fpid_idx ON lebontag.lbt_osmdiff_footprint USING btree (fp_id);

-- Fonction qui échappe tous les caractères spéciaux des regex
-- Exemple : select escape_regex('+33698563257');
DROP FUNCTION IF EXISTS lebontag.escape_regex(text);
CREATE FUNCTION lebontag.escape_regex(text)
RETURNS TEXT AS $$
DECLARE 
	special_chars varchar[] := array['.', '*', '+', '?', '^', '$', '{', '}', '(', ')', '|', '[', ']'];
	x text;
BEGIN
  FOREACH x IN ARRAY special_chars
  LOOP
    $1 = replace($1, x, '\' || x);
  END LOOP;
  RETURN $1;
END;
$$ LANGUAGE plpgsql;

