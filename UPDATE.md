# Mise à jour de l'application LeBonTag

## Mise à jour des fichiers via le dépôt git

Choisir "latest" pour la dernière "release" (stable) ou "master" pour la dernière version de développement (bêta).

### Latest (release)

```
cd /opt/le-bon-tag && sudo git checkout -- . && sudo git checkout latest && sudo git pull https://gitlab.com/Geonov/le-bon-tag.git
sudo dos2unix /opt/le-bon-tag/cron/*.sh && sudo chmod +x /opt/le-bon-tag/cron/*.sh
```

### Master (dev)

```
cd /opt/le-bon-tag && sudo git checkout -- . && sudo git checkout master && sudo git pull https://gitlab.com/Geonov/le-bon-tag.git
sudo dos2unix /opt/le-bon-tag/cron/*.sh && sudo chmod +x /opt/le-bon-tag/cron/*.sh
```

## Mise à jour SQL

Charger le(s) fichier(s) SQL présents dans "/opt/le-bon-tag/update/" dans l'ordre des versions pour mettre à jour la base de données PostgreSQL de l'application.

Par exemple, si la version précédemment installée était la 13, charger les fichiers dans l'ordre à partir de "11\_update\_0.13.0\_to\_0.14.0.sql".

## Port d'écoute

Modifier le port d'écoute (si différent de 80) dans le fichier :
```
/opt/le-bon-tag/server/config.json
```

## Mise à jour des modules et recompilation de l'application

```
cd /opt/le-bon-tag
sudo rm -f package-lock.json
sudo npm install -g npm
sudo npm install --unsafe-perm 
sudo npm run lint && sudo npm run build
```

> En cas d'erreur lors de l'installation des modules node.js, supprimer d'abord le répertoire "node_modules" et ré-exécuter les commandes précédentes :

>```
>sudo rm -R /opt/le-bon-tag/node_modules
>```

## Mise à jour et redémarrage du service

```
cd /opt/le-bon-tag && sudo pm2 update && sudo pm2 restart api/index.js
```
