#! /bin/bash
cd "$(dirname "$0")"/../python
python3 lbt-get-adiff-nodes.py
python3 lbt-get-adiff-ways.py
python3 lbt-get-adiff-relations.py
python3 lbt-autovalidation.py
python3 lbt-footprints-intersection.py
