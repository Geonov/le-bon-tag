set dir=%cd%
python %dir%/../python/lbt-get-adiff-nodes.py
python %dir%/../python/lbt-get-adiff-ways.py
python %dir%/../python/lbt-get-adiff-relations.py
python %dir%/../python/lbt-autovalidation.py
python %dir%/../python/lbt-footprints-intersection.py
