# Reboot de l'application LeBonTag

Le "reboot" consiste à ré-initialiser les données de référence à partir d'un nouvel export Geofabrik et à redémarrer la validation à partir de cette date.

C'est utile après une mise à jour de l'application, en particulier si la mise à jour corrige des bugs dans les scripts Python de chargement des différences OSM.

## Nettoyage des données

Nous vidons les tables osm2pgrouting et osm2pgsql qui seront rechargées à partir du nouveau fichier PBF.

Se connecter à la base "osm" de l'application puis :

```
TRUNCATE TABLE osm2pgsql.osm_line CASCADE;
TRUNCATE TABLE osm2pgsql.osm_nodes CASCADE;
TRUNCATE TABLE osm2pgsql.osm_point CASCADE;
TRUNCATE TABLE osm2pgsql.osm_polygon CASCADE;
TRUNCATE TABLE osm2pgsql.osm_rels CASCADE;
TRUNCATE TABLE osm2pgsql.osm_roads CASCADE;
TRUNCATE TABLE osm2pgsql.osm_ways CASCADE;
TRUNCATE TABLE osm2pgrouting.pgr_ways_vertices CASCADE;
TRUNCATE TABLE osm2pgrouting.pgr_ways CASCADE;
```

Nous déplaçons ensuite dans l'historique les objets encore à valider puis vidons les tables de validation :

```
INSERT INTO lebontag.lbt_osmdiff_history (SELECT * FROM lebontag.lbt_osmdiff) ON CONFLICT (od_id,od_new_version) DO NOTHING;
TRUNCATE TABLE lebontag.lbt_validation CASCADE;
TRUNCATE TABLE lebontag.lbt_osmdiff CASCADE;
```

## Réinitialisation des paramètres de chargement

La validation doit reprendre à partir de la date du nouveau fichier PBF, les dates actuelles doivent donc être supprimées :

```
UPDATE lebontag.lbt_setting SET s_value = '' WHERE s_name = 'osm_diff_nodes_date';
UPDATE lebontag.lbt_setting SET s_value = '' WHERE s_name = 'osm_diff_ways_date';
UPDATE lebontag.lbt_setting SET s_value = '' WHERE s_name = 'osm_diff_relations_date';
UPDATE lebontag.lbt_setting SET s_value = '0' WHERE s_name = 'osm_diff_nodes_attempts';
UPDATE lebontag.lbt_setting SET s_value = '0' WHERE s_name = 'osm_diff_ways_attempts';
UPDATE lebontag.lbt_setting SET s_value = '0' WHERE s_name = 'osm_diff_relations_attempts';
```

## Nettoyage des fichiers

```
sudo rm -R /opt/le-bon-tag/log/* && \
sudo rm -R /opt/le-bon-tag/osm/adiff/* && \
sudo rm -R /opt/le-bon-tag/osm/osc_update/* && \
sudo rm /opt/le-bon-tag/osm/source/* && \
sudo rm -R /opt/le-bon-tag/osm/changeset/*
```

## Chargement du nouveau fichier PBF

Il faut reprendre les procédures "Données OSM de référence" et "Chargement des données OSM de référence et d'itinéraire" du document d'installation, à savoir :

- Se connecter avec un compte OSM sur https://osm-internal.download.geofabrik.de/index.html (“OpenStreetMap internal server”).
- Une fois le compte OSM lié au site “geofabrik.de”, télécharger les données de la région de son choix, par exemple : https://osm-internal.download.geofabrik.de/europe/france/languedoc-roussillon-internal.osm.pbf
- Placer le fichier téléchargé sur le serveur, dans “/opt/le-bon-tag/osm/source/”.
- Vérifier que ce fichier est bien déclaré dans les réglages de l'application, dans "Données OSM de référence / Chemin et nom du fichier PBF des données OSM de référence à charger".
- Exécuter :
```
sudo dos2unix /opt/le-bon-tag/scripts/*.sh && sudo chmod +x /opt/le-bon-tag/scripts/*.sh
sudo -E /opt/le-bon-tag/scripts/lbt-load-data.sh
```

## Appel Overpass

Pour ne pas attendre le CRON et tester le chargement des différences OSM immédiatement, exécuter :

```
sudo dos2unix /opt/le-bon-tag/cron/*.sh && sudo chmod +x /opt/le-bon-tag/cron/*.sh
sudo -E /opt/le-bon-tag/cron/lbt-get-adiff.sh
```
