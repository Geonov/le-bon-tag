/**
 * Classe ThemeObjectService
 * 
 * Permet d'effectuer des opérations sur les objets des thèmes
 */
export class ThemeObjectService {
    constructor(db, obj, theme, settings) {
        this.db = db;
        this.obj = obj;
        this.theme = theme;
        this.settings = settings;
        this.sequelize = db.sequelize;
        this.Validation = db.validation;
        this.Contributor = db.contributor;
        this.OsmDiff = db.osmdiff;
		this.Footprint = db.footprint;
        this.Op = db.Sequelize.Op;
    }

    /**
     * Retourne l'arbre de diffs
     * @param {*} footprintId 
     * @param {*} filter 
     * @returns 
     */
    findOsmDiffs(footprintId, filter) {
        return new Promise((resolve, reject) => {

            const objectsLimit = this.settings.objects_limit ? this.settings.objects_limit : 15;

            // Trouve les identifiants (sert à compter)
            this.findOsmDiffIds(footprintId, filter).then(aIds => {

                // Trouve les infos en détail pour les x premiers enregistrements
                const detailIds = aIds.slice(0, objectsLimit);
                this.findOsmDiffsDetail(detailIds, filter).then(osmdiff => {
                    resolve({
                        osmdiff: osmdiff,
                        nbOsmDiff: aIds.length,
                    });
                });
            });
        });
    }

    /**
     * Retourne la liste des identifiants des osmDiff
     * @param {*} footprintId 
     * @param {*} filter 
     * @returns 
     */
    findOsmDiffIds(footprintId, filter) {
        return new Promise((resolve, reject) => {

            // requête définissant le groupe d'objets
            const newCompareQuery = this.compareQuery('od_new_tags');
            const oldCompareQuery = this.compareQuery('od_old_tags');

            // tags définissant le groupe d'objets
            const newCompareTags = this.compareTags('od_new_tags');
            const oldCompareTags = this.compareTags('od_old_tags');

            let addedRights = [];
            let deletedRights = [];
            let modifiedRights = [];
            let modifiedGeom = [];

            // Prépare la requête WHERE selon les différents cas (vérifier tous les tags ou vérifier certains tags et/ou vérifier la geom)
            if (this.VerifAllTagsCase() && this.VerifGeomCase()) {
                console.log('VerifAllTags + VerifGeom');
                addedRights.push = (this.sequelize.where(this.sequelize.col("od_added_tags"), '!=', '',));
                deletedRights.push = (this.sequelize.where(this.sequelize.col("od_deleted_tags"), '!=', '',));
                modifiedRights.push = (this.sequelize.where(this.sequelize.col("od_modified_tags"), '!=', '',));
                modifiedGeom.push = (this.sequelize.where(this.sequelize.col("od_changed_geom"), '=', 'true',));
            } else if (this.VerifAllTagsCase() && !this.VerifGeomCase()) {
                console.log('VerifAllTags');
                addedRights.push = (this.sequelize.where(this.sequelize.col("od_added_tags"), '!=', '',));
                deletedRights.push = (this.sequelize.where(this.sequelize.col("od_deleted_tags"), '!=', '',));
                modifiedRights.push = (this.sequelize.where(this.sequelize.col("od_modified_tags"), '!=', '',));
            } else if (!this.VerifAllTagsCase() && this.VerifTagsToValidate().length && this.VerifGeomCase()) {
                console.log('VerifTags + VerifGeom');
                addedRights = this.compareRights('od_added_tags');
                deletedRights = this.compareRights('od_deleted_tags');
                modifiedRights = this.compareRights('od_modified_tags');
                modifiedGeom.push = (this.sequelize.where(this.sequelize.col("od_changed_geom"), '=', 'true',));
            } else if (!this.VerifAllTagsCase() && this.VerifTagsToValidate().length && !this.VerifGeomCase()) {
                console.log('VerifTags');
                addedRights = this.compareRights('od_added_tags');
                deletedRights = this.compareRights('od_deleted_tags');
                modifiedRights = this.compareRights('od_modified_tags');
            } else {
                console.log('Cas autre - id groupe :', this.obj.id);
            }

            // N'est plus utilisée car footprintIntersect en direct est trop lent
            //let intersectQuery = [];
            //if (footprintId) {
            //    intersectQuery.push(this.footprintIntersect(footprintId));
            //}

			// Partie WHERE pour l'emprise
			let intersectQuery = [];
			if (footprintId !== 'none') {
				intersectQuery.push(this.sequelize.where(this.sequelize.col("footprints.fp_id"), '=', footprintId,));
			}

            // Clauses WHERE de la requête
            const whereClause = {
                [this.Op.and]: [
						//...(footprintId !== 'none' ? {
                        //...intersectQuery
                        // Clause WHERE sur l'emprise contenant l'osmdiff
                        //'$footprints.fp_id$': footprintId,
						//} : {}),
                    {
                        [this.Op.or]: [{
                            ...newCompareQuery,
                        },
                        {
                            ...oldCompareQuery,
                        },
                        ],
                    },
                    {
                        [this.Op.or]: [{
                            ...newCompareTags,
                        },
                        {
                            ...oldCompareTags,
                        },
                        ],
                    },
                    {
                        [this.Op.or]: [{
                            ...addedRights,
                        },
                        {
                            ...deletedRights,
                        },
                        {
                            ...modifiedRights,
                        },
                        {
                            ...modifiedGeom,
                        },
                        ],
                    }
                ],
                // AND status.st_name
                '$status.st_name$': {
                    [this.Op.or]: [{
                        [this.Op.in]: ['tovalidate', 'waiting']
                    },
                    {
                        [this.Op.eq]: null
                    },
                    ],
                },
                // AND validations.val_od_version
                '$validations.val_od_version$': {
                    [this.Op.or]: [{
                        [this.Op.eq]: this.sequelize.col('osmdiff.od_new_version')
                    },
                    {
                        [this.Op.eq]: null
                    },
                    ],
                },
				...intersectQuery,
            };

            // S'il y a filtre alors il faut joindre plus de tables (ce qui rend la requête plus longue)
            let haveFilter = false
            if (filter && Object.keys(filter) && Object.keys(filter).length > 0) {
                haveFilter = true;
            }

            // Jointures de la requête
            const includes = haveFilter ? this.getCompleteIncludes() : this.getSimpleIncludes(footprintId);

            // Attributs
            const attributes = haveFilter ? null : ['id', 'name', 'new_tags', 'old_tags', 'added_tags', 'deleted_tags', 'modified_tags'];

            this.requestOsmDiffs(whereClause, includes, attributes, filter).then(diffs => {
                const aIds = [];
                for (let i = 0; i < diffs.length; i++) {
                    const diff = diffs[i];
                    aIds.push(diff.id);
                }
                resolve(aIds);
            });
        });
    }

    /**
     * Trouve l'ensemble des infos des osmdiffs à partir d'une liste d'identifiants
     * @param {*} aIds 
     * @param {*} filter 
     * @returns 
     */
    findOsmDiffsDetail(aIds, filter) {
        return new Promise((resolve, reject) => {

            // Clause where de la requête
            const whereClause = {
                id: aIds
            }

            // Jointures de la requête
            const includes = this.getCompleteIncludes();

            // Attributs
            const attributes = undefined;

            this.requestOsmDiffs(whereClause, includes, attributes, null).then(diffs => {
                resolve(diffs);
            });
        });
    }

    /**
     * Fonction générique pour requêter les osmDiffs
     * @param {*} whereClause 
     * @param {*} includes 
     * @param {*} attributes 
     * @param {*} filter 
     * @returns 
     */
    requestOsmDiffs(whereClause, includes, attributes, filter) {
        return new Promise((resolve, reject) => {

            this.db.osmdiff.findAll({
                where: whereClause,
                order: [
                    ['name', 'ASC'],
                    ['id', 'ASC'],
                ],
                include: includes,
                attributes: attributes,
            }).then((osmdiff) => {
                if (filter) {
                    resolve(this.filterOsmDiffs(osmdiff, filter));
                } else {
                    resolve(osmdiff);
                }
            }).catch(err => {
                reject(err);
            });

        });
    }

    /**
     * Applique le filtre sur les osmdiffs
     * @param {*} osmdiff 
     * @param {*} filter 
     * @returns 
     */
    filterOsmDiffs(osmdiff, filter) {
        if (filter && Object.keys(filter) && Object.keys(filter).length > 0) {
            osmdiff = osmdiff.filter((o) => {
                // filter by validations for the associated rights
                // tous les tags modifiés de l'objet
                const osmKeys = Object.keys({
                    //...o.new_tags,
                    //...o.old_tags,
                    ...o.added_tags,
                    ...o.deleted_tags,
                    ...o.modified_tags,
                });
                // On y ajoute le tag virtuel "lbt_geom"
                osmKeys.push('lbt_geom');

                // validations are valid only because osm object is to validate
                // tags validés
                const validatedKeys = o.validations
                    .filter((v) => v.status.name === 'valid')
                    .reduce((a, v) => [].concat(a, v.tags), []);

                // Si tous les tags sont à valider (case cochée dans les droits), on met tous les tags ajoutés, modifiés, supprimés à la liste des tags à valider depuis les droits
                // Sinon ce sont les tags précisés dans les droits de validation
                let rightKeys;
                const verif_all_tags = this.obj.rights.reduce((a, r) => [].concat(a, r.r_verif_all_tags), []);
                if (verif_all_tags.includes(true)) {
                    rightKeys = Object.keys({
                        ...o.added_tags,
                        ...o.deleted_tags,
                        ...o.modified_tags,
                    });
                } else {
                    // tags à valider depuis les droits
                    rightKeys = this.obj.rights.reduce((a, r) => [].concat(a, r.keys), []).map((k) => k.name);
                }

                // Si la géométrie est à valider (case cochée dans les droits) et que la géométrie de l'objet a été modifiée, on ajoute le tag virtuel "lbt_geom" à la liste des tags à valider depuis les droits
                if (o.changed_geom) {
                    const verif_geom = this.obj.rights.reduce((a, r) => [].concat(a, r.r_verif_geom), []);
                    if (verif_geom.includes(true)) {
                        rightKeys.push('lbt_geom');
                    }
                }

                // find keys to validate i.e. osm keys that exist in right
                // tags à valider : différence entre les tags présents modifiés et les tags à valider
                const keysToValidate = osmKeys.filter((k) => rightKeys.includes(k));

                // find remaining keys to validate i.e. keys that are not valid yet
                // tags qui restent à valider
                const remainingKeys = keysToValidate.filter((k) => !validatedKeys.includes(k));

                // is there some keys?
                // S'il reste des tags à valider, affiche l'objet
                return !!remainingKeys.length;
            })
                // Filtre passé par l'URL
                .filter((osm) => {
                    try {
                        filter = filter ? filter : {};
                        filter.contributors = filter.contributors ? filter.contributors : [];
                        filter.vigilance = filter.vigilance ? filter.vigilance : {};
                        return (
                            (!filter.contributors.length ||
                                (osm.contributor &&
                                    filter.contributors.some(
                                        (c) => c.id === osm.contributor.id,
                                    ))) &&
                            (!filter.vigilance.id ||
                                (osm.contributor &&
                                    osm.contributor.vigilance &&
                                    filter.vigilance.id ===
                                    osm.contributor.vigilance.id))
                        );
                    } catch (e) {
                        return true;
                    }
                })
        }

        return osmdiff;
    }

    /**
     * Retourne les inclusions simples (mais rapides) pour requestOsmDiffs
     * @returns
     */
    getSimpleIncludes(footprintId) {
        return [{
            association: this.OsmDiff.Status,
            required: false,
            attributes: ['st_name'],
        },
        {
            association: this.OsmDiff.Validation,
            required: false,
            attributes: ['val_od_version'],
        },
        {
            association: this.OsmDiff.Footprint,
            attributes: ['fp_id'],
        },
        ]
    }

    /**
     * Retourne les inclusions complètes (mais lentes) pour requestOsmDiffs (jointures)
     * @returns 
     */
    getCompleteIncludes() {
        return [{
            association: this.OsmDiff.NewChangeset,
            // attributes: ['ocs_id'],
        },
        {
            association: this.OsmDiff.Status,
            required: false,
            // attributes: ['st_name'],
        },
        {
            association: this.OsmDiff.Validation,
            required: false,
            include: [{
                association: this.Validation.Status
            },
            {
                association: this.Validation.User,
                // attributes: ['u_id', 'u_login', 'u_first_name', 'u_last_name'],
            },
            ],
        },
		// Récupère l'id fp_id de l'emprise associée (jointure 1-n : plusieurs emprises possibles)
		{
            association: this.OsmDiff.Footprint,
			attributes: ['fp_id'],
        },
        {
            association: this.OsmDiff.Contributor,
            // attributes: ['ct_name'],
            include: [{
                association: this.Contributor.Vigilance,
                // attributes: ['vg_color'],
            },],
        }];
    }

    /**
     * fonction de génération clause WHERE colonne contient (@>) hstore
     * @param {*} col 
     * @param {*} hArray 
     * @returns 
     */
    hstoreContainHstore(col, hArray) {
        return this.sequelize.where(
            this.sequelize.col(col),
            '@>',
            this.sequelize.fn(
                'hstore',
                this.sequelize.literal(`ARRAY['${hArray.join('\',\'')}']`),
            ),
        );
    }

    /**
     * fonction de génération clause WHERE colonne - opérateur - clés
     * @param {*} col 
     * @param {*} op 
     * @param {*} keys 
     * @returns 
     */
    hstoreContainKeys(col, op, keys) {
        return this.sequelize.where(
            this.sequelize.col(col),
            op,
            this.sequelize.literal(`ARRAY['${keys.join('\',\'')}']`),
        );
    }

    /**
     * Traduit une requête JSON en requête sequelize
     * @param {*} col 
     * @param {*} op 
     * @param {*} keys 
     * @returns 
     */
    translateQuery(col, parsedQuery) {

        const firstOp = this.getFirstOperator(parsedQuery);
        if (Array.isArray(parsedQuery[firstOp]) && firstOp !== null) {
            const conditions = [];

            for (let i = 0; i < parsedQuery[firstOp].length; i++) {
                const node = parsedQuery[firstOp][i];
                const key = Object.keys(node)[0];
                const val = node[key];

                if (typeof val === 'string') {
                    if (val === '*') {
                        conditions.push(this.sequelize.where(
                            this.sequelize.col(col),
                            '?',
                            `${key}`
                        ));
                    } else {
                        conditions.push(this.sequelize.where(
                            this.sequelize.col(col),
                            '@>',
                            `"${key}"=>"${val}"`
                        ));
                    }

                } else if (typeof val === 'object') {

                        // Opérateur NOT
                        if ('[op.ne]' in val) {
                            const opVal = val['[op.ne]'];
                            const testSQL = 'not ' + col + ' @> \'' + `"${key}"=>"${opVal}"` + '\''
                            conditions.push(this.sequelize.literal(`(${testSQL})`));

                        // Les opérateurs > < >= <= ne peuvent pas être utilisés avec du HSTORE, ils sont donc ignorés
                        } else if ('[op.gt]' in val) {
                            const opVal = val['[op.gt]'];

                        } else if ('[op.gte]' in val) {
                            const opVal = val['[op.gte]'];

                        } else if ('[op.lt]' in val) {
                            const opVal = val['[op.lt]'];

                        } else if ('[op.lte]' in val) {
                            const opVal = val['[op.lte]'];

                        } else {
                            conditions.push(this.translateQuery(col, node));
                        }
                } 
            }

            if (firstOp === '[op.or]') {
                return {
                    [this.Op.or]: conditions
                };
            } else if (firstOp === '[op.and]') {
                return {
                    [this.Op.and]: conditions
                };
            }
        }
    }

    /**
     * Retourne le premier opérateur présent
     * @param {*} col 
     * @param {*} op 
     * @param {*} keys 
     * @returns 
     */
    getFirstOperator(parsedQuery) {
        if (parsedQuery['[op.or]']) {
            return '[op.or]';
        } else if (parsedQuery['[op.and]']) {
            return '[op.and]';
        } else {
            return false;
        }
    }

    /**
     * clause WHERE col / requête du groupe d'objets (og_query)
     * @param {*} col 
     * @returns 
     */
    compareQuery(col) {
        const clauses = [];
        // Requête groupe d'objets
        if (this.obj.query_json) {
            const query = this.translateQuery(col, this.obj.query_json);
            clauses.push(query);
        }
        // Si query est vide, on crée une clause WHERE toujours vraie à la place de hstoreContainHstore
        else {
            clauses.push(
                this.sequelize.where(
                    this.sequelize.col(col),
                    '=',
                    this.sequelize.col(col),
                )
            );
        }

        // clauses renvoie une clause WHERE pour la future requête
        return clauses;
    }

    /**
     * fonction de comparaison avec les tags obligatoires du groupe d'objets (og_tags)
     * @param {*} col 
     * @returns 
     */
    compareTags(col) {
        const clauses = [];

        if (this.obj.keys.length) {
            const keys = this.obj.keys.map((k) => k.name);
            clauses.push(this.hstoreContainKeys(col, '?&', keys));
        }
        // Si keys est vide, on crée une clause WHERE toujours vraie à la place de hstoreContainKeys
        else {
            clauses.push(
                this.sequelize.where(
                    this.sequelize.col(col),
                    '=',
                    this.sequelize.col(col),
                )
            );
        }

        return clauses;
    }

    /**
     * fonction de comparaison avec les tags à valider (droits)
     * @param {*} col 
     * @returns 
     */
    compareRights(col) {
        const clauses = [];
        if (this.obj.rights.length) {
            const keys = this.obj.rights.reduce((a, r) => [].concat(a, r.keys), []).map((k) => k.name);
            clauses.push(this.hstoreContainKeys(col, '?|', keys));
        }
        return clauses;
    }

    /**
     * Est-ce que la case "Valider les objets dont n'importe quel attribut a été modifié" est cochée (r_verif_all_tags) ?
     * @param {*} col 
     * @returns 
     */
    VerifAllTagsCase() {
        let result = false;
        if (this.obj.rights.length) {
            const verif_all_tags = this.obj.rights.reduce((a, r) => [].concat(a, r.r_verif_all_tags), []);
            if (verif_all_tags.includes(true)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Est-ce que la case "Valider les objets dont la géométrie a changé" est cochée (r_verif_geom) ?
     * @param {*} col 
     * @returns 
     */
    VerifGeomCase() {
        let result = false;
        if (this.obj.rights.length) {
            const verif_geom = this.obj.rights.reduce((a, r) => [].concat(a, r.r_verif_geom), []);
            if (verif_geom.includes(true)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Est-ce que "Valider les objets dont les attributs suivants ont été modifiés" contient des attributs ?
     * @param {*} col 
     * @returns 
     */
    VerifSelectedTags() {
        let result = false;
        if (this.obj.rights.length) {
            const keys = this.obj.rights.reduce((a, r) => [].concat(a, r.keys), []).map((k) => k.name);
            if (keys.length) {
                result = true;
            }
        }
        return result;
    }

    /**
     *  Quels sont les attributs à valider ?
     * @param {*} col 
     * @returns 
     */
    VerifTagsToValidate() {
        const tagsToValidate = [];
        if (this.obj.rights.length) {
            const keys = this.obj.rights.reduce((a, r) => [].concat(a, r.keys), []).map((k) => k.name);
            tagsToValidate.push(keys);
        }
        return tagsToValidate;
    }


	// N'est plus utilisée (st_intersects à la volée trop long)
    /**
     * Intersection par emprise
     * @param {*} footprintId 
     * @returns 
     */
    footprintIntersect(footprintId) {
        const tempSQL = this.sequelize.dialect.QueryGenerator.selectQuery('lebontag.lbt_footprint', {
            // ST_CollectionExtract(geom,3) convertit les multicollections en multipolygons
            attributes: [this.sequelize.literal('(st_dump(ST_CollectionExtract(lbt_footprint.fp_geom,3))).geom as fp_geom')],
            where: {
                fp_id: footprintId,
            },
            // limit inutile car req basée sur ID unique de l'emprise
            //limit: 1,
        })
            .slice(0, -1).replace(/"lebontag.lbt_footprint"/g, `"lebontag"."lbt_footprint"`);

        return this.sequelize.where(this.sequelize.fn(
            'ST_Intersects',
            this.sequelize.col('od_new_geom'),
            this.sequelize.literal(`(${tempSQL})`)
        ), true)
    }
}