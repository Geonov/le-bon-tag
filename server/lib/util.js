import { ThemeObjectService } from "./themeObject";

/**	Creates a callback that proxies node callback style arguments to an Express Response object.
 *	@param {express.Response} res	Express HTTP Response
 *	@param {number} [status=200]	Status code to send on success
 *
 *	@example
 *		list(req, res) {
 *			collection.find({}, toRes(res));
 *		}
 */
export function toRes(res, status = 200) {
	return (err, thing) => {
		if (err) return res.status(500).send(err);

		if (thing && typeof thing.toObject === 'function') {
			thing = thing.toObject();
		}
		res.status(status).json(thing);
	};
}

/**
 * Récupère les settings
 * @param {*} db 
 * @returns 
 */
export function getSettings(db) {
	return new Promise((resolve, reject) => {

		const Setting = db.setting;
		Setting.findAll()
			.then((res) => {
				const settings = res.reduce((a, s) => {
					a[s.name] = s.value;
					return a;
				}, {});
				resolve(settings);
			}).catch(err => {
				reject(err);
			});
	});
}

/**
 * Retourne la géométrie d'un footprint
 * @param {*} db 
 * @param {*} footprintId 
 * @returns 
 */
export function getFootprintGeom(db, footprintId) {
	return new Promise((resolve, reject) => {
		const Footprint = db.footprint;
		Footprint.findByPk(footprintId)
			.then(footprint => {

				let insersectGeom;
				if (footprint &&
					footprint.dataValues &&
					footprint.dataValues.geom) {
					insersectGeom = footprint.dataValues.geom;
				}

				resolve(insersectGeom);
			}).catch(err => {
				reject(err);
			});
	});
}

/**
 * Retourne l'arbre des thèmes avec les diffs associées
 * @param {*} db 
 * @param {*} themes 
 * @param {*} footprintId
 * @param {*} filter 
 * @returns 
 */
export function getThemesOsmDiff(db, themes, footprintId, filter) {
	return new Promise((resolve, reject) => {
		// Paramètres
		getSettings(db).then((settings) => {
			footprintId = footprintId || settings.osm_footprint;

			Promise.all(themes.map((theme) => {
				return new Promise((resolve, reject) => {
					getThemeOsmDiff_(db, theme, settings, footprintId, filter).then(res => {
						theme = res[0];
						if (theme.objects) {
							let nbThemeDiff = 0;
							for (let i = 0; i < theme.objects.length; i++) {
								if (theme.objects[i].nbOsmDiff) {
									nbThemeDiff += theme.objects[i].nbOsmDiff;
								}
							}
							theme.nbThemeDiff = nbThemeDiff;
						}
						resolve(theme);
					}).catch(err => {
						reject(err);
					});
				});
			})).then((tree) => {
				resolve(tree);
			}).catch(err => {
				reject(err);
			});;
		});
	});
}

/**
 * Retourne un thème avec les diffs associées
 * @param {*} db 
 * @param {*} theme 
 * @param {*} settings 
 * @param {*} footprintId 
 * @param {*} filter 
 * @returns 
 */
export function getThemeOsmDiff_(db, theme, settings, footprintId, filter) {
	let tmpObjects = [];
	return Promise.all(theme.objects.map((object) => {
		return new Promise((resolve, reject) => {
			const themeObjectSrv = new ThemeObjectService(db, object, theme, settings);
			themeObjectSrv.findOsmDiffs(footprintId, filter).then((res) => {
				const jTheme = theme.toJSON();
				tmpObjects.push({
					...object.toJSON(),
					osmdiff: res.osmdiff.map((d) => {

						// Ajout de l'id du groupe
						d.dataValues.og_gr_id = object.id;

						// Retour
						return d.toJSON();
					}),
					nbOsmDiff: res.nbOsmDiff,
				});
				jTheme.objects = tmpObjects;
				resolve(jTheme);
			});
		});
	}));
}