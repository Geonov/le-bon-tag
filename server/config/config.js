import dotenv from 'dotenv';
const result = dotenv.config();
if (result.error) {
	throw result.error
}

export default {
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB,
    schema: process.env.POSTGRES_SCHEMA,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    dialect: 'postgres',
    jwt_secret: process.env.JWT_SECRET,
    logging: process.env.LOGGING === 'true' && console.log,
    epsgOsm: process.env.EPSG_OSM,
    epsgLocal: process.env.EPSG_LOCAL
};
