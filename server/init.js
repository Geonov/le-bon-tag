import db from './models';

db.sequelize.sync({
    logging: console.log,
    force: true,
});
