import http from 'http';
import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import config from './config.json';

const app = express();
app.server = http.createServer(app);

app.use(bodyParser.json({
	limit : config.bodyLimit
}));

// connect to db
initializeDb( db => {

	// internal middleware
	app.use(middleware({ config, db }));

	// api router
	app.use('/api', api({ config, db }));
});

// serve static assets normally
app.use(express.static(path.join(__dirname, '..', 'dist')));

// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
app.get('*', (req, res) => {
	res.sendFile(path.resolve(__dirname, '..', 'dist/index.html'));
});

const port = process.env.PORT || config.port;
app.server.listen(port, () => {
	console.log(`Started on port ${port}!`);
});

export default app;
