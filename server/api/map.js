import { Router } from 'express';
import { getThemesOsmDiff } from '../lib/util';

function getThemes(db, user) {

	const Theme = db.theme;
	const ObjectGroup = db.objectGroup;
	const Right = db.right;
	const BusinessGroup = db.businessGroup;
	const User = db.user;
	const Op = db.Sequelize.Op;

	return new Promise((resolve, reject) => {

		Theme.findAll({
			where: {
				'$objects.rights.business.users.u_id$': { [Op.eq]: user.id },
				// Ne s'occupe que des groupes d'objets actifs
				'$objects.og_active$': { [Op.eq]: true },
				// Ne s'occupe pas des groupes d'objets en auto-validation
				'$objects.og_auto$': { [Op.ne]: true },
			},
			order: [
				['name', 'ASC'],
				[Theme.ObjectGroup, 'name', 'ASC']
			],
			include: [{
				model: ObjectGroup,
				as: 'objects',
				include: [{
					association: ObjectGroup.Keys,
				},
				{
					model: Right,
					as: 'rights',
					include: [{
						association: Right.Keys,
					},
					{
						model: BusinessGroup,
						as: 'business',
						include: [{
							model: User,
							as: 'users',
							// only attribute for restrict results without loading User
							attributes: ['u_id'],
						}]
					}
					],
				}
				],
			},]
		})
			.then((themes) => {
				resolve(themes);
			})
			.catch(err => {
				reject(err);
			})
	});
}

export default ({
	config,
	db
}) => {
	let map = Router();

	map.get('/menu', (req, res, next) => {

		const user = req.authenticated_user;

		// Thèmes associés à l'utilisateur
		getThemes(db, user).then(themes => {

			// Filtre
			let filter = {};
			if (req.query && req.query.filter) {
				try {
					filter = JSON.parse(req.query.filter);
				} catch (error) { }
			}

			getThemesOsmDiff(db, themes, req.query.footprint, filter).then(tree => {
				return res.json(tree);
			}).catch(err => {
				res.status(500).json({
					error: err
				})
			});
		}).catch(err => {
			res.status(500).json({
				error: err
			})
		});
	});

	map.get('/menu/object/:id', (req, res, next) => {
		const Right = db.right;
		// const Tag = db.tag;
		const ObjectGroup = db.objectGroup;

		Right.findByPk(
			req.params.id, {
			include: [{
				association: Right.Object,
				// model: ObjectGroup,
				// as: 'object',
				include: [{
					association: ObjectGroup.Keys
				}],
			},
			{
				association: Right.Keys,
				// model: Tag,
				// as: 'keys',
			}
			],
		})
			.then((right) => {
				const obj = right.object.toJSON();
				const sequelize = db.sequelize;
				const Op = db.Sequelize.Op;

				const hstoreContainHstore = (col, hArray) =>
					sequelize.where(
						sequelize.col(col),
						'@>',
						sequelize.fn(
							'hstore',
							sequelize.literal(`ARRAY['${hArray.join('\',\'')}']`),
						),
					);

				const hstoreContainKeys = (col, op, keys) =>
					sequelize.where(
						sequelize.col(col),
						op,
						sequelize.literal(`ARRAY['${keys.join('\',\'')}']`),
					);

				const getClauses = (col) => {
					const clauses = [];

					if (Object.keys(obj.query || {}).length) {
						const query = Object
							.entries(obj.query)
							.reduce((a, [k, v]) => a.concat(k, v), []);
						// @> hstore(ARRAY[:query])
						clauses.push(hstoreContainHstore(col, query));
					}
					if (obj.keys.length) {
						const keys = obj.keys.map((k) => k.name);
						// ?& ARRAY[:keys]
						clauses.push(hstoreContainKeys(col, '?&', keys));
					}
					if (right.getKeys().length) {
						const keys = right.getKeys().map((k) => k.name);
						// ?| ARRAY[:keys]
						clauses.push(hstoreContainKeys(col, '?|', keys));
					}
					return clauses;
				}

				const newClauses = getClauses('od_new_tags');
				const oldClauses = getClauses('od_old_tags');

				return (
					//newClauses.length ?
					db.osmdiff.findAll({
						// attributes: {
						// 	include: [
						// 		[db.sequelize.fn('ST_Transform', db.sequelise.col('od_new_geom'), 4326), 'od_new_geom'],
						// 		[db.sequelize.fn('ST_Transform', db.sequelise.col('od_old_geom'), 4326), 'od_old_geom'],
						// 	],
						// },
						where: {
							[Op.or]: [{
								[Op.and]: [
									...newClauses,
									{
										action: ['create', 'modify']
									}
								],
							},
							{
								[Op.and]: [
									...oldClauses,
									{
										action: 'delete'
									}
								],
							},
							],
						},
						include: [{
							association: db.osmdiff.NewChangeset
						},
						{
							model: db.contributor,
							as: 'contributor',
							include: [{
								association: db.contributor.Vigilance
							}],
						},
						],
					})
					//: Promise.resolve([])
				)
					.then((diff) => {
						return Promise.resolve({
							...obj,
							osmdiff: diff.map((d) => d.toJSON()),
						});
					})
					.catch((err) => {
						console.log('err', err);
						throw err;
					})
			})
			.then((object) => res.json(object))
			.catch((err) => res.status(500).json({
				error: err
			}));
	});

	return map;
}