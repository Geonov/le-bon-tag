import resource from 'resource-router-middleware';
import {
	getThemesOsmDiff
} from '../lib/util';

/**
 * Retourne les tags à valider par l'utilisateur pour un object
 * @param {*} db 
 * @param {*} user 
 * @param {*} object
 * @param {*} footprint
 */
function getAuthorizedTags(db, user, object, footprint) {
	const Theme = db.theme;
	const ObjectGroup = db.objectGroup;
	const Right = db.right;
	const BusinessGroup = db.businessGroup;
	const User = db.user;
	const Op = db.Sequelize.Op;

	return new Promise((resolve, reject) => {

		Theme.findAll({
			where: {
				'$objects.rights.business.users.u_id$': { [Op.eq]: user.id },
				// Ne s'occupe pas des groupes d'objets inactifs
				'$objects.og_active$': { [Op.eq]: true },
			},
			order: [
				['name', 'ASC'],
				[Theme.ObjectGroup, 'name', 'ASC']
			],
			include: [{
				model: ObjectGroup,
				as: 'objects',
				include: [{
					association: ObjectGroup.Keys,
				},
				{
					model: Right,
					as: 'rights',
					include: [{
						association: Right.Keys,
					},
					{
						model: BusinessGroup,
						as: 'business',
						include: [{
							model: User,
							as: 'users',
							// only attribute for restrict results without loading User
							attributes: ['u_id'],
						}]
					}
					],
				}
				],
			},]
		})
			.then((themes) => {
				getThemesOsmDiff(db, themes, footprint, null).then(tree => {
					let authorizedKeys = [];
					for (let i = 0; i < tree.length; i++) {
						const themetags = getAuthorizedTagsByObject_(tree[i], object);
						if (themetags) {
							authorizedKeys = [...authorizedKeys, ...themetags]
						}
					}
					resolve(authorizedKeys);
				});
			})
	});
}

/**
 * Retourne tous les tags à valider pour un object (tous les utilisateurs)
 * @param {*} db 
 * @param {*} object
 * @param {*} footprint
 */
function getTagsToValidate(db, object, footprint) {
	const Theme = db.theme;
	const ObjectGroup = db.objectGroup;
	const Right = db.right;
	const BusinessGroup = db.businessGroup;
	const Op = db.Sequelize.Op;

	return new Promise((resolve, reject) => {

		Theme.findAll({
			order: [
				['name', 'ASC'],
				[Theme.ObjectGroup, 'name', 'ASC']
			],
			include: [{
				model: ObjectGroup,
				as: 'objects',
				include: [{
					association: ObjectGroup.Keys,
				},
				{
					model: Right,
					as: 'rights',
					include: [{
						association: Right.Keys,
					},
					{
						model: BusinessGroup,
						as: 'business',
					}
					],
				}
				],
			},]
		})
			.then((themes) => {
				getThemesOsmDiff(db, themes, footprint, null).then(tree => {
					let authorizedKeys = [];
					for (let i = 0; i < tree.length; i++) {
						const themetags = getAuthorizedTagsByObject_(tree[i], object);
						if (themetags) {
							authorizedKeys = [...authorizedKeys, ...themetags]
						}
					}
					resolve(authorizedKeys);
				});
			})
	});
}

/**
 * Retourne le groupe d'objets en fonction d'un osmdiff id
 * @param {*} tree
 * @param {*} object 
 */
function getGroupByObject_(tree, object) {
	if (tree.objects) {
		for (let ii = 0; ii < tree.objects.length; ii++) {
			const group = tree.objects[ii];
			if (object.og_gr_id === group.id) {
				return group;
			}
		}
	}
}

/**
 * Retourne les tags autorisés pour un osmdiff id
 * @param {*} themes : thème associé aux groupes d'objets qui contiennent les droits et les objets
 * @param {*} object : objet OSM en cours de validation
 */
function getAuthorizedTagsByObject_(themes, object) {
	// group = tous les objets du groupe d'objet
	// group.rights = droits du groupe d'objet
	const group = getGroupByObject_(themes, object);

	const authorizedTags = [];
	if (group && group.rights) {
		for (let i = 0; i < group.rights.length; i++) {
			const right = group.rights[i];

			// Si tous les tags sont à vérifier, on ajoute tous les tags comme tags validés dans la table lbt_validation
			if (right.r_verif_all_tags) {
				const keys = (Object.keys(object.added_tags)).concat(Object.keys(object.deleted_tags)).concat(Object.keys(object.modified_tags));
				for (let ii = 0; ii < keys.length; ii++) {
					const key = keys[ii];
					authorizedTags.push(key);
				}
			}
			// Sinon on ajoute uniquement les tags à valider issus des droits
			else if (right.keys) {
				for (let ii = 0; ii < right.keys.length; ii++) {
					const rightKey = right.keys[ii];
					if (rightKey.name) {
						authorizedTags.push(rightKey.name);
					}
				}
			}

			// Si la géométrie est à vérifier, on ajoute un faux tag "lbt_geom" dans la table lbt_validation
			if (right.r_verif_geom) {
				authorizedTags.push('lbt_geom');
			}

		}
	} else {
		return null;
	}

	//return authorizedTags;
	// Supprime les éventuels tags en doublon (si plusieurs droits)
	return authorizedTags.filter((value, index) => authorizedTags.indexOf(value) === index);
}

export default ({
	config,
	db
}) => resource({

	/** Property name to store preloaded entity on `request`. */
	id: 'validation',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Validation = db.validation;

		Validation.findByPk(
			id, {
			include: [
				Validation.User,
				Validation.Status,
			]
		}
		)
			.then(validation => callback(null, validation))
			.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({
		params
	}, res) {
		const Validation = db.validation;

		Validation.findAll({
			include: [
				Validation.User,
				Validation.Status,
			],
		})
			.then(validations => res.json(validations))
			.catch(err => res.status(500).json({
				error: err
			}));
	},

	/** POST / - Create a new entity */
	create({
		body
	}, res) {

		const Validation = db.validation;
		const OsmDiff = db.osmdiff;
		const Op = db.Sequelize.Op;

		body.val_st_id = body.status.id;
		body.val_u_id = body.user.id;
		body.val_od_id = body.object.id;

		// const footprint = body.footprint;
		const footprint = 'none';

		// Tous les tags à valider qu'importe l'utilisateur (contiendra lbt_geom si présent)
		getTagsToValidate(db, body.object, footprint).then((objectAllTags) => {
			console.log('object : all tags to validate (global)', objectAllTags);
			body.allTags = objectAllTags;

			// Tous les tags à valider par l'user en cours
			getAuthorizedTags(db, body.user, body.object, footprint).then((objectTags) => {
				console.log('object : all tags to validate by user', objectTags);
				body.tags = objectTags;

				// Tous les tags modifiés sur l'objet en cours
				body.allModifiedTags = [].concat(Object.keys(body.object.deleted_tags), Object.keys(body.object.modified_tags), Object.keys(body.object.added_tags));
				// Si la géométrie de l'objet a été modifiée, on y ajoute le tag "lbt_geom"
				if (body.object.changed_geom) {
					body.allModifiedTags.push('lbt_geom');
				}
				console.log('object : all modified tags for this object', body.allModifiedTags);

				// Création de la validation
				Validation.create(body)
					.then((created) => {

						// Si status vaut invalid ou waiting, ne vérifie pas la liste des tags
						if (['invalid', 'waiting'].includes(body.status.name)) {
							// don't check validation tags list
							return OsmDiff.findByPk(body.object.id)
								.then((osm) => osm.setStatus(body.status.id))
								.then(() => created);
						}

						// Status vaut valid
						// Recherche dans les validations avec tags non null qui ont été faites sur le même objet
						return Validation.findAll({
							where: {
								tags: {
									[Op.ne]: null
								},
							},
							include: [{
								association: Validation.Object,
								attributes: ['id'],
								where: {
									id: body.object.id,
								},
							}],
						}).then((validations) => {

							// Tags des validations sur le même objet (nouvellement et anciennement validées)
							const validedTags = validations.reduce((a, v) => [].concat(a, ...v.tags), []);
							console.log('validedTags', validedTags);

							// Si aucun tag validé
							if (!validedTags.length) {
								// Change "status" de l'osmdiff en created
								return OsmDiff.findByPk(body.object.id)
									.then((osm) => {
										if (osm.changed_geom) {
											return osm.setStatus(body.status.id).then(() => created);
										}
										console.error(`no validation tags found for object ${body.object.id}`);
										return created;
									});
							}

							// Tags restants à valider théoriquement (tous les tags à valider - tous les tags validés)
							const tagsToValidate = (body.allTags).filter(x => !validedTags.includes(x));

							// Tags restants à valider réellement (intersection entre tags réellement présents dans l'objet et tags restants à valider théoriquement)
							const tagsRemainToValidate = (body.allModifiedTags).filter(x => tagsToValidate.includes(x));
							console.log('tags remain', tagsRemainToValidate);

							// Si des tags restent à valider
							if (tagsRemainToValidate.length) {
								return created;
							} else {
								// Autrement set le status de l'osmdiff à created (objet validé)
								return OsmDiff.findByPk(body.object.id)
									.then((osm) => osm.setStatus(body.status.id))
									.then(() => created);
							}
						})

					})
					.then((validation) => Validation.findByPk(
						validation.id, {
						include: [
							Validation.User,
							Validation.Status,
							Validation.Object,
						],
					}))
					.then((reloaded) => res.json(reloaded))
					.catch(err => res.status(500).json({
						error: err
					}));

			})
		});
	},

	/** GET /:id - Return a given entity */
	read({
		validation
	}, res) {
		res.json(validation);
	},

	/** PUT /:id - Update a given entity */
	update({
		validation,
		body
	}, res) {
		res.status(501);
	},

	/** DELETE /:id - Delete a given entity */
	delete(_, res) {
		res.status(501);
	}
});