import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'wms',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Wms = db.wms;
	
		Wms.findByPk(id)
			.then(wms => callback(null, wms))
        	.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Wms = db.wms

		Wms.findAll({
			order: [
				['name', 'ASC'],
			],
		})
		.then(wms => res.json(wms))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Wms = db.wms
		Wms.create(body).then((wms) => res.json(wms)).catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ tag }, res) {
		res.json(tag);
	},

	/** PUT /:id - Update a given entity */
	update({ wms, body }, res) {
		wms.update(body)
		.then(() => (wms.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ wms }, res) {
		wms.destroy()
		.then(() => res.sendStatus(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
