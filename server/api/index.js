import { version } from '../../package.json';
import { Router } from 'express';
import tag from './tag';
import user from './user';
import businessGroup from './businessGroup';
import userType from './userType';
import userRole from './userRole';
import contributor from './contributor';
import right from './right';
import theme from './theme';
import objectGroup from './objectGroup';
import tile from './tile';
import wms from './wms';
import wmts from './wmts';
import vigilance from './vigilance';
import validation from './validation';
import setting from './setting';
import status from './status';
import state from './state';
import map from './map';
import lbtExport from './lbtExport';
import rattachment from './rattachment';
import footprint from './footprint';
import userFootprint from './userFootprint';
import { auth, authorization } from './auth';

export default ({ config, db }) => {
	let api = Router();

	// perhaps expose some API metadata at the root
	api.get(/^\/$/, (req, res) => {
		res.json({ version });
	});

	api.post(
		'/login',
		auth(db)
	);

	// check authorization and get authenticated user
	// TODO filter some resources, not all routes need to fetch user
	api.use(authorization(db));
	
	// mount the tag resource
	api.use('/tag', tag({ config, db }));

	// mount the user resource
	api.use('/user', user({ config, db }));

	// mount the business resource
	api.use('/business-group', businessGroup({ config, db }));

	// mount the userType resource
	api.use('/user-type', userType({ config, db }));

	// mount the userRole resource
	api.use('/user-role', userRole({ config, db }));

	// mount the contributor resource
	api.use('/contributor', contributor({ config, db }));

	// mount the vigilance resource
	api.use('/vigilance', vigilance({ config, db }));

	// mount the theme resource
	api.use('/theme', theme({ config, db }));

	// mount the objectGroup resource
	api.use('/object-group', objectGroup({ config, db }));

	// mount the right resource
	api.use('/right', right({ config, db }));

	// mount the tile resource
	api.use('/tile', tile({ config, db }));

	// mount the wms resource
	api.use('/wms', wms({ config, db }));

	// mount the wmts resource
	api.use('/wmts', wmts({ config, db }));

	// mount the setting resource
	api.use('/setting', setting({ config, db }));

	// mount the validation resource
	api.use('/validation', validation({ config, db }));

	// mount the status resource
	api.use('/status', status({ config, db }));

	// mount the state resource
	api.use('/state', state({ config, db }));
	
	// mount the map resource
	api.use('/map', map({ config, db }));
	
	// mount the generate-export resource
	api.use('/export', lbtExport({ config, db }));

	// mount the rattachment resource
	api.use('/rattachment', rattachment({ config, db }));

	// mount the footprint resource
	api.use('/footprint', footprint({ config, db }));

	// mount the userFootprint resource
	api.use('/user-footprint', userFootprint({ config, db }));

	api.get('/check/tag/name/:value', (req, res) => {
		const { value } = req.params;

		const Model = db.tag;
		Model.findOne({ where: { name: value, deleted: false, }})
		.then((object) => res.json({ statut: !object }))
		.catch((error) => res.status(500).json({ error: error }));
	});

	api.get('/check/:model/:field/:value', (req, res) => {
		const { model, field, value } = req.params;

		const Model = db[model];
		Model.findOne({ where: { [field]: value }})
		.then((object) => res.json({ statut: !object }))
		.catch((error) => res.status(500).json({ error: error }));
	});

	return api;
}
