import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'wmts',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Wmts = db.wmts;
	
		Wmts.findByPk(id)
			.then(wmts => callback(null, wmts))
        	.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Wmts = db.wmts

		Wmts.findAll({
			order: [
				['name', 'ASC'],
			],
		})
		.then(wmts => res.json(wmts))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Wmts = db.wmts
		Wmts.create(body).then((wmts) => res.json(wmts)).catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ tag }, res) {
		res.json(tag);
	},

	/** PUT /:id - Update a given entity */
	update({ wmts, body }, res) {
		wmts.update(body)
		.then(() => (wmts.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ wmts }, res) {
		wmts.destroy()
		.then(() => res.sendStatus(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
