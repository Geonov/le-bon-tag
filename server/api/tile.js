import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'tile',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Tile = db.tile;
	
		Tile.findByPk(id)
			.then(tile => callback(null, tile))
        	.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Tile = db.tile

		Tile.findAll({
			order: [
				['name', 'ASC'],
			],
		})
		.then(tile => res.json(tile))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Tile = db.tile
		Tile.create(body).then((tile) => res.json(tile)).catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ tag }, res) {
		res.json(tag);
	},

	/** PUT /:id - Update a given entity */
	update({ tile, body }, res) {
		tile.update(body)
		.then(() => (tile.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ tile }, res) {
		tile.destroy()
		.then(() => res.sendStatus(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
