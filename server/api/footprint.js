import resource from 'resource-router-middleware';

const filterUserInfo = (db) => ({
	include: [
		{
			association: db.footprint.User,
			attributes: {
				exclude: ['password'],
			},
		},
	]
});

/**
 * Supprime les enregistrements liés à l'emprise
 */
function deleteFootprintRelations(seq, id) {
	const sRequest = `
		DELETE FROM lebontag.lbt_osmdiff_footprint WHERE fp_id = ${id};
	`;
	return seq.query(sRequest);
}

/**
 * Calcule les intersections liées à l'emprise
 */
function calculateFootprintRelations(seq, id) {
	const sRequest = `
    DROP INDEX IF EXISTS lebontag.osmdiff_footprint_odidv_idx;
    DROP INDEX IF EXISTS lebontag.osmdiff_footprint_odid_idx;
    DROP INDEX IF EXISTS lebontag.osmdiff_footprint_fpid_idx;

    WITH footprints AS ( SELECT fp_id,(st_dump(ST_CollectionExtract(ST_MakeValid(fp_geom),3))).geom FROM lebontag.lbt_footprint WHERE fp_id = ${id} ) INSERT INTO lebontag.lbt_osmdiff_footprint (od_id,od_new_version,fp_id) SELECT d.od_id,d.od_new_version,f.fp_id FROM lebontag.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_new_geom, f.geom) WHERE d.od_new_geom IS NOT NULL UNION SELECT d.od_id,d.od_new_version,f.fp_id FROM lebontag.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_old_geom, f.geom) WHERE d.od_old_geom IS NOT NULL;

    CREATE INDEX osmdiff_footprint_odidv_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id,od_new_version);
    CREATE INDEX osmdiff_footprint_odid_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id);
    CREATE INDEX osmdiff_footprint_fpid_idx ON lebontag.lbt_osmdiff_footprint USING btree (fp_id);
	`;
	return seq.query(sRequest);
}

/**
 * Retourne une promesse de l'emprise par défaut
 * @param {*} db 
 * @returns 
 */
function getDefaultFootprint(db) {
	const Setting = db.setting;
	return new Promise((resolve, reject) => {
		Setting.findAll({
			where: {
				s_name: 'osm_footprint'
			}
		}).then(res => {
			const setting = res[0];
			if (setting &&
				setting.dataValues &&
				setting.dataValues.value) {
				resolve(setting.dataValues.value);
			} else {
				resolve(null);
			}
		}).catch((err) => {
			console.error(err);
			reject();
		})
	});
}

/**
 * Transforme un GeoJSON pour être interprété par PostGIS
 * @param {*} geojson 
 * @returns 
 */
function geomFromGeoJSON(geojson) {
	if (geojson &&
		geojson.type === 'FeatureCollection' &&
		geojson.features &&
		geojson.features[0] &&
		geojson.features[0].geometry) {

		// Cas de FeatureCollection 
		// la fonction ST_GeomFromGeoJSON utilisée par sequelize ne prend pas en compte les FeatureCollection
		// alors il faut créer une GeometryCollection
		const geomCollection = {
			type: 'GeometryCollection',
			geometries: []
		};
		for (let i = 0; i < geojson.features.length; i++) {
			if (geojson.features[i].geometry) {
				geomCollection.geometries.push(geojson.features[i].geometry);
			}
		}

		return geomCollection;
	} else {

		// Cas de simple géométrie
		return geojson;
	}
}

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id: 'footprint',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Footprint = db.footprint;

		getDefaultFootprint(db).then((defaultFootprintId) => {
			Footprint.findByPk(id, filterUserInfo(db))
				.then(footprint => {
					if (footprint &&
						footprint.dataValues &&
						footprint.dataValues.id == Number(defaultFootprintId)) {
						footprint.dataValues.default = true;
					} else {
						footprint.dataValues.default = false;
					}
					callback(null, footprint)
				})
				.catch(err => callback(console.error(err)))
		});
	},

	/** GET /:id - Return a given entity */
	read({ footprint }, res) {
		res.json(footprint);
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Footprint = db.footprint;

		getDefaultFootprint(db).then((defaultFootprintId) => {
			Footprint.findAll({
				order: [['name', 'ASC']],
				...filterUserInfo(db),
			})
				.then(footprints => {
					for (let i = 0; i < footprints.length; i++) {
						if (footprints[i].dataValues) {
							if (footprints[i].dataValues.id == Number(defaultFootprintId)) {
								footprints[i].dataValues.default = true;
							} else {
								footprints[i].dataValues.default = false;
							}
						}
					}
					res.json(footprints)
				})
				.catch((err) => res.status(500).json({ error: err }));
		})
			.catch((err) => res.status(500).json({ error: err }));

	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Footprint = db.footprint;

		const footprint = {
			...body,
			users: body.users.map((u) => u.id),
		};

		if (footprint.geojson) {
			footprint.geom = geomFromGeoJSON(footprint.geojson);
		}

		const model = Footprint.build(footprint);
		model.save()
			.then((created) => created.setUsers(footprint.users)
				.then(() => Footprint.findByPk(
					created.id,
					filterUserInfo(db)))
			)
			.then(b => res.json(b))
			.catch(err => res.status(500).json({ error: err }));
	},

	/** PUT /:id - Update a given entity */
	update({ footprint, body }, res) {

		// Ne met pas à jour le JSON s'il n'a pas été modifié
		if (body.geojson) {
			body.geom = geomFromGeoJSON(body.geojson);
		} else {
			delete body.geom;
		}

		db.sequelize
			.transaction((t) => {
				return footprint
					.update(body, { transaction: t })
					.then(() => (footprint.setUsers(body.users.map(u => u.id), { transaction: t })));
			})
			.then(() => (footprint.reload(filterUserInfo(db))))
			.then((updated) => res.json(updated))
			.catch((err) => res.status(500).json({ error: err }));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ footprint }, res) {
		if (footprint.dataValues && footprint.dataValues.default !== true) {
			db.sequelize
				.transaction((t) => {
					return footprint.setUsers([], { transaction: t })
						.then(() => {
							return footprint.destroy({ transaction: t });
						});
				})
				.then(() => res.sendStatus(204))
				.catch((err) => res.status(500).json({ error: err }));
		} else {
			res.status(500).json({ error: 'Cannot delete default printfoot' });
		}
	},

})

/** PUT /:id/clean : delete rows in lbt_osmdiff_footprint */
.put('/:id/clean', function(req,res,next) {
				let seq = db.sequelize;
				return deleteFootprintRelations(seq, req.params.id).then(() => {
						res.json("Clean ok");
						//res.sendStatus(204);
					}).catch((err) => {
							console.error('Delete footprint relations error', err);
							// Rollback transaction
							t.rollback();
							// Retour erreur 500
							res.status(500).json({
								error: err
							})
						});
				})

/** PUT /:id/calculate : calculate new intersections in lbt_osmdiff_footprint */
.put('/:id/calculate', function(req,res,next) {
				let seq = db.sequelize;
				return calculateFootprintRelations(seq, req.params.id).then(() => {
						res.json("Calculate ok");
						//res.sendStatus(204);
					}).catch((err) => {
							console.error('Calculate new intersections error', err);
							// Rollback transaction
							t.rollback();
							// Retour erreur 500
							res.status(500).json({
								error: err
							})
						});
				})
;
