import {
  Router
} from 'express';

import db from './../models';
import env_config from './../config/config';

const {
  exec
} = require('child_process');
const fs = require('fs');
const md5 = require('md5');
const archiver = require('archiver');

// Fonction SQL pour connaître les géométries des objets à extraire
function getGeometries(objectStates, objectStatus, objectTags, startDate, endDate, tagsCondition) {

  if (objectTags.length > 0) {
    return db.sequelize.query(` \
      SELECT DISTINCT subreq.geom_type AS geom_type FROM ( \
      SELECT ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) as geom_type \
      FROM lebontag.lbt_osmdiff osm \
      WHERE osm.od_new_timestamp >= '${formatDate(startDate)}' \
      AND osm.od_new_timestamp <= '${formatDate(endDate)}' \
      AND (osm.od_added_tags || osm.od_deleted_tags || osm.od_modified_tags) ?| ${formatStates(objectTags)} \
      AND ( ${tagsCondition} ) \
      AND osm.od_status ${formatStatus(objectStatus)} \
      AND osm.od_state ${formatStatus(objectStates)} \
      UNION ALL \
      SELECT ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) as geom_type \
      FROM lebontag.lbt_osmdiff_history osmh \
      WHERE osmh.od_new_timestamp >= '${formatDate(startDate)}' \
      AND osmh.od_new_timestamp <= '${formatDate(endDate)}' \
      AND (osmh.od_added_tags || osmh.od_deleted_tags || osmh.od_modified_tags) ?| ${formatStates(objectTags)} \
      AND ( ${tagsCondition} ) \
      AND osmh.od_status ${formatStatus(objectStatus)} \
      AND osmh.od_state ${formatStatus(objectStates)} \
      ) subreq`,
      { type: db.sequelize.QueryTypes.SELECT });
  } else {
    return db.sequelize.query(` \
      SELECT DISTINCT subreq.geom_type AS geom_type FROM ( \
      SELECT ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) as geom_type \
      FROM lebontag.lbt_osmdiff osm \
      WHERE osm.od_new_timestamp >= '${formatDate(startDate)}' \
      AND osm.od_new_timestamp <= '${formatDate(endDate)}' \
      AND ( ${tagsCondition} ) \
      AND osm.od_status ${formatStatus(objectStatus)} \
      AND osm.od_state ${formatStatus(objectStates)} \
      UNION ALL \
      SELECT ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) as geom_type \
      FROM lebontag.lbt_osmdiff_history osmh \
      WHERE osmh.od_new_timestamp >= '${formatDate(startDate)}' \
      AND osmh.od_new_timestamp <= '${formatDate(endDate)}' \
      AND ( ${tagsCondition} ) \
      AND osmh.od_status ${formatStatus(objectStatus)} \
      AND osmh.od_state ${formatStatus(objectStates)} \
      ) subreq`,
      { type: db.sequelize.QueryTypes.SELECT });
  }

}

/**
 * Lance la fonction d'export
 * @param {number} objectGroup 
 * @param {array<string>} objectStates 
 * @param {array<number>} objectStatus 
 * @param {array<string>} objectTags 
 * @param {string} outputFormat 
 * @param {string} fileName
 * @param {number} startDate date au format timestamp
 * @param {number} endDate date au format timestamp
 * @returns {Primise<string>} chemin vers le fichier généré
 */
function exportItems_(db, objectGroup, objectStates, objectStatus, objectTags, outputFormat, fileName, startDate, endDate) {
  return new Promise(function (resolve, reject) {

    if (objectGroup &&
      objectStates &&
      objectStatus &&
      objectTags &&
      outputFormat &&
      fileName &&
      startDate &&
      endDate) {

      //console.info('objectGroup: ',objectGroup);
      //console.info('objectStates: ',objectStates);
      //console.info('objectStatus: ',objectStatus);
      //console.info('objectTags: ',objectTags);
      //console.info('outputFormat: ',outputFormat);
      //console.info('fileName: ',fileName);
      //console.info('startDate: ',startDate);
      //console.info('endDate: ',endDate);

      getObjectGroup_(db, objectGroup).then((val) => {

        const tagsCondition = getTagsQuery_(val.queryObj);
        const tagsCondition2 = getTagsQuery2_(val.queryObj);
        // console.info('tagsCondition: ',tagsCondition);
        // console.info('tagsCondition2: ',tagsCondition2);

        getGeometries(objectStates, objectStatus, objectTags, startDate, endDate, tagsCondition2).then(function (geom) {

          // Traiter les geoms - Si vide : rien à exporter

          // Exécute prepareExportDir pour avoir le dossier temp de destination
          prepareExportDir().then((exportDirPath) => {

            var request1 = '';
            var request2 = '';
            var request3 = '';
            var request4 = '';
            var request5 = '';
            var request6 = '';
            var request7 = '';

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_Point')) > -1) {
              request1 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_Point'
              );
            }

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_Polygon')) > -1) {
              request2 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_Polygon'
              );
            }

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_LineString')) > -1) {
              request3 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_LineString'
              );
            }

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_MultiPoint')) > -1) {
              request4 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_MultiPoint'
              );
            }

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_MultiPolygon')) > -1) {
              request5 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_MultiPolygon'
              );
            }

            if (Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_MultiLineString')) > -1) {
              request6 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_MultiLineString'
              );
            }

            if ((Object.values(geom).indexOf(geom.find(g => g.geom_type === 'ST_GeometryCollection')) > -1) && (outputFormat !== 'shapefile')) {
              request7 = getExportRequest_(
                objectGroup,
                objectStates,
                objectStatus,
                objectTags,
                startDate,
                endDate,
                tagsCondition,
                'ST_GeometryCollection'
              );
            }

            //console.info('request1: ',request1);
            //console.info('request2: ',request2);
            //console.info('request3: ',request3);
            //console.info('request4: ',request4);
            //console.info('request5: ',request5);
            //console.info('request6: ',request6);
            //console.info('request7: ',request7);

			// fs.writeFile("/temp/requests.log", request1 + "\n" + request2 + "\n"  + request3 + "\n"  + request4 + "\n"  + request5 + "\n"  + request6 + "\n" + request7 + "\n", function(err) { if(err) { return console.log(err); } });

            // Exporte avec ogr2ogr
            exportOgr2ogr(request1, exportDirPath, fileName, '_point', outputFormat).then(() => {
              exportOgr2ogr(request2, exportDirPath, fileName, '_polygon', outputFormat).then(() => {
                exportOgr2ogr(request3, exportDirPath, fileName, '_line', outputFormat).then(() => {
                  exportOgr2ogr(request4, exportDirPath, fileName, '_multipoint', outputFormat).then(() => {
                    exportOgr2ogr(request5, exportDirPath, fileName, '_multipolygon', outputFormat).then(() => {
                      exportOgr2ogr(request6, exportDirPath, fileName, '_multiline', outputFormat).then(() => {
                        exportOgr2ogr(request7, exportDirPath, fileName, '_collection', outputFormat).then(() => {
                          // Crée le zip et supprime les autres fichiers
                          archiveExportDir(exportDirPath, fileName).then((exportedFilePath) => {
                            resolve(exportedFilePath)
                          }).catch((err) => {
                            console.error('exportedFilePath');
                            reject(err);
                          });
                        }).catch((err) => {
                          console.error('export_collection');
                          reject(err);
                        });
                      }).catch((err) => {
                        console.error('export_multiline');
                        reject(err);
                      });
                    }).catch((err) => {
                      console.error('export_multipolygon');
                      reject(err);
                    });
                  }).catch((err) => {
                    console.error('export_multipoint');
                    reject(err);
                  });
                }).catch((err) => {
                  console.error('export_line');
                  reject(err);
                });
              }).catch((err) => {
                console.error('export_polygon');
                reject(err);
              });
            }).catch((err) => {
              console.error('export_point');
              reject(err);
            });

          }).catch((err) => {
            console.error('prepareExportDir');
            reject(err);
          });
        }).catch((err) => {
          console.error('getGeometries');
          reject(err);
        });
      }).catch((err) => {
        console.error('getObjectGroup_: Object group not found');
        reject();
      });
    } else {
      console.error('exportItems_ bad arguments');
      reject();
    }
  });
}

/**
 * Retourne le groupe d'objets correspondant
 * @param {any} db 
 * @param {number} ogId 
 */
function getObjectGroup_(db, ogId) {
  return new Promise((resolve, reject) => {
    const ObjectGroup = db.objectGroup;
    return ObjectGroup.findByPk(ogId).then((res) => {
      if (res.dataValues) {
        const objectGroup = res.dataValues;

        // parse query_json
        if (objectGroup.query_json) {
          try {
            objectGroup.queryObj = JSON.parse(objectGroup.query_json);
          } catch (error) { }
        }
        resolve(objectGroup);
      } else {
        reject();
      }
    }).catch(() => {
      reject();
    });
  });
}

/**
 * Retourne la condition SQL pour les tags
 * @param {*} queryObj 
 */
function getTagsQuery_(queryObj) {
  let query = 'true';
  if (queryObj) {
    query = translateQuery_(queryObj);
  }
  return query;
}

// Pour la version sans /
function getTagsQuery2_(queryObj) {
  let query = 'true';
  if (queryObj) {
    query = translateQuery2_(queryObj);
  }
  return query;
}

/**
 * Retourne le premier opérateur présent
 */
function getFirstOperator_(parsedQuery) {
  if (parsedQuery['[op.or]']) {
    return '[op.or]';
  } else if (parsedQuery['[op.and]']) {
    return '[op.and]';
  } else {
    return false;
  }
}

/**
 * Traduit la requête et retourne son équivalent SQL
 */
function translateQuery_(parsedQuery) {

  let query = '';
  const firstOp = getFirstOperator_(parsedQuery);
  const sqlOp = firstOp === '[op.or]' ? 'OR' : firstOp === '[op.and]' ? 'AND' : null;

  if (Array.isArray(parsedQuery[firstOp]) && sqlOp !== null) {
    for (let i = 0; i < parsedQuery[firstOp].length; i++) {
      const node = parsedQuery[firstOp][i];
      const key = Object.keys(node)[0];
      const val = node[key];

      if (i > 0) {
        query += ` ${sqlOp} `;
      }

	// Gère différents opérateurs 
	// =
	// != : op.ne
	// >  : op.gt (non utilisé)
	// >= : op.gte (non utilisé)
	// <  : op.lt (non utilisé)
	// <= : op.lte (non utilisé)
	if (typeof val === 'string') {
		if (val === '*') {
			query += `(od_new_tags || od_old_tags) ? '${key}'`;
		} else {
			query += `(od_new_tags || od_old_tags) @> '\\"${key}\\"=>\\"${val}\\"'`;
		}
	} else if (typeof val === 'object') {
		if ('[op.ne]' in val) {
			const opVal = val['[op.ne]'];
			query += `not (od_new_tags || od_old_tags) @> '\\"${key}\\"=>\\"${opVal}\\"'`;
		} else if ('[op.gt]' in val) {
			const opVal = val['[op.gt]'];
			query += '1 = 1';
		} else if ('[op.gte]' in val) {
			const opVal = val['[op.gte]'];
			query += '1 = 1';
		} else if ('[op.lt]' in val) {
			const opVal = val['[op.lt]'];
			query += '1 = 1';
		} else if ('[op.lte]' in val) {
			const opVal = val['[op.lte]'];
			query += '1 = 1';
		} else {
			query += `(${translateQuery_(node)})`;
		}
	}

    }
  }

  return query;
}

// Sans les / pour la requête RAW des géométries
function translateQuery2_(parsedQuery) {

  let query = '';
  const firstOp = getFirstOperator_(parsedQuery);
  const sqlOp = firstOp === '[op.or]' ? 'OR' : firstOp === '[op.and]' ? 'AND' : null;

  if (Array.isArray(parsedQuery[firstOp]) && sqlOp !== null) {
    for (let i = 0; i < parsedQuery[firstOp].length; i++) {
      const node = parsedQuery[firstOp][i];
      const key = Object.keys(node)[0];
      const val = node[key];

      if (i > 0) {
        query += ` ${sqlOp} `;
      }

      if (typeof val === 'string') {
        if (val === '*') {
          query += `(od_new_tags || od_old_tags) ? '${key}'`;
        } else {
          query += `(od_new_tags || od_old_tags) @> '\"${key}\"=>\"${val}\"'`;
        }
      } else if (typeof val === 'object') {
        query += `(${translateQuery2_(node)})`;
      }
    }
  }

  return query;
}

/**
 * Crée la requête à exécuter pour l'export
 * @param {number} objectGroup
 * @param {array<number>} objectStates
 * @param {array<number>} objectStatus
 * @param {array<string>} objectTags
 * @param {number} startDate date au format timestamp
 * @param {number} endDate date au format timestamp
 * @param {string} geometryType type de géométrie à extraire : 'ST_Linestring', 'ST_Polygon', 'ST_MultiPolygon, etc.
 * @returns {string}
 */
function getExportRequest_(objectGroup, objectStates, objectStatus, objectTags, startDate, endDate, tagsCondition, geometryType) {

  if (objectTags.length > 0) {
    let sRequest = ` \
    SELECT DISTINCT ON (od_id) od_id AS id, \
                      od_name AS name, \
                      od_new_tags AS new_tags, \
                      od_old_tags AS old_tags, \
                      od_new_version AS version, \
                      to_char(od_new_timestamp,'YYYY-MM-DD HH24:MI:SSTZH') AS timestamp, \
                      od_new_changeset AS changeset, \
                      od_new_ct_id AS ct_id, \
                      od_action AS bt_action, \
                      od_element AS bt_type, \
                      od_changed_geom AS bt_m_geom, \
                      array_to_string(akeys(od_added_tags), ',') AS bt_a_tags, \
                      array_to_string(akeys(od_deleted_tags), ',') AS bt_d_tags, \
                      array_to_string(akeys(od_modified_tags), ',') AS bt_m_tags, \
                      COALESCE (od_new_geom,od_old_geom) AS geom \
    FROM \
      (SELECT od_id, \
              od_name, \
              od_new_tags, \
              od_old_tags, \
              od_new_version, \
              od_new_timestamp, \
              od_action, \
              od_element, \
              od_new_geom, \
              od_old_geom, \
              od_new_changeset, \
              od_new_ct_id, \
              od_changed_geom, \
              od_added_tags, \
              od_deleted_tags, \
              od_modified_tags \
      FROM lebontag.lbt_osmdiff osm \
      WHERE osm.od_new_timestamp >= '${formatDate(startDate)}' \
        AND osm.od_new_timestamp <= '${formatDate(endDate)}' \
        AND (osm.od_added_tags || osm.od_deleted_tags || osm.od_modified_tags) ?| ${formatStates(objectTags)} \
        AND ( ${tagsCondition} )\
        AND osm.od_status ${formatStatus(objectStatus)} \
        AND osm.od_state ${formatStatus(objectStates)} \
      UNION ALL SELECT od_id, \
                        od_name, \
                        od_new_tags, \
                        od_old_tags, \
                        od_new_version, \
                        od_new_timestamp, \
                        od_action, \
                        od_element, \
                        od_new_geom, \
                        od_old_geom, \
                        od_new_changeset, \
                        od_new_ct_id, \
                        od_changed_geom, \
                        od_added_tags, \
                        od_deleted_tags, \
                        od_modified_tags \
      FROM lebontag.lbt_osmdiff_history osmh \
      WHERE osmh.od_new_timestamp >= '${formatDate(startDate)}' \
        AND osmh.od_new_timestamp <= '${formatDate(endDate)}' \
        AND (osmh.od_added_tags || osmh.od_deleted_tags || osmh.od_modified_tags) ?| ${formatStates(objectTags)} \
        AND ( ${tagsCondition} )\
        AND osmh.od_status ${formatStatus(objectStatus)} \
        AND osmh.od_state ${formatStatus(objectStates)} ) subreq \
    WHERE ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) = '${geometryType}' \
    ORDER BY od_id, \
            od_new_timestamp DESC`;

    sRequest = sRequest.replace(/ {2,}/g, ' ');
    return sRequest;
  } else {
    let sRequest = ` \
    SELECT DISTINCT ON (od_id) od_id AS id, \
                      od_name AS name, \
                      od_new_tags AS new_tags, \
                      od_old_tags AS old_tags, \
                      od_new_version AS version, \
                      to_char(od_new_timestamp,'YYYY-MM-DD HH24:MI:SSTZH') AS timestamp, \
                      od_new_changeset AS changeset, \
                      od_new_ct_id AS ct_id, \
                      od_action AS bt_action, \
                      od_element AS bt_type, \
                      od_changed_geom AS bt_m_geom, \
                      array_to_string(akeys(od_added_tags), ',') AS bt_a_tags, \
                      array_to_string(akeys(od_deleted_tags), ',') AS bt_d_tags, \
                      array_to_string(akeys(od_modified_tags), ',') AS bt_m_tags, \
                      COALESCE (od_new_geom,od_old_geom) AS geom \
    FROM \
      (SELECT od_id, \
              od_name, \
              od_new_tags, \
              od_old_tags, \
              od_new_version, \
              od_new_timestamp, \
              od_action, \
              od_element, \
              od_new_geom, \
              od_old_geom, \
              od_new_changeset, \
              od_new_ct_id, \
              od_changed_geom, \
              od_added_tags, \
              od_deleted_tags, \
              od_modified_tags \
      FROM lebontag.lbt_osmdiff osm \
      WHERE osm.od_new_timestamp >= '${formatDate(startDate)}' \
        AND osm.od_new_timestamp <= '${formatDate(endDate)}' \
        AND ( ${tagsCondition} )\
        AND osm.od_status ${formatStatus(objectStatus)} \
        AND osm.od_state ${formatStatus(objectStates)} \
      UNION ALL SELECT od_id, \
                        od_name, \
                        od_new_tags, \
                        od_old_tags, \
                        od_new_version, \
                        od_new_timestamp, \
                        od_action, \
                        od_element, \
                        od_new_geom, \
                        od_old_geom, \
                        od_new_changeset, \
                        od_new_ct_id, \
                        od_changed_geom, \
                        od_added_tags, \
                        od_deleted_tags, \
                        od_modified_tags \
      FROM lebontag.lbt_osmdiff_history osmh \
      WHERE osmh.od_new_timestamp >= '${formatDate(startDate)}' \
        AND osmh.od_new_timestamp <= '${formatDate(endDate)}' \
        AND ( ${tagsCondition} )\
        AND osmh.od_status ${formatStatus(objectStatus)} \
        AND osmh.od_state ${formatStatus(objectStates)} ) subreq \
    WHERE ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) = '${geometryType}' \
    ORDER BY od_id, \
            od_new_timestamp DESC`;

    sRequest = sRequest.replace(/ {2,}/g, ' ');
    return sRequest;
  }
}

/**
 * Formate la date pour l'export
 * @param {number} date
 * @returns {string}
 */
function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  let result = [year, month, day].join('-');
  result += ' 00:00:00';

  return result;
}

/**
 * Formate les états pour l'export
 * @param {array<string>} objectStates
 * @returns {string}
 */
function formatStates(objectStates) {

  let result = 'ARRAY[';
  for (let i = 0; i < objectStates.length; i++) {
    const state = objectStates[i];
    if (i > 0) {
      result += ',';
    }
    result += "'" + state + "'";
  }
  result += ']';
  return result;
}

/**
 * Formate les statuts pour l'export
 * @param {array<number>} objectStatus
 * @returns {string}
 */
function formatStatus(objectStatus) {
  let result = 'IN (';
  result += objectStatus.join(', ');
  result += ')';
  return result;
}

/**
 * Prépare le dossier d'export dans le répertoire public
 * @returns {Promise<string>} chemin vers le répertoire
 */
function prepareExportDir() {
  return new Promise(function (resolve, reject) {

    // Chemin vers le dossier cible
    const exportPathArray = ['public', 'ogr2ogr', md5(Date.now()), 'temp'];

    // Crée le dossier cible
    let tmpExportPath = '';
    for (let i = 0; i < exportPathArray.length; i++) {
      tmpExportPath += exportPathArray[i] + '/';
      if (!fs.existsSync(tmpExportPath)) {
        fs.mkdirSync(tmpExportPath);
      }
    }

    fs.stat(exportPathArray.join('/'), function (err, stats) {
      if (stats.isDirectory()) {
        resolve(exportPathArray);
      } else {
        reject();
      }
    });
  });
}

/**
 * Crée le zip et supprime les autres fichiers
 * @param {Array<string>} exportDirPath
 * @param fileName
 * @returns {Promise<string>}
 */
function archiveExportDir(exportDirPath, fileName) {
  return new Promise(function (resolve, reject) {

    const source = exportDirPath.join('/');
    const dest = [
      ...exportDirPath.slice(0, exportDirPath.length - 1),
      //...['export-lebontag']
      ...[fileName]
    ].join('/') + '.zip';

    // Crée un zip du répertoire de sortie
    zipDirectory(source, dest).then((res) => {

      // Supprime le répertoire de sortie
      deleteFolderRecursive(exportDirPath.join('/'));

      setTimeout(() => {
        resolve(dest);
      })
    }).catch((err) => {
      reject(err);
    });
  });
}

/**
 * @param {String} source
 * @param {String} out
 * @returns {Promise}
 */
function zipDirectory(source, out) {
  const archive = archiver('zip', {
    zlib: {
      level: 9
    }
  });
  const stream = fs.createWriteStream(out);

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', err => reject(err))
      .pipe(stream);

    stream.on('close', () => resolve());
    archive.finalize();
  });
}

/**
 * Supprime un dossier
 * @param {string} path 
 */
function deleteFolderRecursive(path) {
  var files = [];
  if (fs.existsSync(path)) {
    files = fs.readdirSync(path);
    files.forEach(function (file, index) {
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

/**
 * Exécute la commande d'export
 * @param {string} sqlRequest
 * @param {Array<string>} exportDirPath dossier de destination
 * @param {string} fileName nom des fichiers
 * @param {string} layerName nom de la couche
 * @param {string} format (format de sortie)
 * @returns {Promise<string>}
 */
function exportOgr2ogr(sqlRequest, exportDirPath, fileName, layerName, format) {

  return new Promise(function (resolve, reject) {

    if (sqlRequest !== '') {

      //console.info('sqlRequest: ',sqlRequest);

      let command = 'ogr2ogr ';
      command += '-overwrite ';
      command += '-s_srs "EPSG:' + env_config.epsgOsm + '" ';
	  
	  // projection 4326 si KML
	  if (format == 'kml') {
        command += '-t_srs "EPSG:4326" ';
        command += '-a_srs "EPSG:4326" ';
      } else {
		command += '-t_srs "EPSG:' + env_config.epsgLocal + '" ';
        command += '-a_srs "EPSG:' + env_config.epsgLocal + '" ';
	  };

      if (format == 'shapefile') {
        command += '-f "ESRI Shapefile" ';
        command += exportDirPath.join('/') + ' ';
      } else if (format == 'gpkg') {
        command += '-f "GPKG" ';
        command += exportDirPath.join('/') + '/' + fileName + '.gpkg ';
      } else if (format == 'kml') {
        command += '-f "KML" ';
        command += exportDirPath.join('/') + '/' + fileName + '.kml ';
      };

      command += 'PG:"host=' + env_config.host + ' port=' + env_config.port + ' dbname=' + env_config.database + ' user=' + env_config.username + ' password=' + env_config.password + '" ';
      command += '-sql "' + sqlRequest + '" ';
      command += '-skipfailures ';
      if (format == 'shapefile' || format == 'gpkg') {
        command += '-lco SPATIAL_INDEX=YES ';
      };
      if (format == 'shapefile') {
        command += '-lco ENCODING=UTF-8 ';
      };
      if (format == 'kml') {
        command += '-mapFieldType Integer64=Integer ';
      };
      command += '-nln "' + fileName + layerName + '"';

      // Remplace les espaces en excès
      command = command.replace(/ {2,}/g, ' ');

      //console.info('command: ', command);
      //Exemple de commande générée
      //ogr2ogr -overwrite -s_srs "EPSG:4326" -t_srs "EPSG:2154" -a_srs "EPSG:2154" -f "ESRI Shapefile" public/ogr2ogr/8d5bcc84b6620e88f8cb11840aab8dcb/shape PG:"host=localhost port=5432 dbname=osm user=osm password=osm" -sql " SELECT DISTINCT ON (od_id) od_id AS id, od_name AS name, od_new_tags AS tags, od_new_version AS version, od_new_timestamp AS timestamp, od_new_changeset AS changeset, od_new_ct_id AS ct_id, od_action AS bt_action, od_element AS bt_type, od_changed_geom AS bt_m_geom, array_to_string(akeys(od_added_tags), ',') AS bt_a_tags, array_to_string(akeys(od_deleted_tags), ',') AS bt_d_tags, array_to_string(akeys(od_modified_tags), ',') AS bt_m_tags, COALESCE (od_new_geom,od_old_geom) AS geom FROM (SELECT od_id, od_name, od_new_tags, od_new_version, od_new_timestamp, od_action, od_element, od_new_geom, od_old_geom, od_new_changeset, od_new_ct_id, od_changed_geom, od_added_tags, od_deleted_tags, od_modified_tags FROM lebontag.lbt_osmdiff osm WHERE osm.od_new_timestamp >= '2019-11-01 00:00:00' AND osm.od_new_timestamp <= '2020-02-15 00:00:00' AND (osm.od_added_tags || osm.od_deleted_tags || osm.od_modified_tags) ?| ARRAY['name'] AND( (od_new_tags || od_old_tags) @> '\"amenity\"=>\"bicycle_parking\"' ) AND osm.od_status IN (1, 4) AND osm.od_state IN (1, 4, 2, 3) UNION ALL SELECT od_id, od_name, od_new_tags, od_new_version, od_new_timestamp, od_action, od_element, od_new_geom, od_old_geom, od_new_changeset, od_new_ct_id, od_changed_geom, od_added_tags, od_deleted_tags, od_modified_tags FROM lebontag.lbt_osmdiff_history osmh WHERE osmh.od_new_timestamp >= '2019-11-01 00:00:00' AND osmh.od_new_timestamp <= '2020-02-15 00:00:00' AND (osmh.od_added_tags || osmh.od_deleted_tags || osmh.od_modified_tags) ?| ARRAY['name'] AND( (od_new_tags || od_old_tags) @> '\"amenity\"=>\"bicycle_parking\"' ) AND osmh.od_status IN (1, 4) AND osmh.od_state IN (1, 4, 2, 3) ) subreq WHERE ST_GeometryType(COALESCE (od_new_geom,od_old_geom)) = 'ST_MultiLineString' ORDER BY od_id, od_new_timestamp DESC" -skipfailures -lco ENCODING=UTF-8 -lco SPATIAL_INDEX=YES -nln "export_multiline"

      exec(command, (err, stout, sterr) => {
        if (err) {
          console.error('Error with ogr2ogr command');
          console.error(err);
          resolve();
        } else {
          if (stout) {
            resolve();
          } else {
            resolve();
          }
        }
      })

    } else { resolve(); }

  });
}

export default ({
  config,
  db
}) => {

  // Router
  const map = Router();

  // Op
  const Op = db.Sequelize.Op;

  // Modèle
  const LbtExport = db.lbtExport;

  /**
   * Liste des exports
   */
  map.get('/saved', (req, res, next) => {

    // User ID
    const userId = req.authenticated_user.id;

    // Déclenche la requête
    LbtExport.findAll({
      where: {
        [Op.or]: {
          shared: true,
          u_id: userId
        }
      }
    })
      .then(lbtExports => res.json(lbtExports))
      .catch((err) => res.status(500).json({
        error: err
      }));
  });

  /**
   * Télécharge l'export généré
   */
  map.get('/download/:id/:name', (req, res, next) => {

    const path = `public/ogr2ogr/${req.params.id}/${req.params.name}`;

    if (fs.existsSync(path)) {
      res.status(200);
      res.setHeader('content-type', 'application/zip');
      fs.createReadStream(path).pipe(res);
    } else {
      res.status(404);
      return res.json({
        errorMessage: 'File not found'
      });
    }
  });

  /**
   * Upsert de l'export
   */
  map.post('/saved', (req, res, next) => {

    // User ID
    const userId = req.authenticated_user.id;

    // set u_id
    req.body.u_id = userId;

    LbtExport.findOne({
      where: {
        [Op.and]: {
          name: req.body.name,
          [Op.or]: {
            shared: true,
            u_id: userId
          }
        }
      }
    }).then((obj) => {
      if (obj) {
        // Déclenche la mise à jour
        obj.update(req.body)
          .then((obj) => res.json(obj))
          .catch((err) => res.status(500).json({
            error: err
          }));
      } else {
        // Déclenche la création
        LbtExport.create(req.body)
          .then((lbtExport) => res.json(lbtExport))
          .catch((err) => res.status(500).json({
            error: err
          }));
      }
    }).catch((err) => {
      res.status(404);
      return res.json({});
    });
  });

  /**
   * Suppression de l'export
   */
  map.delete('/saved/:id', (req, res, next) => {

    // User ID
    const userId = req.authenticated_user.id;

    LbtExport.findOne({
      where: {
        [Op.and]: {
          id: req.params.id,
          [Op.or]: {
            shared: true,
            u_id: userId
          }
        }
      }
    }).then((obj) => {
      if (obj) {
        // Déclenche la suppression
        obj.destroy()
          .then((obj) => res.json(obj))
          .catch((err) => res.status(500).json({
            error: err
          }));
      } else {
        res.status(404);
        return res.json({});
      }
    }).catch((err) => {
      res.status(404);
      return res.json({});
    });
  });

  /**
   * Génération de l'export
   */
  map.post('/generate', (req, res, next) => {

    // Déclenche l'export : lit le contenu du JSON envoyé par POST
    exportItems_(
      db,
      req.body.objectGroup,
      req.body.objectStates,
      req.body.objectStatus,
      req.body.objectTags,
      req.body.outputFormat,
      req.body.fileName,
      req.body.startDate,
      req.body.endDate
    ).then((exportedFilePath) => {
      res.status(200);
      return res.json({
        'exported_file': exportedFilePath
      });
    }).catch((err) => {
      res.status(404);
      return res.json({});
    });
  });

  return map;
}