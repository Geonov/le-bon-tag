import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'vigilance',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Vigilance = db.vigilance;
	
		Vigilance.findByPk(id)
		.then(vigilance => callback(null, vigilance))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Vigilance = db.vigilance;
		Vigilance.findAll()
		.then(vigilances => res.json(vigilances))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create(_, res) {
		res.status(403);
	},

	/** GET /:id - Return a given entity */
	read({ vigilance }, res) {
		res.json(vigilance);
	},

	/** PUT /:id - Update a given entity */
	update({ vigilance, body }, res) {
		res.status(403);
	},

	/** DELETE /:id - Delete a given entity */
	delete(_, res) {
		res.status(403);
	}
});
