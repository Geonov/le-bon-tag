import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'theme',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Theme = db.theme;
	
		Theme.findByPk(
			id,
			{ include: [ Theme.ObjectGroup ] }
		)
		.then(theme => callback(null, theme))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Theme = db.theme
		Theme.findAll({
			order: [
				['name', 'ASC'],
			],
			include: [ Theme.ObjectGroup ]
		})
		.then(themes => res.json(themes))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Theme = db.theme;

		const theme = {
			...body,
			objects: body.objects.map((o) => o.id),
		};

		const model = Theme.build(theme);
		model.save()
		.then((created) => created.setObjects(theme.objects)
			.then(() => Theme.findByPk(
				created.id,
				{ include: [ Theme.ObjectGroup ] }
			))
		)
		.then(t => res.json(t))
		.catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ theme }, res) {
		res.json(theme);
	},

	/** PUT /:id - Update a given entity */
	update({ theme, body }, res) {
		const Theme = db.theme;
		
		db.sequelize
		.transaction((t) => {
			return theme
				.update(body, {transaction: t})
				.then(() => (theme.setObjects(body.objects.map(o => o.id), {transaction: t})));
		})
		.then(() => (theme.reload({ include: [ Theme.ObjectGroup ] })))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ theme }, res) {
		db.sequelize
			.transaction((t) => {
				return theme.setObjects([], {transaction: t})
			  		.then(() => {
						return theme.destroy({transaction: t});
					});
		  	})
		  	.then(() => res.sendStatus(204))
			.catch((err) => res.status(500).json({error: err}));
	}
});
