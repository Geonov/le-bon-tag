import resource from 'resource-router-middleware';
import bcrypt from 'bcryptjs';

const attributeFilter = { exclude: ['password'] };
const saltRounds = 10;

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'user',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const User = db.user;
	
		User.findByPk(
			id,
			{
				include: [
					User.BusinessGroup,
					User.Type,
					User.Role,
					User.Footprint,
				],
				attributes: attributeFilter,
			}
		)
		.then(user => callback(null, user))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const User = db.user
		User.findAll(
			{
				order: [
					['last_name', 'ASC'],
					['first_name', 'ASC'],
				],
				include: [
					User.BusinessGroup,
					User.Type,
					User.Role,
					User.Footprint,
				],
				attributes: attributeFilter,
			}
		)
		.then(users => res.json(users))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const User = db.user

		body.u_ur_id = body.role.id;
		body.u_ut_id = body.type.id;

		bcrypt
		.hash(body.password, saltRounds)
		.then(function(hash) {
			body.password = hash;
			return User
				.create(body);
		})
		.then((user) =>
			user
			.setGroups(body.groups.map((g) => g.id))
			.then(() => 
				(user.setFootprints(body.footprints.map((f) => f.id))
				.then(() => 
				User.findByPk(user.id, {
					include: [
						User.BusinessGroup,
						User.Type,
						User.Role,
						User.Footprint,
					],
					attributes: attributeFilter,
				})))))
		.then((user) => res.json(user))
		.catch(err => console.log(err));
	},

	/** GET /:id - Return a given entity */
	read({ user }, res) {
		res.json(user);
	},

	/** PUT /:id - Update a given entity */
	update({ user, body }, res) {
		const User = db.user;
		
		if (body.password) {
			const salt = bcrypt.genSaltSync(saltRounds);
			const hash = bcrypt.hashSync(body.password, salt);
			body.password = hash;
		}

		db.sequelize
		.transaction((t) => {
			return user
				.update(body, {transaction: t})
				.then(() => (user.setGroups(body.groups.map(g => g.id), {transaction: t})))
				.then(() => (user.setFootprints(body.footprints.map(g => g.id), {transaction: t})))
				.then(() => (user.setType(body.type.id), {transaction: t}))
				.then(() => (user.setRole(body.role.id), {transaction: t}));
		})
		.then(() => (User.findByPk(user.id, { attributes: attributeFilter })))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ user }, res) {
		db.sequelize
			.transaction((t) => {
				return user.setGroups([], {transaction: t})
			  		.then(() => {
						return user.destroy({transaction: t});
					});
		  	})
		  	.then(() => res.sendStatus(204))
			.catch((err) => res.status(500).json({error: err}));
	}
});
