import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'tag',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Tag = db.tag;
		Tag.findByPk(id)
			.then(tag => callback(null, tag))
        	.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Tag = db.tag

		const filter = { where: { deleted: false } };
		Tag.findAll({
			...filter,
			order: [
				['name', 'ASC'],
			],
		})
		.then(tags => res.json(tags))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Tag = db.tag;

		Tag.findOne({ where: { name: body.name } })
		.then((tag) => {
			if (tag) {
				if (!tag.deleted) {
					return new Promise.reject("Le tag existe déjà et n'est pas supprimé");
				}
				return tag.update({
					name_fr: body.name_fr,
					deleted: false,
				})
			}
			return Tag.create(body);
		})
		.then((tag) => res.json(tag))
		.catch((err) => res.status(403).json({error: JSON.stringify(err)}));
	},

	/** GET /:id - Return a given entity */
	read({ tag }, res) {
		res.json(tag);
	},

	/** PUT /:id - Update a given entity */
	update({ tag, body }, res) {
		tag.update(body)
		.then(() => (tag.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ tag }, res) {
		tag.update({ deleted: true })
		.then(() => (tag.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	}
});
