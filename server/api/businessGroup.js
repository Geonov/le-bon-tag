import resource from 'resource-router-middleware';

const filterUserInfo = (db) => ({
	include: [
		{
			association: db.businessGroup.User,
			attributes: {
				exclude: ['password'],
			},
		},
	]
});

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'businessGroup',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const BusinessGroup = db.businessGroup;
	
		BusinessGroup.findByPk(id, filterUserInfo(db))
		.then(business => callback(null, business))
		.catch(err => callback(console.log(err)))
	},
	
	/** GET / - List all entities */
	list({ params }, res) {
		const BusinessGroup = db.businessGroup;
		BusinessGroup.findAll({
			order: [ ['name', 'ASC'] ],
			...filterUserInfo(db),
		})
		.then(business => res.json(business))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const BusinessGroup = db.businessGroup;

		const business = {
			...body,
			users: body.users.map((u) => u.id),
		};

		const model = BusinessGroup.build(business);
		model.save()
		.then((created) => created.setUsers(business.users)
			.then(() => BusinessGroup.findByPk(
				created.id,
				filterUserInfo(db)))
		)
		.then(b => res.json(b))
		.catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ businessGroup }, res) {
		res.json(businessGroup);
	},

	/** PUT /:id - Update a given entity */
	update({ businessGroup, body }, res) {
		db.sequelize
			.transaction((t) => {
				return businessGroup
					.update(body, {transaction: t})
					.then(() => (businessGroup.setUsers(body.users.map(u => u.id), {transaction: t})));
			})
			.then(() => (businessGroup.reload(filterUserInfo(db))))
		  	.then((updated) => res.json(updated))
			.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ businessGroup }, res) {
		db.sequelize
			.transaction((t) => {
				return businessGroup.setUsers([], {transaction: t})
			  		.then(() => {
						return businessGroup.destroy({transaction: t});
					});
		  	})
		  	.then(() => res.sendStatus(204))
			.catch((err) => res.status(500).json({error: err}));
	}
});
