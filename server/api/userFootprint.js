import {
	Router
} from 'express';

import env_config from '../config/config';
import jwt from 'jwt-simple';

/**
 * Retourne une promesse de l'emprise par défaut
 * @param {*} db 
 * @returns 
 */
function getDefaultFootprint(db) {
	const Setting = db.setting;
	return new Promise((resolve, reject) => {
		Setting.findAll({
			where: {
				s_name: 'osm_footprint'
			}
		}).then(res => {
			const setting = res[0];
			if (setting &&
				setting.dataValues &&
				setting.dataValues.value) {
				resolve(setting.dataValues.value);
			} else {
				resolve(null);
			}
		}).catch((err) => {
			console.error(err);
			reject(err);
		})
	});
}

export default ({
	config,
	db
}) => {

	// Router
	const map = Router();

	// Modèles
	const Footprint = db.footprint;
	const UserFootprint = db.userFootprint;

	/**
	 * Liste des emprises de l'utilisateur
	 */
	map.get('/', (req, res, next) => {

		const authorization = req.headers.authorization;
		if (!authorization) {
			return res.status(401).json({ error: 'No credentials sent!' });
		}
		const token = authorization.split(/Bearer\s?/)[1];
		const decoded = jwt.decode(token, env_config.jwt_secret);
		if (!decoded) {
			res.status(401);
		}

		const user_id = decoded.id;
		const filterIds = []

		// Emprises associées à l'utilisateur
		UserFootprint.findAll({
			where: {
				id: user_id
			}
		})
			.then(userFootprints => {
				for (let i = 0; i < userFootprints.length; i++) {
					if (userFootprints[i].dataValues &&
						userFootprints[i].dataValues.fp_id) {
						filterIds.push(userFootprints[i].dataValues.fp_id);
					}
				}

				// Emprise par défaut
				getDefaultFootprint(db).then((defaultFootprintId) => {
					if (filterIds.length === 0) {
						filterIds.push(defaultFootprintId);
					}

					// Définition des emprises
					Footprint.findAll({
						order: [['name', 'ASC']],
						where: {
							id: filterIds
						}
					})
						.then(footprints => {
							res.json(footprints);
						})
						.catch((err) => res.status(500).json({ error: err }));
				}).catch((err) => res.status(500).json({ error: err }));
			}).catch((err) => res.status(500).json({ error: err }));
	});

	return map;
}