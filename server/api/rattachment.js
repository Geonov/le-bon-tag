import {
	Router
} from 'express';

import env_config from '../config/config';
import Sequelize from 'sequelize';

/**
 * Se connecte à la base externe et retourne un objet de type sequelize
 * @param {*} rattachment 
 * @param {*} db 
 */
function getSeqConnection(rattachment, db) {
	let seq = db.sequelize;

	const extConfig = {
		...env_config
	}

	extConfig.host = rattachment.host;
	extConfig.port = rattachment.port;
	extConfig.database = rattachment.db;
	extConfig.username = rattachment.user;
	extConfig.password = rattachment.pass;

	seq = new Sequelize(extConfig.database, extConfig.username, extConfig.password, extConfig);

	return seq;
}

function countRattachment(seq, rattachment, transaction) {
	const sRequest = `SELECT count(*) as totalRattachment FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NOT NULL;`;
	return seq.query(sRequest, { transaction });
}

function countTotal(seq, rattachment, transaction) {
	const sRequest = `SELECT count(*) as total FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp};`;
	return seq.query(sRequest, { transaction });
}

/**
 * Crée une copie de la table attributaire métier avec récupération de l'ancienne géométrie métier
 *
 *	CREATE TABLE travail.rattachment_attr_160157732353395 AS
 *	SELECT a.*,
 *	b.geom AS geom_old_rattachment
 *	FROM metier.voirie_attr a
 *	INNER JOIN metier.voirie_geom b ON a.gid = b.gid;
 *	ALTER TABLE travail.rattachment_attr_160157732353395 RENAME COLUMN gid TO gid_old_rattachment;
 *	ALTER TABLE travail.rattachment_attr_160157732353395 ADD COLUMN gid int8;
 *	CREATE INDEX rattachment_attr_160157732353395_idold_idx ON travail.rattachment_attr_160157732353395 USING btree(gid_old_rattachment);
 *	CREATE INDEX rattachment_attr_160157732353395_geomold_idx ON travail.rattachment_attr_160157732353395 USING gist(geom_old_rattachment);
 */
function createTableMetier(seq, rattachment, transaction) {

	const sRequest = `
		CREATE TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} AS
		SELECT a.*,
			b.${rattachment.fgeom} AS ${rattachment.fgeom}_old_rattachment
		FROM ${rattachment.tableattr} a
		INNER JOIN ${rattachment.tablegeom} b ON a.${rattachment.fid} = b.${rattachment.fid};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} RENAME COLUMN ${rattachment.fid} TO ${rattachment.fid}_old_rattachment;
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} ADD COLUMN ${rattachment.fid} int8;
		CREATE INDEX ${rattachment.tableattr_temp}_idold_idx ON ${rattachment.schema_temp}.${rattachment.tableattr_temp} USING btree(${rattachment.fid}_old_rattachment);
		CREATE INDEX ${rattachment.tableattr_temp}_geomold_idx ON ${rattachment.schema_temp}.${rattachment.tableattr_temp} USING gist(${rattachment.fgeom}_old_rattachment);
	`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Crée une copie de la table du filaire LeBonTag
 * 
 *	CREATE TABLE travail.rattachment_geom_160157732353395 AS
 *	SELECT * 
 *		FROM (SELECT * ,
 *			ROW_NUMBER() over(PARTITION BY geom ORDER BY area DESC) AS row_num FROM osm.filairefromlbt) sub
 *	WHERE sub.row_num = 1;
 *	ALTER TABLE travail.rattachment_geom_160157732353395 DROP row_num;
 *	CREATE INDEX rattachment_geom_160157732353395_id_idx ON travail.rattachment_geom_160157732353395 USING btree(gid);
 *	CREATE INDEX rattachment_geom_160157732353395_geom_idx ON travail.rattachment_geom_160157732353395 USING gist(geom);
 */
function createTableFilaire(seq, rattachment, transaction) {

	const sRequest = `
		CREATE TABLE ${rattachment.schema_temp}.${rattachment.tablegeom_temp} AS
		SELECT * 
			FROM (SELECT * ,
				ROW_NUMBER() over(PARTITION BY ${rattachment.fgeom} ORDER BY area DESC) AS row_num FROM ${rattachment.tablefil}) sub
		WHERE sub.row_num = 1;
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tablegeom_temp} DROP row_num;
		CREATE INDEX ${rattachment.tablegeom_temp}_id_idx ON ${rattachment.schema_temp}.${rattachment.tablegeom_temp} USING btree(${rattachment.fid});
		CREATE INDEX ${rattachment.tablegeom_temp}_geom_idx ON ${rattachment.schema_temp}.${rattachment.tablegeom_temp} USING gist(${rattachment.fgeom});
	`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Lance un rattachement automatique utilisant les géométries proches dans la tolérance
 * ATTENTION, CETTE REQUETE PEUT ETRE LONGUE
 * 
 */
function runAutoRattachment(seq, rattachment, transaction) {

	const sRequest = `
CREATE temporary TABLE rattachment_auto_met as
(SELECT met.${rattachment.fid}_old_rattachment AS metgid, met.${rattachment.fgeom}_old_rattachment as metgeom, st_buffer(met.${rattachment.fgeom}_old_rattachment, ${rattachment.tolerance}) as metgeombuffer,
ST_StartPoint(ST_SnapToGrid(ST_LineMerge(met.${rattachment.fgeom}_old_rattachment), ${rattachment.tolerance})) as metgeomstartpt
FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} AS met);
CREATE INDEX rattachment_auto_met_metgid_idx ON rattachment_auto_met USING btree (metgid);
CREATE INDEX rattachment_auto_met_metgeom_idx ON rattachment_auto_met USING gist (metgeom);
CREATE INDEX rattachment_auto_met_metgeombuffer_idx ON rattachment_auto_met USING gist (metgeombuffer);
CREATE INDEX rattachment_auto_met_metgeomstartpt_idx ON rattachment_auto_met USING gist (metgeomstartpt);
CREATE temporary TABLE rattachment_auto_ref as
(SELECT ref.${rattachment.fid} AS refgid, ref.${rattachment.fgeom} as refgeom, st_buffer(ref.${rattachment.fgeom}, ${rattachment.tolerance}) as refgeombuffer,
ST_StartPoint(ST_SnapToGrid(ST_LineMerge(ref.${rattachment.fgeom}), ${rattachment.tolerance})) as refgeomstartpt
FROM ${rattachment.schema_temp}.${rattachment.tablegeom_temp} AS ref);
CREATE INDEX rattachment_auto_ref_refgid_idx ON rattachment_auto_ref USING btree (refgid);
CREATE INDEX rattachment_auto_ref_refgeom_idx ON rattachment_auto_ref USING gist (refgeom);
CREATE INDEX rattachment_auto_ref_refgeombuffer_idx ON rattachment_auto_ref USING gist (refgeombuffer);
CREATE INDEX rattachment_auto_ref_refgeomstartpt_idx ON rattachment_auto_ref USING gist (refgeomstartpt);
CREATE temporary TABLE rattachment_auto AS
		(SELECT met.metgid, ref.refgid
			FROM rattachment_auto_met AS met
			INNER JOIN rattachment_auto_ref AS ref ON st_within(met.metgeom, ref.refgeombuffer)
			AND st_within(ref.refgeom, met.metgeombuffer)
			AND ST_Equals(met.metgeomstartpt, ref.refgeomstartpt));
DELETE FROM rattachment_auto WHERE refgid IN ( SELECT refgid FROM rattachment_auto GROUP BY refgid HAVING count(*) > 1 and refgid is not null );
UPDATE ${rattachment.schema_temp}.${rattachment.tableattr_temp} as a SET ${rattachment.fid} = j.refgid FROM rattachment_auto as j WHERE a.${rattachment.fid}_old_rattachment = j.metgid AND ${rattachment.fid} IS NULL;
`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Lance le rattachement inverse
 *
 * Fait une sauvegarde de la table géométrique des données métier puis prépare la session de rattachement inverse.
 * Les filaires de la table du filaire à jour (osm.filairefromlbt)
 * qui ne sont pas présents dans la table géométrique des données métier (clé « gid » metier_2.voirie_geom) sont
 * insérés dans la table géométrique des données métier (les attributs sont identiques).
 * La clause WHERE optionnelle élimine certains filaires non désirés (ils ne sont pas à traiter et
 * ne sont donc pas insérés).
 * Ancienne requête trop lente :
 * let whereClause = `WHERE gid NOT IN (select gid FROM ${rattachment.tablegeom})`;
 * INSERT INTO ${rattachment.tablegeom} (${rattachment.tablefil_attr}) (SELECT ${rattachment.tablefil_attr} FROM ${rattachment.tablefil} ${whereClause});
 * @param {*} seq 
 * @param {*} rattachment 
 */
function runInverseRattachment(seq, rattachment, transaction, tableDate) {

	let whereClause = `WHERE ${rattachment.tablegeom}.${rattachment.fid} is null`;
	if (rattachment.tablefil_where && rattachment.tablefil_where.length > 0) {
		whereClause += ` AND ${rattachment.tablefil_where}`;
	}

	let attr = `${rattachment.tablefil_attr}`;
	attr = "l." + attr.replace(/ /g, "").replace(/,/g, ",l.");

	const sRequest = `
	CREATE TABLE IF NOT EXISTS ${rattachment.tablegeom}_rattinv_` + tableDate + ` as TABLE ${rattachment.tablegeom};
	INSERT INTO ${rattachment.tablegeom} (${rattachment.tablefil_attr}) (SELECT ` + attr + ` FROM ${rattachment.tablefil} l left join ${rattachment.tablegeom} on l.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} ${whereClause});
	`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Effectue un VACUUM sur les tables temporaires (avant rattachement automatique pour optimisation)
 *
 *	VACUUM FULL travail.rattachment_attr_160157732353395;
 *	VACUUM FULL travail.rattachment_geom_160157732353395;
 */
function vacuumTempTable(seq, schema, table) {

	const sRequest = `
		VACUUM FULL ${schema}.${table};
	`;

	return seq.query(sRequest);
}

/**
 * Supprime les tables associées à un rattachement
 * 
 *	DROP TABLE IF EXISTS travail.rattachment_attr_160157732353395;
 *	DROP TABLE IF EXISTS travail.rattachment_geom_160157732353395;
 */
function dropRattachmentTables(seq, rattachment, transaction) {

	const sRequest = `
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tableattr_temp};
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tablegeom_temp};
	`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Clôture du rattachement classique - Sauvegarde, vide et remplit les tables d'origine (attention mêmes champs obligatoires)
 * 
 * CREATE TABLE IF NOT EXISTS metier.voirie_attr_20201010 as TABLE metier.voirie_attr;
 * CREATE TABLE IF NOT EXISTS metier.voirie_geom_20201010 as TABLE metier.voirie_geom;
 * ALTER TABLE travail.rattachment_attr_160224367416777 DROP geom_old_rattachment;
 * UPDATE travail.rattachment_attr_160224367416777 SET gid_old_rattachment = gid;
 * ALTER TABLE travail.rattachment_attr_160224367416777 DROP gid;
 * ALTER TABLE travail.rattachment_attr_160224367416777 RENAME COLUMN gid_old_rattachment TO gid;
 * TRUNCATE metier.voirie_attr;
 * INSERT INTO metier.voirie_attr SELECT * FROM travail.rattachment_attr_160224367416777 WHERE gid IS NOT NULL;
 * DROP TABLE IF EXISTS travail.rattachment_attr_160224367416777;
 * TRUNCATE metier.voirie_geom;
 * INSERT INTO metier.voirie_geom SELECT * FROM travail.rattachment_geom_160224367416777;
 * DROP TABLE IF EXISTS travail.rattachment_geom_160224367416777;
 */
function closeRattachmentTables(seq, rattachment, transaction, tableDate) {
	
	/**
	* Si le rattachement classique n'est pas suivi d'un rattachement inverse, tous les filaires sont insérés dans la table géométrique.
	* Sinon, seuls les filaires rattachés sont insérés dans la table géométrique.
	*/
	if (rattachment.isfollowedbyreverse !== true) {
		
		const sRequest = `
		CREATE TABLE IF NOT EXISTS ${rattachment.tableattr}_` + tableDate + ` as TABLE ${rattachment.tableattr};
		CREATE TABLE IF NOT EXISTS ${rattachment.tablegeom}_` + tableDate + ` as TABLE ${rattachment.tablegeom};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} DROP ${rattachment.fgeom}_old_rattachment;
		UPDATE ${rattachment.schema_temp}.${rattachment.tableattr_temp} SET ${rattachment.fid}_old_rattachment = ${rattachment.fid};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} DROP ${rattachment.fid};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} RENAME COLUMN ${rattachment.fid}_old_rattachment TO ${rattachment.fid};
		TRUNCATE ${rattachment.tableattr};
		INSERT INTO ${rattachment.tableattr} SELECT * FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NOT NULL;
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tableattr_temp};
		TRUNCATE ${rattachment.tablegeom};
		INSERT INTO ${rattachment.tablegeom} (${rattachment.tablefil_attr}) (SELECT ${rattachment.tablefil_attr} FROM ${rattachment.schema_temp}.${rattachment.tablegeom_temp});
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tablegeom_temp};
		`;
		
			return seq.query(sRequest, {
		transaction
	});
		
	} else {
		
		let attr = `${rattachment.tablefil_attr}`;
		attr = "l." + attr.replace(/ /g, "").replace(/,/g, ",l.");

		const sRequest = `
		CREATE TABLE IF NOT EXISTS ${rattachment.tableattr}_` + tableDate + ` as TABLE ${rattachment.tableattr};
		CREATE TABLE IF NOT EXISTS ${rattachment.tablegeom}_` + tableDate + ` as TABLE ${rattachment.tablegeom};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} DROP ${rattachment.fgeom}_old_rattachment;
		UPDATE ${rattachment.schema_temp}.${rattachment.tableattr_temp} SET ${rattachment.fid}_old_rattachment = ${rattachment.fid};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} DROP ${rattachment.fid};
		ALTER TABLE ${rattachment.schema_temp}.${rattachment.tableattr_temp} RENAME COLUMN ${rattachment.fid}_old_rattachment TO ${rattachment.fid};
		TRUNCATE ${rattachment.tableattr};
		INSERT INTO ${rattachment.tableattr} SELECT * FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NOT NULL;
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tableattr_temp};
		TRUNCATE ${rattachment.tablegeom};
		INSERT INTO ${rattachment.tablegeom} (${rattachment.tablefil_attr}) (SELECT ` + attr + ` FROM ${rattachment.schema_temp}.${rattachment.tablegeom_temp} l inner join ${rattachment.tableattr} t on l.${rattachment.fid} = t.${rattachment.fid} );
		DROP TABLE IF EXISTS ${rattachment.schema_temp}.${rattachment.tablegeom_temp};
		`;
		
			return seq.query(sRequest, {
		transaction
	});

	}

}

/**
 * Clôture du rattachement inverse
 * Les géométries de la table de référence non présentes sont insérées dans la table géométrique
 */
function closeReverseRattachmentTables(seq, rattachment, transaction) {
	
	let attr = `${rattachment.tablefil_attr}`;
	attr = "l." + attr.replace(/ /g, "").replace(/,/g, ",l.");
	
	const sRequest = `
	INSERT INTO ${rattachment.tablegeom} (${rattachment.tablefil_attr}) (SELECT ` + attr + ` FROM ${rattachment.tablefil} l left join ${rattachment.tablegeom} t on l.${rattachment.fid} = t.${rattachment.fid} WHERE t.${rattachment.fid} IS NULL);
	`;

	return seq.query(sRequest, {
		transaction
	});
}

/**
 * Récupère les attributs des objets non rattachés
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid_old_rattachment, gestionnaire,exploitant FROM travail.rattachment_attr_160157732353395 WHERE gid IS NULL;
 */
function getNonRattachedObjects(seq, rattachment, xmin, xmax, ymin, ymax) {
	const sRequest = `SELECT ${rattachment.fid}_old_rattachment, ${rattachment.flist} FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NULL
	AND ST_Intersects(the_geom_old_rattachment,ST_Transform('SRID=4326;POLYGON((${xmin} ${ymin}, ${xmax} ${ymin}, ${xmax} ${ymax}, ${xmin} ${ymax}, ${xmin} ${ymin}))'::geometry,${rattachment.epsg}));`;
	return seq.query(sRequest);
}

/**
 * Récupère les attributs d'un objet non rattaché
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid_old_rattachment, gestionnaire,exploitant FROM travail.rattachment_attr_160157732353395 WHERE gid IS NULL;
 */
function getObjectById(seq, rattachment, object_id) {
	const sRequest = `
	SELECT ${rattachment.fid}_old_rattachment, ${rattachment.flist} 
	FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} 
	WHERE ${rattachment.fid}_old_rattachment = :object_id`;
	return seq.query(sRequest, {
		replacements: {
			object_id: String(object_id)
		}
	});
}

/**
 * Récupère les objets qui ont été insérés dans la table géométrique des données métier, 
 * mais qui n'ont pas de correspondance dans la table attributaire des données métier.
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid FROM metier.voirie_geom where gid not in (SELECT gid FROM metier.voirie_attr)
 */
function getNonAttrObject(seq, rattachment, xmin, xmax, ymin, ymax) {
	const sRequest = `SELECT ${rattachment.fid} FROM ${rattachment.tablegeom} WHERE ${rattachment.fid} NOT IN (SELECT ${rattachment.fid} FROM ${rattachment.tableattr})
	AND ST_Intersects(the_geom_old_rattachment,ST_Transform('SRID=4326;POLYGON((${xmin} ${ymin}, ${xmax} ${ymin}, ${xmax} ${ymax}, ${xmin} ${ymax}, ${xmin} ${ymin}))'::geometry,${rattachment.epsg}));`;
	return seq.query(sRequest);
}

/**
 * Récupère la géométrie d'un rattachement
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT ST_Transform(geom_old_rattachment, 4326) as geom FROM travail.rattachment_attr_160157732353395 WHERE gid_old_rattachment = :object_id
 */
function getRatGeom(seq, rattachment, object_id) {
	const sRequest = `SELECT ST_Transform(${rattachment.fgeom}_old_rattachment, 4326) as geom FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid}_old_rattachment = :object_id`;
	return seq.query(sRequest, {
		replacements: {
			object_id: object_id
		}
	});
}

/**
 * Récupère la géométrie d'un objet de rattachement inverse
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT ST_Transform(geom, 4326) as geom FROM metier.voirie_geom WHERE gid = :object_id
 */
function getInverseRatGeom(seq, rattachment, object_id) {
	const sRequest = `SELECT ST_Transform(${rattachment.fgeom}, 4326) as geom FROM ${rattachment.tablegeom} WHERE ${rattachment.fid} = :object_id`;
	return seq.query(sRequest, {
		replacements: {
			object_id: object_id
		}
	});
}

/**
 * Récupère les attributs d'un objet de rattachement inverse
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid, gestionnaire, exploitant FROM metier.voirie_geom WHERE gid = :object_id
 */
function getInverseRatAttributes(seq, rattachment, object_id) {

  let str = `${rattachment.flist}`;
  let table = `${rattachment.tableattr}`;
  let split = str.split(',');
  let prefix = split.map(el => table + '.' + el);
  let res = prefix.join(',');

	const sRequest = `
		SELECT ${rattachment.tablegeom}.${rattachment.fid}, ${res} FROM ${rattachment.tablegeom} 
		INNER JOIN ${rattachment.tableattr} ON ${rattachment.tableattr}.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} 
		WHERE ${rattachment.tablegeom}.${rattachment.fid} = :object_id`;
	return seq.query(sRequest, {
		replacements: {
			object_id: object_id
		}
	});
}

/**
 * Rattache la géométrie d'un rattachement
 * @param {*} db 
 * @param {*} rattachment 
 *
 * UPDATE travail.rattachment_attr_160157732353395 SET gid = :geom_id WHERE gid_old_rattachment = :object_id
 */
function attachRatGeom(seq, rattachment, object_id, geom_id) {
	const sRequest = `UPDATE ${rattachment.schema_temp}.${rattachment.tableattr_temp} SET ${rattachment.fid} = :geom_id WHERE ${rattachment.fid}_old_rattachment = :object_id`;
	return seq.query(sRequest, {
		replacements: {
			object_id: object_id,
			geom_id: geom_id,
		}
	});
}

/**
 * Insère un objet dans la table attribut d'une session
 * @param {*} seq 
 * @param {*} rattachment 
 * @param {*} object_id 
 */
function insertRatAttribute(seq, rattachment, object_id, attributes) {

	attributes[rattachment.fid] = object_id;
	let valuesReq = '';
	let i = 0;
	for (const key in attributes) {
		if (attributes.hasOwnProperty(key)) {
			if (i > 0) {
				valuesReq += `, `;
			}
			valuesReq += `:${key}`
		}
		i++;
	}

	const sRequest = `INSERT INTO ${rattachment.tableattr} (${rattachment.fid}, ${rattachment.flist}) VALUES (${valuesReq})`;
	return seq.query(sRequest, {
		replacements: attributes
	});
}

/**
 * Récupère la géométrie de la session de rattachement sur une coordonnée
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT gid as id, ST_Transform(geom, 4326) as geom 
 *	FROM travail.rattachment_geom_160157732353395 
 *	WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 10), geom)
 *	ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom)
 *	LIMIT 1;
 * Ajout de NOT EXISTS pour n'afficher que les nouveaux filaires gris qui ne sont pas utilisés dans les données métier (car 1 filaire gris ne peut être utilisé qu'une fois)
 */
function getSessionGeom(seq, rattachment, params) {
	const sRequest = `SELECT ${rattachment.tablegeom_temp}.${rattachment.fid} as id, ST_Transform(${rattachment.fgeom}, 4326) as geom 
		FROM ${rattachment.schema_temp}.${rattachment.tablegeom_temp} 
		WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 1), ${rattachment.fgeom})
		AND NOT EXISTS (SELECT NULL FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} tattr WHERE tattr.${rattachment.fid} = ${rattachment.tablegeom_temp}.${rattachment.fid})
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom})
		LIMIT 1;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les géométries de la session de rattachement
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT gid as id, ST_Transform(geom, 4326) as geom 
 * FROM travail.rattachment_geom_160157732353395 
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom)
 * LIMIT 1000;
 * Ajout de NOT EXISTS pour n'afficher que les nouveaux filaires gris qui ne sont pas utilisés dans les données métier (car 1 filaire gris ne peut être utilisé qu'une fois)
 */
function getSessionGeoms(seq, rattachment, params) {
	const sRequest = `SELECT ${rattachment.fid} as id, ST_Transform(${rattachment.fgeom}, 4326) as geom 
		FROM ${rattachment.schema_temp}.${rattachment.tablegeom_temp} 
                WHERE NOT EXISTS (SELECT NULL FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} tattr WHERE tattr.${rattachment.fid} = ${rattachment.tablegeom_temp}.${rattachment.fid})
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom})
		LIMIT 1000;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les anciens filaires déjà rattachés
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT gid_old_rattachment as id, ST_Transform(geom_old_rattachment, 4326) as geom FROM travail.rattachment_attr_160157732353395 WHERE gid IS NOT NULL 
 *	ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom_old_rattachment)
 *	LIMIT 1000;
 */
function getOldRattachedGeoms(seq, rattachment, params) {
	const sRequest = `SELECT ${rattachment.fid}_old_rattachment as id, ST_Transform(${rattachment.fgeom}_old_rattachment, 4326) as geom FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NOT NULL 
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom}_old_rattachment)
		LIMIT 1000;`;
	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère un filaire encore à rattacher
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid_old_rattachment as id, ST_Transform(geom_old_rattachment, 4326) as geom
 * FROM tmp.rattachment_attr_161496554846171
 * WHERE gid IS NULL AND ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 5), geom_old_rattachment)
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom_old_rattachment)
 * LIMIT 1;
 */
function getOldNonRattachedGeom(seq, rattachment, params) {
	const sRequest = `SELECT ${rattachment.fid}_old_rattachment as id, ST_Transform(${rattachment.fgeom}_old_rattachment, 4326) as geom FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} 
		WHERE ${rattachment.fid} IS NULL AND ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 1), ${rattachment.fgeom}_old_rattachment)
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom}_old_rattachment)
		LIMIT 1;`;
	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les anciens filaires encore à rattacher
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid_old_rattachment as id, ST_Transform(geom_old_rattachment, 4326) as geom FROM travail.rattachment_attr_160157732353395 WHERE gid IS NULL 
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom_old_rattachment)
 * LIMIT 1000;
 */
function getOldNonRattachedGeoms(seq, rattachment, params) {
	const sRequest = `SELECT ${rattachment.fid}_old_rattachment as id, ST_Transform(${rattachment.fgeom}_old_rattachment, 4326) as geom FROM ${rattachment.schema_temp}.${rattachment.tableattr_temp} WHERE ${rattachment.fid} IS NULL 
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom}_old_rattachment)
		LIMIT 1000;`;
	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère une géométrie de la session, peut être rattachée ou non
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT metier.voirie_geom.gid AS id, ST_Transform(geom, 4326) AS geom, gestionnaire, exploitant FROM metier.voirie_geom
 * LEFT JOIN metier.voirie_attr ON metier.voirie_attr.gid = metier.voirie_geom.gid
 * WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), 10), geom)
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), geom)
 * LIMIT 1;
 */
function getInverseMetierGeom(seq, rattachment, params) {

  let str = `${rattachment.flist}`;
  let table = `${rattachment.tableattr}`;
  let split = str.split(',');
  let prefix = split.map(el => table + '.' + el);
  let res = prefix.join(',');

	const sRequest = `
		SELECT ${rattachment.tablegeom}.${rattachment.fid} AS id, ST_Transform(${rattachment.tablegeom}.${rattachment.fgeom}, 4326) AS geom, ${res} FROM ${rattachment.tablegeom} 
		LEFT JOIN ${rattachment.tableattr} ON ${rattachment.tableattr}.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} 
		WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 1), ${rattachment.tablegeom}.${rattachment.fgeom})
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.tablegeom}.${rattachment.fgeom})
		LIMIT 1;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les données depuis une jointure entre la table attributaire
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT metier.voirie_geom.gid AS id, ST_Transform(geom, 4326) AS geom, gestionnaire, exploitant FROM metier.voirie_geom
 * INNER JOIN metier.voirie_attr ON metier.voirie_attr.gid = metier.voirie_geom.gid
 * WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), 10), geom)
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), geom)
 * LIMIT 1;
 */
function getInverseMetierRatGeom(seq, rattachment, params) {

  let str = `${rattachment.flist}`;
  let table = `${rattachment.tableattr}`;
  let split = str.split(',');
  let prefix = split.map(el => table + '.' + el);
  let res = prefix.join(',');

	const sRequest = `
		SELECT ${rattachment.tablegeom}.${rattachment.fid} AS id, ST_Transform(${rattachment.tablegeom}.${rattachment.fgeom}, 4326) AS geom, ${res} FROM ${rattachment.tablegeom} 
		INNER JOIN ${rattachment.tableattr} ON ${rattachment.tableattr}.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} 
		WHERE ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 1), ${rattachment.tablegeom}.${rattachment.fgeom})
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.tablegeom}.${rattachment.fgeom})
		LIMIT 1;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les donnés métier depuis une jointure entre la table attributaire
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT metier.voirie_geom.gid AS id, ST_Transform(geom, 4326) AS geom, gestionnaire, exploitant FROM metier.voirie_geom
 * INNER JOIN metier.voirie_attr ON metier.voirie_attr.gid = metier.voirie_geom.gid
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), geom)
 * LIMIT 1;
 */
function getInverseMetierRatGeoms(seq, rattachment, params) {

  let str = `${rattachment.flist}`;
  let table = `${rattachment.tableattr}`;
  let split = str.split(',');
  let prefix = split.map(el => table + '.' + el);
  let res = prefix.join(',');

	const sRequest = `
		SELECT ${rattachment.tablegeom}.${rattachment.fid} AS id, ST_Transform(${rattachment.tablegeom}.${rattachment.fgeom}, 4326) AS geom, ${res} FROM ${rattachment.tablegeom} 
		INNER JOIN ${rattachment.tableattr} ON ${rattachment.tableattr}.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} 
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.tablegeom}.${rattachment.fgeom})
		limit 1000;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les filaires métier sans attribut
 * @param {*} db 
 * @param {*} rattachment 
 * 
 * SELECT metier.voirie_geom.gid AS id, ST_Transform(geom, 4326) AS geom, gestionnaire, exploitant FROM metier.voirie_geom
 * LEFT JOIN metier.voirie_attr ON metier.voirie_attr.gid = metier.voirie_geom.gid
 * WHERE metier.voirie_attr.gid is null
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(3.831047415733338 43.573741497192955)'), 2154), geom)
 * LIMIT 1;
 */
function getInverseMetierNonRatGeoms(seq, rattachment, params) {

  let str = `${rattachment.flist}`;
  let table = `${rattachment.tableattr}`;
  let split = str.split(',');
  let prefix = split.map(el => table + '.' + el);
  let res = prefix.join(',');

	const sRequest = `
		SELECT ${rattachment.tablegeom}.${rattachment.fid} AS id, ST_Transform(${rattachment.tablegeom}.${rattachment.fgeom}, 4326) AS geom, ${res} FROM ${rattachment.tablegeom} 
		LEFT JOIN ${rattachment.tableattr} ON ${rattachment.tableattr}.${rattachment.fid} = ${rattachment.tablegeom}.${rattachment.fid} 
		WHERE ${rattachment.tableattr}.${rattachment.fid} is null
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.tablegeom}.${rattachment.fgeom})
		limit 1000;`;

	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère la géométrie du filaire sur une coordonnée
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid as id, ST_Transform(geom, 4326) as geom, gestionnaire, exploitant
 * FROM osm.filairefromlbt
 * WHERE gid IS NOT null AND ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 10), geom)
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom)
 * LIMIT 1;
 */
function getFilaireGeom(seq, rattachment, params) {
	const sRequest = `
		SELECT ${rattachment.fid} as id, ST_Transform(${rattachment.fgeom}, 4326) as geom, ${rattachment.flist}
		FROM ${rattachment.tablefil}
		WHERE ${rattachment.fid} IS NOT null AND ST_Intersects(ST_Buffer(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), 1), ${rattachment.fgeom})
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom})
		LIMIT 1;`;
	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

/**
 * Récupère les filaires de la table définie dans rt_session.tablefil
 * @param {*} db 
 * @param {*} rattachment 
 *
 * SELECT gid as id, ST_Transform(geom, 4326) as geom, gestionnaire, exploitant
 * FROM osm.filairefromlbt
 * WHERE gid IS NOT null AND geom IS NOT null
 * ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), geom)
 * LIMIT 1000;
 */
function getFilaireGeoms(seq, rattachment, params) {
	const sRequest = `
		SELECT ${rattachment.fid} as id, ST_Transform(${rattachment.fgeom}, 4326) as geom, ${rattachment.flist}
		FROM ${rattachment.tablefil}
		WHERE ${rattachment.fid} IS NOT null AND ${rattachment.fgeom} IS NOT null
		ORDER BY ST_Distance(ST_Transform(ST_GeomFromEWKT('SRID=4326;POINT(:xcenter :ycenter)'), 2154), ${rattachment.fgeom})
		LIMIT 1000;`;
	return seq.query(sRequest, {
		replacements: {
			xcenter: Number(params.long),
			ycenter: Number(params.lat),
		}
	});
}

export default ({
	config,
	db
}) => {

	// Router
	const map = Router();

	// Modèle session de rattachement
	const Rattachment = db.rattachment;

	/**
	 * Retourne un arbre avec tous les objets non rattachés de l'ensemble des sessions
	 */
	map.get('/menu', (req, res, next) => {
		
		var xmin = req.query.Xmin;
		var xmax = req.query.Xmax;
		var ymin = req.query.Ymin;
		var ymax = req.query.Ymax;
		
		Rattachment.findAll({
			order: [
				['name', 'ASC'],
			],
			where: {
				isinverse: {
					[Sequelize.Op.not]: true
				}
			}
		})
			.then(rattachments => {
				const promises = rattachments.map((rattachment) => {

					// Connexion externe
					const seq = getSeqConnection(rattachment, db);

					return getNonRattachedObjects(seq, rattachment, xmin, xmax, ymin, ymax).then((objects) => {
						rattachment.dataValues.objects = objects[0];
						// compte le nombre d'objets et stocke dans nbOsmDiff
						rattachment.dataValues.nbOsmDiff = objects[0].length;
						return rattachment;
					}).catch((err) => {
						rattachment.dataValues.objects = [];
						return rattachment;
					});
				})
				Promise.all(promises).then((results) => {
					res.json(results);
				})
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Retourne un arbre avec tous les objets non rattachés de l'ensemble des sessions
	 */
	map.get('/menuinverse', (req, res, next) => {

		var xmin = req.query.Xmin;
		var xmax = req.query.Xmax;
		var ymin = req.query.Ymin;
		var ymax = req.query.Ymax;

		Rattachment.findAll({
			order: [
				['name', 'ASC'],
			],
			where: {
				isinverse: {
					[Sequelize.Op.is]: true
				}
			}
		})
			.then(rattachments => {
				const promises = rattachments.map((rattachment) => {

					// Connexion externe
					const seq = getSeqConnection(rattachment, db);

					return getNonAttrObject(seq, rattachment, xmin, xmax, ymin, ymax).then((objects) => {
						rattachment.dataValues.objects = objects[0];
						// compte le nombre d'objets et stocke dans nbOsmDiff
						rattachment.dataValues.nbOsmDiff = objects[0].length;
						return rattachment;
					}).catch((err) => {
						rattachment.dataValues.objects = [];
						return rattachment;
					});
				})
				Promise.all(promises).then((results) => {
					res.json(results);
				})
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère l'objet de la session
	 */
	map.get('/:rattachment_id/:object_id/object', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.rattachment_id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);
				getObjectById(seq, rattachment, req.params.object_id).then(objects => {
					res.json(objects[0][0]);
				}).catch(err => res.status(500).json({
					error: err
				}));

			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère la géométrie de l'objet de la session
	 */
	map.get('/:rattachment_id/:object_id/geom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.rattachment_id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				if (rattachment.isinverse === true) {
					getInverseRatGeom(seq, rattachment, req.params.object_id).then(geoms => {
						res.json(geoms[0][0].geom);
					}).catch(err => res.status(500).json({
						error: err
					}));
				} else {
					getRatGeom(seq, rattachment, req.params.object_id).then(geoms => {
						res.json(geoms[0][0].geom);
					}).catch(err => res.status(500).json({
						error: err
					}));
				}
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère les attributs de l'objet de la session
	 */
	map.get('/:rattachment_id/:object_id/attributes', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.rattachment_id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getInverseRatAttributes(seq, rattachment, req.params.object_id).then(objects => {
					res.json(objects[0][0]);
				}).catch(err => res.status(500).json({
					error: err
				}));
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Rattachement d'un objet
	 */
	map.put('/:rattachment_id/:object_id/attach', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.rattachment_id,
			}
		})
			.then(rattachment => {
				if (req.body &&
					req.body.geom_id) {

					// Connexion externe
					const seq = getSeqConnection(rattachment, db);

					attachRatGeom(seq, rattachment, req.params.object_id, req.body.geom_id).then(objects => {
						res.json(objects);
					}).catch(err => {
						console.error(err);
						res.status(500).json({
							error: err
						})
					});
				} else {
					console.error('geom_id not defined');
				}
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Rattachement inverse d'un objet
	 */
	map.put('/:rattachment_id/:object_id/insert_attr', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.rattachment_id,
			}
		})
			.then(rattachment => {
				if (req.body) {

					// Connexion externe
					const seq = getSeqConnection(rattachment, db);

					insertRatAttribute(seq, rattachment, req.params.object_id, req.body).then(objects => {
						res.json(objects);
					}).catch(err => {
						console.error(err);
						res.status(500).json({
							error: err
						})
					});
				} else {
					console.error('geom_id not defined');
				}
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère la géométrie de session sur une coordonnée
	 * Teste d'abord avec la couche des non rattachées (rouge) puis avec les filaires (gris)
	 */
	map.get('/:id/geom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				// Requête la couche rouge
				getOldNonRattachedGeom(seq, rattachment, req.query).then(nonRatGeoms => {
					if (nonRatGeoms[0] &&
						nonRatGeoms[0][0] &&
						nonRatGeoms[0][0].id) {
						nonRatGeoms[0][0].type = 'oldNonRattached'
						res.json(nonRatGeoms[0]);
					} else {

						// Si aucune feature rouge, requête la couche grise
						getSessionGeom(seq, rattachment, req.query).then(geoms => {
						if (geoms[0] &&
							geoms[0][0] &&
							geoms[0][0].id) {
								geoms[0][0].type = 'geom'
								res.json(geoms[0]);
							}
							
						}).catch(err => {
							console.error('err', err);
							res.status(500).json({
								error: err
							});
						});
					}

					// res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère les géométries de la session
	 */
	map.get('/:id/geoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getSessionGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère les anciens filaires déjà rattachés
	 */
	map.get('/:id/oldratgeoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getOldRattachedGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Récupère les anciens filaires encore à rattacher
	 */
	map.get('/:id/oldnonratgeom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getOldNonRattachedGeom(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0][0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Récupère les anciens filaires encore à rattacher
	 */
	map.get('/:id/oldnonratgeoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getOldNonRattachedGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Récupère la géométrie de session inverse sur une coordonnée
	 */
	map.get('/:id/inversemetiergeom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getInverseMetierGeom(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});

			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère la géométrie de session inverse sur une coordonnée
	 */
	map.get('/:id/inversemetierratgeom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getInverseMetierRatGeom(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});

			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère les filaires métier pour une session inverse
	 */
	map.get('/:id/inversemetierratgeoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getInverseMetierRatGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Récupère les filaires métier non rattachés (sans attributs) pour une session inverse
	 */
	map.get('/:id/inversemetiernonratgeoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getInverseMetierNonRatGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Récupère la géométrie de session sur une coordonnée
	 */
	map.get('/:id/filairegeom', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getFilaireGeom(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});

			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Récupère les filaires de la session
	 */
	map.get('/:id/filairegeoms', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Connexion externe
				const seq = getSeqConnection(rattachment, db);

				getFilaireGeoms(seq, rattachment, req.query).then(geoms => {
					res.json(geoms[0]);
				}).catch(err => {
					console.error('err', err);
					res.status(500).json({
						error: err
					});
				});
			})
			.catch(err => {
				console.error('err', err);
				res.status(500).json({
					error: err
				});
			});
	});

	/**
	 * Retourne un rattachement
	 */
	map.get('/:id', (req, res, next) => {
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => res.json(rattachment))
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Liste des rattachements
	 */
	map.get('/', (req, res, next) => {
		Rattachment.findAll({
			order: [
				['name', 'ASC'],
			],
		})
			.then(rattachment => res.json(rattachment))
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Création du rattachement
	 */
	map.post('/', (req, res, next) => {

		const Rattachment = db.rattachment;

		if (req.body.isinverse === true) {
			req.body.tableattr_temp = '';
			req.body.tablegeom_temp = '';
		} else {
			const uId = `${Date.now()}${Math.floor(Math.random() * 100)}`;
			req.body.tableattr_temp = `rattachment_attr_${uId}`;
			req.body.tablegeom_temp = `rattachment_geom_${uId}`;
		}

		const seq = getSeqConnection(req.body, db);

		// Transaction
		return db.sequelize.transaction((t) => {

			const trans = env_config.database === req.body.db ? t : null;

			// Création du rattachement
			return Rattachment.create(req.body, {
				transaction: t
			}).then((rattachment) => {
				if (rattachment.isinverse === true) {

					// Lance rattachement inverse
					// Date
					var d = new Date();
					const tableDate = d.getFullYear().toString() + ("0" + (d.getMonth() + 1)).slice(-2).toString() + ("0" + d.getDate()).slice(-2).toString();
					return runInverseRattachment(seq, rattachment, trans, tableDate).then(() => {
						// Retour ok
						res.json(rattachment);
					}).catch((err) => {
						console.error('runInverseRattachment error', err);
						// Rollback transaction
						t.rollback();
						// Retour erreur 500
						res.status(500).json({
							error: err
						})
					});

				} else {

					// Création d'un clone de la table métier
					return createTableMetier(seq, rattachment, trans).then(() => {

						// Création d'un clone de la table geom
						return createTableFilaire(seq, rattachment, trans).then(() => {

							// Retour ok
							res.json(rattachment);

						}).catch((err) => {
							console.error('createTableFilaire error', err);
							// Rollback transaction
							t.rollback();
							// Retour erreur 500
							res.status(500).json({
								error: err
							})
						});
					}).catch((err) => {
						console.error('createTableMetier error', err);
						// Rollback transaction
						t.rollback();
						// Retour erreur 500
						res.status(500).json({
							error: err
						})
					});
				}
			}).catch((err) => {
				console.error('Insert rattachment error', err);
				// Rollback transaction
				t.rollback();
				// Retour erreur 500
				res.status(500).json({
					error: err
				})
			});

		});
	});

	/**
	 * MAJ du rattachement
	 */
	map.put('/:id', (req, res, next) => {

		// Récupère le rattachement
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// Si fermeture de la session de rattachement
				if (req.body.isclosed === true &&
					rattachment.isclosed === false) {

					var d = new Date(Date.now());
					req.body.enddate = d.toISOString();

					return db.sequelize.transaction((t) => {

						// Mise à jour des attributs	
						return rattachment.update(req.body, {
							transaction: t
						}).then(() => {

							// Get nouvelles données
							return rattachment.reload({
								transaction: t
							}).then((updated) => {
								
								// Clôture du rattachement classique
								if (rattachment.isinverse !== true) {

								// Connexion externe
								const seq = getSeqConnection(rattachment, db);
								const trans = env_config.database === rattachment.db ? t : null;

								// Date
								//20210402 mois et jour sur 2 chiffres (ex : 01 au lieu de 1)
								var d = new Date();
 								//const tableDate = d.getFullYear().toString() + (d.getMonth() + 1).toString() + d.getDate().toString();
								const tableDate = d.getFullYear().toString() + ("0" + (d.getMonth() + 1)).slice(-2).toString() + ("0" + d.getDate()).slice(-2).toString();

								// Écrase les tables métier par les tables temporaires (pour le rattachement classique)
								return closeRattachmentTables(seq, rattachment, trans, tableDate).then(() => {

									res.json(updated)
								}).catch((err) => {
									console.error('closeRattachmentTables error', err);
									// Rollback transaction
									t.rollback();
									// Retour erreur 500
									res.status(500).json({
										error : '001 ' + err
									})
								});
								
							}
							
							// Clôture du rattachement inverse
							else
								
							{ 

								// Connexion externe
								const seq = getSeqConnection(rattachment, db);
								const trans = env_config.database === rattachment.db ? t : null;

								// Insère les géométries manquantes depuis la table de référence (pour le rattachement inverse)
								return closeReverseRattachmentTables(seq, rattachment, trans).then(() => {

									res.json(updated)
								}).catch((err) => {
									console.error('closeReverseRattachmentTables error', err);
									// Rollback transaction
									t.rollback();
									// Retour erreur 500
									res.status(500).json({
										error : '002 ' + err
									})
								});

							}
							
							}).catch((err) => {
								// Rollback transaction
								t.rollback();
								// Retour erreur 500
								res.status(500).json({
									error : '003 ' + err
								})
							});
						}).catch((err) => {
							console.error('Update session error', err);
							// Rollback transaction
							t.rollback();
							// Retour erreur 500
							res.status(500).json({
								error : '004 ' + err
							})
						});
					});
				} else {

					// Mise à jour des attributs
					rattachment.update(req.body)
						.then(() => (rattachment.reload()))
						.then((updated) => res.json(updated))
						.catch((err) => res.status(500).json({
							error : '005 ' + err
						}));
				}
			})
			.catch(err => res.status(500).json({
				error : '006 ' + err
			}));
	});

	/**
	 * Rattachement automatique : vacuum
	 */
	map.put('/:id/vacuum', (req, res, next) => {

		// Récupère le rattachement
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				const seq = getSeqConnection(rattachment, db);

				// Effectue le VACUUM hors transaction
				return vacuumTempTable(seq, rattachment.schema_temp, rattachment.tableattr_temp).then(() => {
					vacuumTempTable(seq, rattachment.schema_temp, rattachment.tablegeom_temp).then(() => {
						res.json("vacuum ok");
						//res.sendStatus(204);
					});
				});
			});
	});

	/**
	 * Rattachement automatique
	 */
	map.put('/:id/automatic', (req, res, next) => {

		// Récupère le rattachement
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// début transaction
				return db.sequelize.transaction((t) => {

					// Connexion externe
					const seq = getSeqConnection(rattachment, db);
					const trans = env_config.database === rattachment.db ? t : null;

					// Rattachement automatique
					return runAutoRattachment(seq, rattachment, trans).then(() => {
						countRattachment(seq, rattachment, trans).then((results) => {
							countTotal(seq, rattachment, trans).then((results2) => {
								res.json(results[0].concat(results2[0]));
							}).catch((err) => {
								console.error('runAutoRattachment error', err);
								// Rollback transaction
								t.rollback();
								// Retour erreur 500
								res.status(500).json({
									error: err
								})
							});
						});
					});
				});

			})
			.catch(err => res.status(500).json({
				error: err
			}));
	});

	/**
	 * Suppression du rattachement
	 */
	map.delete('/:id', (req, res, next) => {

		// Récupère le rattachement
		Rattachment.findOne({
			where: {
				id: req.params.id,
			}
		})
			.then(rattachment => {

				// début transaction
				return db.sequelize.transaction((t) => {

					// Suppression de la session de rattachement
					return rattachment.destroy({
						transaction: t
					}).then(() => {

						if (rattachment.isinverse === true) {
							res.sendStatus(204);
						} else {

							// Connexion externe
							const seq = getSeqConnection(rattachment, db);
							const trans = env_config.database === rattachment.db ? t : null;

							// Suppression des tables temporaires associées
							return dropRattachmentTables(seq, rattachment, trans).then(() => {
								res.sendStatus(204);
							}).catch((err) => {
								console.error('dropRattachmentTables error', err);
								// Rollback transaction
								t.rollback();
								// Retour erreur 500
								res.status(500).json({
									error: err
								})
							});
						}
					})
						.catch((err) => {
							console.error('Delete rattachment error', err);
							// Rollback transaction
							t.rollback();
							// Retour erreur 500
							res.status(500).json({
								error: err
							})
						});
				});
			});
	});

	return map;
}
