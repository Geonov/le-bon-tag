import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'contributor',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Contributor = db.contributor;
	
		Contributor.findByPk(id, {include: [Contributor.Vigilance]})
		.then(contributor => callback(null, contributor))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Contributor = db.contributor;
		Contributor.findAll(
			{
				include: [Contributor.Vigilance],
				order: [
					['auto_validation', 'DESC'],
					['internal', 'DESC'],
					['name', 'ASC'],
				],
			}
		)
		.then(contributors => res.json(contributors))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create(_, res) {
		res.status(403);
	},

	/** GET /:id - Return a given entity */
	read({ contributor }, res) {
		res.json(contributor);
	},

	/** PUT /:id - Update a given entity */
	update({ contributor, body }, res) {
		const Op = db.Sequelize.Op;
		const Vigilance = db.vigilance;

		const vigilanceFilter = body.internal
			? { name: 'internal' }
			: { range: { [Op.contains]: body.score } };

		db.sequelize
		.transaction((t) => {
			return Vigilance
				.findOne({ where: vigilanceFilter }, {transaction: t})
				.then((vigilance) => {
					return contributor
						.update(body, {transaction: t})
						.then(() => {
							return contributor.setVigilance(vigilance, {transaction: t});
						});
				});
		})
		.then(() => (contributor.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete(_, res) {
		res.status(403);
	}
});
