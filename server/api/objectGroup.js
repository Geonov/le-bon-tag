import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'objectGroup',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const ObjectGroup = db.objectGroup;
	
		ObjectGroup.findByPk(
			id,
			{ include: [ObjectGroup.Theme, ObjectGroup.Keys] }
		)
		.then(object => callback(null, object))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params, query }, res) {
		const ObjectGroup = db.objectGroup;
		ObjectGroup.scope(query.scope).findAll({
			order: [ ['name', 'ASC'] ],
			include:[ObjectGroup.Theme, ObjectGroup.Keys],
		})
		.then((objects) => res.json(objects))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const ObjectGroup = db.objectGroup;

		const model = ObjectGroup.build(body);
		model.save()
		.then((created) => created.setTheme(body.theme.id)
			.then(() => created.setKeys(body.keys.map((k) => k.id))
				.then(() => ObjectGroup.findByPk(
					created.id,
					{include:[ObjectGroup.Theme, ObjectGroup.Keys]}))))
		.then((o) => res.json(o))
		.catch(err => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ objectGroup }, res) {
		res.json(objectGroup);
	},

	/** PUT /:id - Update a given entity */
	update({ objectGroup, body }, res) {
		const ObjectGroup = db.objectGroup;

		db.sequelize
		.transaction((t) => objectGroup
			.update(body, {transaction: t})
			.then(() => objectGroup.setTheme(body.theme.id, {transaction: t})
					.then(() => objectGroup.setKeys(body.keys.map((k) => k.id), {transaction: t})))
		)
		.then(() => (objectGroup.reload({include:[ObjectGroup.Theme, ObjectGroup.Keys]})))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ objectGroup }, res) {
		db.sequelize
			.transaction((t) => {
				return objectGroup.setKeys([], {transaction: t})
				.then(() => objectGroup.destroy({transaction: t}));
		  	})
		  	.then(() => res.sendStatus(204))
			.catch((err) => res.status(500).json({error: err}));
	}
});
