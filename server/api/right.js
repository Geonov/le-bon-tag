import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'right',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Right = db.right;
	
		Right.findByPk(id, { include: [Right.Business, Right.Object, Right.Keys] })
		.then(right => callback(null, right))
		.catch(err => callback(console.log(err)))
	},
	
	/** GET / - List all entities */
	list({ params }, res) {
		const Right = db.right;

		Right.findAll({
			order: [
				[Right.Business, 'name', 'ASC'],
				[Right.Object, 'name', 'ASC'],
			],
			include: [Right.Business, Right.Object, Right.Keys],
		})
		.then(rights => res.json(rights))
		.catch(err => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Right = db.right;

		const model = Right.build(body);
		model.bg_id = body.business.id;
		model.og_id = body.object.id;
		model.save()
		.then((created) =>
			created.setKeys(body.keys.map((k) => k.id))
			.then(() => Right.findByPk(
				created.id,
				{ include: [Right.Business, Right.Object, Right.Keys]}))
		)
		.then((r) => res.json(r))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ right }, res) {
		res.json(right);
	},

	/** PUT /:id - Update a given entity */
	update({ right, body }, res) {
		body.bg_id = body.business.id;
		body.og_id = body.object.id;
		db.sequelize
			.transaction((t) => {
				return right
					.update(body, { transaction: t})
					.then(() => (right.setKeys(body.keys.map(k => k.id), {transaction: t})));
			})
			.then(() => (right.reload()))
		  	.then((updated) => res.json(updated))
			.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ right }, res) {
		db.sequelize
			.transaction((t) => {
				return right.setKeys([], {transaction: t})
			  		.then(() => {
						return right.destroy({transaction: t});
					});
		  	})
		  	.then(() => res.sendStatus(204))
			.catch((err) => res.status(500).json({error: err}));
	}
});
