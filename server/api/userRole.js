import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'userRole',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const UserRole = db.userRole;
	
		UserRole.findByPk(id)
		.then(userRole => callback(null, userRole))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const UserRole = db.userRole;
		UserRole.findAll({
			order: [ ['desc', 'ASC'] ],
		})
			.then(roles => res.json(roles))
			.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const UserRole = db.userRole;
		UserRole.create(body).then((userRole) => res.json(userRole)).catch(err => console.log(err));
	},

	/** GET /:id - Return a given entity */
	read({ userRole }, res) {
		res.json(userRole);
	},

	/** PUT /:id - Update a given entity */
	update({ userRole, body }, res) {
		userRole.update(body)
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ userRole }, res) {
		userRole.destroy()
		.then(() => res.sendStatus(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
