import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'status',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Status = db.status;
	
		Status.findByPk(id)
		.then(status => callback(null, status))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Status = db.status;

		Status.findAll()
		.then(statuses => res.json(statuses))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Status = db.status;

		Status.create(body)
		.then((status) => res.json(status))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ validation }, res) {
		res.json(validation);
	},

	/** PUT /:id - Update a given entity */
	update({ status, body }, res) {
		status
		.update(body)
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete(_, res) {
		res.status(403);
	}
});
