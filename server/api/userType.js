import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'userType',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const UserType = db.userType;
	
		UserType.findByPk(id)
		.then(userType => callback(null, userType))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const UserType = db.userType;
		UserType.findAll()
			.then(types => res.json(types))
			.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const UserType = db.userType;
		UserType.create(body).then((userType) => res.json(userType)).catch(err => console.log(err));
	},

	/** GET /:id - Return a given entity */
	read({ userType }, res) {
		res.json(userType);
	},

	/** PUT /:id - Update a given entity */
	update({ userType, body }, res) {
		userType.update(body)
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ userType }, res) {
		userType.destroy()
		.then(() => res.sendStatus(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
