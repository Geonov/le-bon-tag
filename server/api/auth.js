import bcrypt from 'bcryptjs';
import jwt from 'jwt-simple';
import ldap from 'ldapjs';
import env_config from './../config/config';

export const authorization = (db) => (req, res, next) => {
	const authorization = req.headers.authorization;
	if (!authorization) {
		return res.status(401).json({ error: 'No credentials sent!' });
	}
	const token = authorization.split(/Bearer\s?/)[1];
	const decoded = jwt.decode(token, env_config.jwt_secret);
	if (!decoded) {
		res.status(401);
	}
	const User = db.user;

	User.findByPk(
		decoded.id,
		{
			include: [{ association: User.BusinessGroup }],
		}
	)
	.then((user) => {
		req.authenticated_user = user;
	})
	.catch((err) => {
		console.log('error loading authenticated user', JSON.stringify(err));
	})
	.finally(() => next());
};

export const auth = (db) => (req, res, next) => {
	const {login, password} = req.body;
    const User = db.user;
    const Setting = db.setting;
    const UserRole = db.userRole;
    const UserType = db.userType;
    const Op = db.Sequelize.Op;

	User.findOne({
		where: { login: login },
		include: [
			User.BusinessGroup,
			User.Type,
			User.Role
		],
    })
    .then((user) => {
        if (!user || user.type.name === 'ldap') {
            // try ldap search (bind)
            return Setting.findAll({
                where: {
                    name: {
                        [Op.like]: 'ldap\_%',
                    },
                },
            })
            // save/update user (return Promise) or status 404 (res)
            .then((settings) => {
                // convert settings to options
                const options = settings.reduce((a, s) => {
                    a[s.name] = s.value;
                    return a;
                }, {});

				if (options.ldap_host) {
				return ldapAuth(login, password, options);
				// Gère les erreurs mais ne peut plus se connecter avec un user LDAP...
				//return Promise(ldapAuth(login, password, options));
				}

            })
            .then((ldapUser) => {
                console.log('ldap user found', ldapUser);
                if (user) {
                    // console.log('update user', user);
                    return user.update(ldapUser);
                }
                // set role and type
                return Promise.all([
                    UserRole.findOne({
                        where: {
                            name: 'user',
                        },
                    }),
                    UserType.findOne({
                        where: {
                            name: 'ldap',
                        },
                    })
                ])
                .then(
                    (userSettings) => {
                        // console.log('settings', userSettings);
                        const [userRole, userType] = userSettings;
                        ldapUser.u_ur_id = userRole.id;
                        ldapUser.u_ut_id = userType.id;
                        console.log('create ldap user', ldapUser);
                        return User.create(ldapUser).then((created) => 
                            User.findByPk(created.id, {
                                include: [
                                    User.BusinessGroup,
                                    User.Type,
                                    User.Role,
                                ]
                            })
                        );
                    },
                    (err) => Promise.reject(err)
                );
            })
        }

        // no ldap type user 
		return bcrypt.compare(password, user.password).then((result) => {
			if (!result) {
                // res.status(404);
                return Promise.reject();
            }
            
            return Promise.resolve(user);
		})
    })
    .then(
        (user) => {
            console.log('create user token...');
            // create JWT
            const payload = {
                login: user.login,
                role: user.role.name,
                id: user.id,
            };
            const token = jwt.encode(payload, env_config.jwt_secret);
            res.json({
                ...payload,
                token,
            });
        },
        (err) => res.status(404).json(err)
    )
    .catch((error) => next(error));
}

const ldapAuth = (login, password, options) => {

    const clientOptions = {
        url: `${options.ldap_host}:${options.ldap_port}`,
		strictDN: true,
		reconnect: false,
		queueDisable: false,
		timeout: 10000,
		connectTimeout: 10000,
		idleTimeout: 10000,
        tlsOptions: {
            //ca: [options.ldap_tls_cert],
            rejectUnauthorized: [options.ldap_cert_validity],
			enableTrace: false,
        },
    };

    console.log('create ldap client');
    const client = ldap.createClient(clientOptions);

	client.on('error', (err) => {
		console.error("An error is occured", err);
		//return ("An error is occured");
	});
	client.on('timeout', (err) => {
		console.error("Operation timing out");
		//return ("Operation timing out");
	});
	client.on('connectTimeout', (err) => {
		console.error("TCP connection timing out");
		//return ("TCP connection timing out");
	});
	client.on('idleTimeout', (err) => {
		console.info("Idle timing out");
		//return ("Idle timing out");
	});

    if (options.ldap_encrypt === 'TLS') {
        console.log('start TLS connection');
        return new Promise((resolve, reject) => {
            client.starttls(clientOptions.tlsOptions, client.controls, function(err) {
                if (err) {
                    console.error('starttls error', JSON.stringify(err));
                    reject(err);
                    return;
                }
                resolve(ldapSearch(client, login, password, options));
            });
        });
    }
    return Promise.resolve(ldapSearch(client, login, password, options));
}

const ldapSearch = (client, login, password, options) => {

    const convertUser = (ldapModel) => {
        console.log('convert user', ldapModel);
        return {
            login: ldapModel[options.ldap_login_attribute],
            first_name: ldapModel[options.ldap_firstname_attribute],
            last_name: ldapModel[options.ldap_lastname_attribute],
            email: ldapModel[options.ldap_email_attribute],
        };
    }

    const ldapDnSearch = {
        filter: `(&(${options.ldap_login_attribute}=${login}))`,
        scope: 'sub',
        attributes: [
            'dn',
            options.ldap_login_attribute,
            options.ldap_firstname_attribute,
            options.ldap_lastname_attribute,
            options.ldap_email_attribute,
        ],
    };

    return new Promise((resolve, reject) => {
        console.log('bind user dn...');
        client.bind(options.ldap_bind_user, options.ldap_bind_password, function(err) {
            if (err) {
                console.error('bind error user dn');
                reject(err);
                return;
            }
            resolve();
        });
    })
    .then(
        () => new Promise((resolve, reject) => {
            console.log('search user dn...');
            client.search(options.ldap_base_dn, ldapDnSearch, function(err, res) {
                let localEntry = null;
                if (err) {
                    console.error('search error');
                    reject(err);
                    return;
                };
                res.on('searchEntry', function(entry) {
                    // On se connecte (BIND) avec le 'dn' trouvé et le mot de passe fourni
                    // Si la connexion fonctionne, le mot de passe fourni est le bon
                    // Sinon le mot de passe est invalide (échec de connexion)
                    localEntry = entry.object;
                });
                res.on('end', function(result) {
                    console.log('search end with', localEntry);
                    localEntry
                        ? resolve(localEntry)
                        : reject(null);
                });
                res.on('error', function(err) {
                    console.error('search on error');
                    reject(err);
                });
            });
        }),
        (err) => { throw new Error(err) })
    .then(
        (ldapUser) => new Promise((resolve, reject) => {
            console.log('bind user dn...');
            client.bind(ldapUser.dn, password, function(err) {
                if (err) {
                    console.error('bind error user');
                    reject(err);
                    return;
                }
                client.unbind(function(err) {
                    if (err) {
                        console.error('unbind user error');
                    }
                    resolve(ldapUser);
                });
            });
        }))
    .then(
        (ldapUser) => new Promise((resolve) => {
            client.unbind(function(err) {
                if (err) {
                    console.error('unbind user dn error');
                }
                resolve(convertUser(ldapUser));
            });
        }),
        (err) => Promise.reject(err)
    );
}
