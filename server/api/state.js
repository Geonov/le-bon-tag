import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'state',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Status = db.state;
	
		Status.findByPk(id)
		.then(state => callback(null, state))
		.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Status = db.state;

		Status.findAll()
		.then(states => res.json(states))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Status = db.state;

		Status.create(body)
		.then((state) => res.json(state))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** GET /:id - Return a given entity */
	read({ validation }, res) {
		res.json(validation);
	},

	/** PUT /:id - Update a given entity */
	update({ state, body }, res) {
		state
		.update(body)
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete(_, res) {
		res.status(403);
	}
});
