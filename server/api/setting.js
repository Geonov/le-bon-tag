import resource from 'resource-router-middleware';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'setting',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		const Setting = db.setting;
		Setting.findByPk(id)
			.then(setting => callback(null, setting))
        	.catch(err => callback(console.log(err)))
	},

	/** GET / - List all entities */
	list({ params }, res) {
		const Setting = db.setting;

		Setting.findAll()
		.then(settings => res.json(settings))
		.catch(err => console.log(err))
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		const Setting = db.setting;

		Setting.create(body)
		.then((setting) => res.json(setting))
		.catch((err) => res.status(403).json({error: JSON.stringify(err)}));
	},

	/** GET /:id - Return a given entity */
	read({ setting }, res) {
		res.json(setting);
	},

	/** PUT /:id - Update a given entity */
	update({ setting, body }, res) {
		setting.update(body)
		.then(() => (setting.reload()))
		.then((updated) => res.json(updated))
		.catch((err) => res.status(500).json({error: err}));
	},

	/** DELETE /:id - Delete a given entity */
	delete({ setting }, res) {
		setting.destroy()
		.then(() => res.status(204))
		.catch((err) => res.status(500).json({error: err}));
	}
});
