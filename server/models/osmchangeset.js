'use strict';
module.exports = (sequelize, DataTypes) => {
  const OsmChangeset = sequelize.define('osmchangeset', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      field: 'ocs_id',
    },
    tags: {
      type: DataTypes.HSTORE,
      field: 'ocs_tags',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_osmchangeset',
    timestamps: false,
    underscored: true,
  });
  OsmChangeset.associate = function(models) {
    // associations can be defined here
  };
  return OsmChangeset;
};