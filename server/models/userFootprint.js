/* eslint-disable prettier/prettier */

'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserFootprint = sequelize.define('userFootprint', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: false,
      field: 'u_id',
    },
    fp_id: {
      type: DataTypes.INTEGER,
      primaryKey: false,
      autoIncrement: false,
      field: 'fp_id',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_user_footprint',
    timestamps: false,
    underscored: true,
  });
  UserFootprint.associate = function (models) {
    // associations can be defined here
  };
  return UserFootprint;
};