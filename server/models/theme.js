'use strict';

module.exports = (sequelize, DataTypes) => {
  const Theme = sequelize.define('theme', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'th_id',
    },
    name: {
      type: DataTypes.STRING,
	  allowNull: false,
      unique: true,
      field: 'th_name',
    },
    icon: {
      type: DataTypes.STRING,
      field: 'th_icon',
    },
    color: {
      type: DataTypes.STRING,
      field: 'th_color',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_theme',
    timestamps: false,
    underscored: true,
  });
  Theme.associate = function(models) {
    // associations can be defined here
    Theme.ObjectGroup = Theme.hasMany(
      models['objectGroup'],
      {
        as: 'objects',
        foreignKey: 'og_th_id',
      }
    );
  };
  return Theme;
};
