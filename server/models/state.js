'use strict';
module.exports = (sequelize, DataTypes) => {
  const Status = sequelize.define('state', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'state_id',
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      field: 'state_name',
    },
    desc_fr: {
      type: DataTypes.STRING,
      field: 'state_desc_fr',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_state',
    timestamps: false,
    underscored: true,
  });
  Status.associate = function(models) {
    // associations can be defined here
  };
  return Status;
};
