'use strict';
module.exports = (sequelize, DataTypes) => {
  const Status = sequelize.define('status', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'st_id',
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      field: 'st_name',
    },
    desc_fr: {
      type: DataTypes.STRING,
      field: 'st_desc_fr',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_status',
    timestamps: false,
    underscored: true,
  });
  Status.associate = function(models) {
    // associations can be defined here
  };
  return Status;
};
