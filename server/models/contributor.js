'use strict';
module.exports = (sequelize, DataTypes) => {
  const Contributor = sequelize.define('contributor', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'ct_id',
    },
    name: {
      type: DataTypes.STRING,
      field: 'ct_name',
    },
    score: {
      type: DataTypes.INTEGER,
      default: 0,
      field: 'ct_score',
    },
    internal: {
      type: DataTypes.BOOLEAN,
      default: false,
      field: 'ct_internal',
    },
    auto_validation: {
      type: DataTypes.BOOLEAN,
      default: false,
      field: 'ct_auto_validation',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_contributor',
    timestamps: false,
    underscored: true,
  });
  Contributor.associate = function(models) {
    // associations can be defined here
    Contributor.Vigilance = Contributor.belongsTo(models['vigilance'], { as: 'vigilance', foreignKey: 'ct_vg_id' });
  };
  return Contributor;
};
