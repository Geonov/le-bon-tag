'use strict';
module.exports = (sequelize, DataTypes) => {
  const Footprint = sequelize.define('footprint', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'fp_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'fp_name',
    },
    geom: {
      type: DataTypes.GEOMETRY(),
      allowNull: false,
      field: 'fp_geom',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_footprint',
    timestamps: false,
    underscored: true,
  });
  Footprint.associate = function(models) {
    // associations can be defined here
    Footprint.User = Footprint.belongsToMany(
      models['user'],
      {
        as: 'users',
        through: 'lbt_user_footprint',
        foreignKey: 'fp_id',
        otherKey: 'u_id',
        timestamps: false,
      }
    );
  };
  return Footprint;
};
