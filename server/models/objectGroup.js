'use strict';
import Sequelize from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  const ObjectGroup = sequelize.define('objectGroup', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'og_id',
    },
    name: {
      type: DataTypes.STRING,
	  allowNull: false,
	  unique: true,
      field: 'og_name',
    },
	// NOTE : Le champ og_query n'est plus utilisé
    query: {
      type: DataTypes.HSTORE,
      field: 'og_query',
    },
    query_json: {
      type: DataTypes.TEXT,
      field: 'og_query_json',
      get() {
        const query = this.getDataValue('query_json');
        try {
          return JSON.parse(query);
        } catch (error) {
          return query
        }
      },
      set(val) {
        this.setDataValue('query_json', JSON.stringify(val));
      }
    },
    active: {
      type: DataTypes.BOOLEAN,
      field: 'og_active',
    },
    auto: {
      type: DataTypes.BOOLEAN,
      field: 'og_auto',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_objectgroup',
    timestamps: false,
    underscored: true,
    scopes: {
      withoutTheme: {
        where: {
          og_th_id: {
            [Sequelize.Op.eq]: null
          }
        },
      },
    },
  });
  ObjectGroup.associate = function (models) {
    // associations can be defined here
    ObjectGroup.Theme = ObjectGroup.belongsTo(
      models['theme'], {
        as: 'theme',
        foreignKey: 'og_th_id',
      }
    );
    // NOTE : La table lbt_objectgroup_tag n'est plus utilisée
    ObjectGroup.Keys = ObjectGroup.belongsToMany(
      models['tag'], {
        as: 'keys',
        through: 'lbt_objectgroup_tag',
        foreignKey: 'og_id',
        otherKey: 'tg_id',
        timestamps: false,
      }
    );
    ObjectGroup.Right = ObjectGroup.belongsToMany(
      models['right'], {
        as: 'rights',
        through: 'lbt_right',
        foreignKey: 'og_id',
        otherKey: 'r_id',
        timestamps: false,
      }
    );
  };
  return ObjectGroup;
};