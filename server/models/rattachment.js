'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rattachment = sequelize.define('rattachment', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'rtsession_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'rtsession_name',
    },
    host: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_host',
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: false,
      field: 'rtsession_port',
    },
    db: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_db',
    },
    user: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
      field: 'rtsession_user',
    },
    pass: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
      field: 'rtsession_password',
    },
    tablefil: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_tablefil',
    },
    tablefil_attr: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_tablefil_attr',
    },
    tableattr: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_tableattr',
    },
    tablegeom: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_tablegeom',
    },
    schema_temp: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
      field: 'rtsession_schema_temp',
    },
    fid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_fid',
    },
    fgeom: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_fgeom',
    },
    flist: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
      field: 'rtsession_flist',
    },
    tolerance: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      unique: false,
      field: 'rtsession_tolerance',
    },
    epsg: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: false,
      field: 'rtsession_epsg',
    },
    tableattr_temp: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'rtsession_tableattr_temp',
    },
    tablegeom_temp: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'rtsession_tablegeom_temp',
    },
    startdate: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      field: 'rtsession_startdate',
    },
    enddate: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      field: 'rtsession_enddate',
    },
    isclosed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0,
      field: 'rtsession_close',
    },
    isinverse: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0,
      field: 'rtsession_isreverse',
    },
    tablefil_where: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'rtsession_tablefil_where',
    },
    isfollowedbyreverse: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0,
      field: 'rtsession_isfollowedbyreverse',
    }
  }, {
    schema: 'lebontag',
    tableName: 'lbt_rattachment_session',
    timestamps: false
  });
  Rattachment.associate = function(models) {
    // associations can be defined here
  };
  return Rattachment;
};
