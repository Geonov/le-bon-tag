'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('tag', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'tg_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'tg_name',
    },
    name_fr: {
      type: DataTypes.STRING,
      field: 'tg_name_fr',
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      field: 'tg_deleted',
      default: false,
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_tag',
    timestamps: false
  });
  Tag.associate = function(models) {
    // associations can be defined here
    Tag.ObjectGroup = Tag.belongsToMany(
      models['objectGroup'],
      {
        as: 'objects',
        through: 'lbt_objectgroup_tag',
        foreignKey: 'tg_id',
        otherKey: 'og_id',
        timestamps: false,
      }
    );
    // Tag.Right = Tag.belongsToMany(
    //   models['right'],
    //   {
    //     as: 'rights',
    //     through: 'lbt_right_tag',
    //     foreignKey: 'tg_id',
    //     // otherKey: ['bg_id', 'og_id'],
    //     constraints: false,
    //     timestamps: false,
    //   }
    // );
  };
  return Tag;
};