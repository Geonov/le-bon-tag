'use strict';

module.exports = (sequelize, DataTypes) => {
  const Right = sequelize.define('right', {
    // bug https://github.com/sequelize/sequelize/issues/311
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'r_id',
    },
	r_verif_geom: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
	  defaultValue: 1,
      field: 'r_verif_geom',
    },
	r_verif_all_tags: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
	  defaultValue: 0,
      field: 'r_verif_all_tags',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_right',
    timestamps: false,
    underscored: true,
    scopes: {
      exist: (b, o) => {
        return {
          where: {
            bg_id: b,
            og_id: o,
          },
          limit: 1,
        };
      },
      keys: (b, o) => {
        return {
          where: {
            bg_id: b,
            og_id: o,
          },
        };
      },
    },
  });
  Right.associate = function(models) {
    // associations can be defined here
    Right.Business = Right.belongsTo(
      models['businessGroup'],
      {
        as: 'business',
		allowNull: false,
        foreignKey: 'bg_id',
      }
    );
    Right.Object = Right.belongsTo(
      models['objectGroup'],
      {
        as: 'object',
		allowNull: false,
        foreignKey: 'og_id',
      }
    );
    Right.Keys = Right.belongsToMany(
      models['tag'],
      {
        as: 'keys',
        through: 'lbt_right_tag',
        foreignKey: 'r_id',
        otherKey: 'tg_id',
        timestamps: false,
      }
    );
  };
  return Right;
};
