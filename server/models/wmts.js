'use strict';
module.exports = (sequelize, DataTypes) => {
  const Wmts = sequelize.define('wmts', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'wmts_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'wmts_name',
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_url',
    },
    layer: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_layer',
    },
    format: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_format',
    },
    matrixset: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_matrix_set',
    },
    style: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_style',
    },
    attribution: {
      type: DataTypes.STRING,
      field: 'wmts_attribution',
    },
    maxzoom: {
      type: DataTypes.INTEGER,
      field: 'wmts_maxzoom',
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_type',
    },
    protocol: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wmts_protocol',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_wmts',
    timestamps: false
  });
  Wmts.associate = function(models) {
    // associations can be defined here
  };
  return Wmts;
};
