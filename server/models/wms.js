'use strict';
module.exports = (sequelize, DataTypes) => {
  const Wms = sequelize.define('wms', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'wms_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'wms_name',
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wms_url',
    },
    layer: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wms_layer',
    },
    format: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wms_format',
    },
    attribution: {
      type: DataTypes.STRING,
      field: 'wms_attribution',
    },
    maxzoom: {
      type: DataTypes.INTEGER,
      field: 'wms_maxzoom',
    },
    alpha: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
	  defaultValue: 0,
      field: 'wms_alpha',
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'wms_type',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_wms',
    timestamps: false
  });
  Wms.associate = function(models) {
    // associations can be defined here
  };
  return Wms;
};
