'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserType = sequelize.define('userType', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'ut_id',
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      field: 'ut_name',
    },
    desc: {
      type: DataTypes.STRING,
      field: 'ut_desc_fr',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_usertype',
    timestamps: false,
    underscored: true,
  });
  UserType.associate = function(models) {
    // associations can be defined here
  };
  return UserType;
};
