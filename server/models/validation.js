'use strict';
module.exports = (sequelize, DataTypes) => {
  const Validation = sequelize.define('validation', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'val_id',
    },
    tags: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      field: 'val_tags',
    },
    comment: {
      type: DataTypes.TEXT,
      field: 'val_comment',
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'val_od_version',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_validation',
    timestamps: true,
    createdAt: 'val_created_at',
    updatedAt: false,
    underscored: true,
  });
  Validation.associate = function(models) {
    // associations can be defined here
    Validation.Status = Validation.belongsTo(
      models['status'],
      {
        as: 'status',
        foreignKey: 'val_st_id'
      }
    );
    Validation.User = Validation.belongsTo(
      models['user'],
      {
        as: 'user',
        foreignKey: 'val_u_id'
      }
    );
    Validation.Object = Validation.belongsTo(
      models['osmdiff'],
      {
        as: 'object',
        foreignKey: 'val_od_id'
      }
    );
  };
  return Validation;
};
