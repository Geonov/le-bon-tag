'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserRole = sequelize.define('userRole', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'ur_id',
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      field: 'ur_name',
    },
    desc: {
      type: DataTypes.STRING,
      field: 'ur_desc_fr',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_userrole',
    timestamps: false,
    underscored: true,
  });
  UserRole.associate = function(models) {
    // associations can be defined here
  };
  return UserRole;
};
