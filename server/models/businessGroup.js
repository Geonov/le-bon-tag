'use strict';
module.exports = (sequelize, DataTypes) => {
  const BusinessGroup = sequelize.define('businessGroup', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'bg_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'bg_name',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_businessgroup',
    timestamps: false,
    underscored: true,
  });
  BusinessGroup.associate = function(models) {
    // associations can be defined here
    BusinessGroup.User = BusinessGroup.belongsToMany(
      models['user'],
      {
        as: 'users',
        through: 'lbt_user_businessgroup',
        foreignKey: 'bg_id',
        otherKey: 'u_id',
        timestamps: false,
      }
    );
    BusinessGroup.Right = BusinessGroup.belongsToMany(
      models['right'],
      {
        as: 'rights',
        through: 'lbt_right',
        foreignKey: 'bg_id',
        otherKey: 'r_id',
        timestamps: false,
      }
    );
  };
  return BusinessGroup;
};