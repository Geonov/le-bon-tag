'use strict';
module.exports = (sequelize, DataTypes) => {
  const Setting = sequelize.define('setting', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 's_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      unique: true,
      field: 's_name',
    },
    value: {
      type: DataTypes.STRING,
      field: 's_value',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_setting',
    timestamps: false,
    underscored: true,
  });
  Setting.associate = function(models) {
    // associations can be defined here
  };
  return Setting;
};
