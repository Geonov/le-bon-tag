'use strict';
module.exports = (sequelize, DataTypes) => {
  const Vigilance = sequelize.define('vigilance', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'vg_id',
    },
    name: {
      type: DataTypes.STRING,
      field: 'vg_name',
    },
    desc_fr: {
      type: DataTypes.STRING,
      field: 'vg_desc_fr',
    },
    range: {
      type: DataTypes.RANGE,
      field: 'vg_range',
    },
    color: {
      type: DataTypes.STRING,
      field: 'vg_color',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_vigilance',
    timestamps: false,
    underscored: true,
  });
  Vigilance.associate = function(models) {
    // associations can be defined here
  };
  return Vigilance;
};
