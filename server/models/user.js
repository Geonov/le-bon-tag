'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'u_id',
    },
    login: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'u_login',
    },
    first_name: {
      type: DataTypes.STRING,
      field: 'u_first_name',
    },
    last_name: {
      type: DataTypes.STRING,
      field: 'u_last_name',
    },
    email: {
      type: DataTypes.STRING,
      field: 'u_email',
    },
    password: {
      type: DataTypes.STRING,
      field: 'u_password',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_user',
    timestamps: false,
    underscored: true,
  });
  User.associate = function(models) {
    // associations can be defined here
    User.Role = User.belongsTo(models['userRole'], { as: 'role', foreignKey: 'u_ur_id' });
    User.Type = User.belongsTo(models['userType'], { as: 'type', foreignKey: 'u_ut_id' });
    User.BusinessGroup = User.belongsToMany(
      models['businessGroup'],
      {
        as: 'groups',
        through: 'lbt_user_businessgroup',
        foreignKey: 'u_id',
        otherKey: 'bg_id',
        timestamps: false,
      }
    );
    User.Footprint = User.belongsToMany(
      models['footprint'],
      {
        as: 'footprints',
        through: 'lbt_user_footprint',
        foreignKey: 'u_id',
        otherKey: 'fp_id',
        timestamps: false,
      }
    );
  };
  return User;
};
