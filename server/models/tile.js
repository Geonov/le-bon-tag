'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tile = sequelize.define('tile', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'tile_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      field: 'tile_name',
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'tile_url',
    },
    layer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'tile_layer',
    },
    format: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'tile_format',
    },
    attribution: {
      type: DataTypes.STRING,
      field: 'tile_attribution',
    },
    maxzoom: {
      type: DataTypes.INTEGER,
      field: 'tile_maxzoom',
    },
    alpha: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
	  defaultValue: 0,
      field: 'tile_alpha',
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
	  defaultValue: 0,
      field: 'tile_visible',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_tile',
    timestamps: false
  });
  Tile.associate = function(models) {
    // associations can be defined here
  };
  return Tile;
};
