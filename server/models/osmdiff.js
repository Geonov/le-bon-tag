'use strict';
module.exports = (sequelize, DataTypes) => {
  const OsmDiff = sequelize.define('osmdiff', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      field: 'od_id',
      allowNull: false,
    },
    name: {
      type: DataTypes.TEXT,
      field: 'od_name',
    },
    action: {
      type: DataTypes.STRING(6),
      allowNull: false,
      field: 'od_action',
    },
    element: {
      type: DataTypes.STRING(8),
      allowNull: false,
      field: 'od_element',
    },
    version :{
      type: DataTypes.INTEGER,
      field: 'od_new_version',
    },
    new_timestamp :{
      type: DataTypes.DATE,
      field: 'od_new_timestamp',
    },
    new_tags: {
      type: DataTypes.HSTORE,
      field: 'od_new_tags',
    },
    old_tags: {
      type: DataTypes.HSTORE,
      field: 'od_old_tags',
    },
    new_geom: {
      type: DataTypes.GEOMETRY,
      field: 'od_new_geom',
    },
    old_geom: {
      type: DataTypes.GEOMETRY,
      field: 'od_old_geom',
    },
    added_tags: {
      type: DataTypes.HSTORE,
      field: 'od_added_tags',
      allowNull: false,
      defaultValue: '',
    },
    deleted_tags: {
      type: DataTypes.HSTORE,
      field: 'od_deleted_tags',
      allowNull: false,
      defaultValue: '',
    },
    modified_tags: {
      type: DataTypes.HSTORE,
      field: 'od_modified_tags',
      allowNull: false,
      defaultValue: '',
    },
    unchanged_tags: {
      type: DataTypes.HSTORE,
      field: 'od_unchanged_tags',
      allowNull: false,
      defaultValue: '',
    },
    changed_geom: {
      type: DataTypes.BOOLEAN,
      field: 'od_changed_geom',
    },
    comment: {
      type: DataTypes.STRING,
      field: 'od_comment',
    },
    od_extent: {
      type: DataTypes.TEXT,
      field: 'od_extent',
    },
    od_mapillary: {
      type: DataTypes.TEXT,
      field: 'od_mapillary',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_osmdiff',
    timestamps: false,
    underscored: true,
  });
  OsmDiff.associate = function(models) {
    // associations can be defined here
    OsmDiff.Contributor = OsmDiff.belongsTo(
      models['contributor'],
      {
        as: 'contributor',
        foreignKey: 'od_new_ct_id',
      }
    );
    OsmDiff.NewChangeset = OsmDiff.belongsTo(
      models['osmchangeset'],
      {
        as: 'new_changeset',
        foreignKey: 'od_new_changeset',
      }
    );
    OsmDiff.Validation = OsmDiff.hasMany(
      models['validation'],
      {
        as: 'validations',
        foreignKey: 'val_od_id',
      }
    );
    OsmDiff.Status = OsmDiff.belongsTo(
      models['status'],
      {
        as: 'status',
        foreignKey: 'od_status',
      }
    );
    // 1 osmdiff est lié à n emprises via la table lbt_osmdiff_footprint
    OsmDiff.Footprint = OsmDiff.belongsToMany(
      models['footprint'],
      {
        as: 'footprints',
        through: 'lbt_osmdiff_footprint',
        foreignKey: 'od_id',
        otherKey: 'fp_id',
        timestamps: false,
      }
    );
  };
  return OsmDiff;
};