'use strict';
module.exports = (sequelize, DataTypes) => {
  const lbtExport = sequelize.define('lbtExport', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'exp_id',
    },
    name: {
      type: DataTypes.STRING,
      unique: false,
      field: 'exp_name',
    },
    query: {
      type: DataTypes.JSON,
      unique: false,
      field: 'exp_query',
    },
    shared: {
      type: DataTypes.BOOLEAN,
      unique: false,
      field: 'exp_shared_query',
    },
    u_id: {
      type: DataTypes.STRING,
      unique: false,
      field: 'exp_u_id',
    },
  }, {
    schema: 'lebontag',
    tableName: 'lbt_export',
    timestamps: false,
    underscored: true,
  });
  lbtExport.associate = function (models) {
    // associations can be defined here
    lbtExport.User = lbtExport.belongsTo(
      models['user'], {
        as: 'user',
        foreignKey: 'exp_u_id'
      }
    );
  };
  return lbtExport;
};