v1.0.3 (2024-10-21) **[DEV]**

- [PYTHON] lbt-get-adiff-* : -2 heures à end_date (appel de l'API avec un décalage de 2H pour s'assurer que l'API envoie des données à jour)
- [PYTHON] lbt-update-db-by-osc : correction de l'indentation sur le traitement des relations
- [PYTHON][SQL] Nouveaux paramètres osm_diff_nodes_attempts, osm_diff_ways_attempts et osm_diff_relations_attempts si aucune donnée n'est trouvée dans le OSM DIFF. Utilisés par les scripts OSM Diff pour incrémenter l'intervalle de 24H max de chargement des données (sécurité s'il n'y a vraiment pas de données sur 24H afin d'éviter une boucle infinie)
- [PYTHON] lbt-get-adiff-relations : suppression des enfants 'nd' dans les 'member' des relations dans les fragments OSC générés (incompatibles avec le format OSC et rejetés par Osmium)
- [PYTHON] lbt-update-db-by-osc : log les commandes osmium
- [PYTHON] pgrouting : lbt-load-data.py & lbt-update-db-by-osc.py : teste la valeur des tags maxspeed (doit être numérique) avant de l'envoyer dans la table pgr_ways
- [PYTHON] lbt-get-adiff-ways & lbt-get-adiff-relations : force l'état "géométrie modifiée" des objets modifiés si le nombre de nodes est différent entre l'ancienne et la nouvelle version

v1.0.0 (2024-06-18) **[RELEASE]**
- [PYTHON] lbt-get-adiff* : Empêche l'appel à l'API Overpass sur une période de plus de 24H
- [PYTHON] lbt-get-adiff* : si l'API Overpass OSM DIFF renvoie du HTML, refait une tentative
- [PYTHON] lbt-update-db-by-osc : osmconvert remplacé par osmium pour préparer les fichiers OSC avant le chargement en base (les nodes doivent être en premier dans le OSC) - ATTENTION nécessite `sudo apt-get install -y osmium-tool`

v0.23.1 (2024-04-15) **[DEV]**
- [PYTHON] lbt-get-adiff-relations.py : ajout de tests "if member_node_lat and member_node_lon" pour passer outre les <nd/> sans coordonnées dans les relations

v0.23.0 (2024-01-23) **[DEV]**
- [PYTHON] ATTENTION - Script "lbt-update-0.23.0.py" nécessaire pour les bases < v0.22.6 (sinon reboot de l'application à effectuer) : "python3 lbt-update-0.23.0.py"
- [PYTHON] lbt-update-db-by-osc : utilise les nodes des ways présents dans le fragment OSC (via lbt-get-adiff-ways v0.22.6) et récupère leurs éventuels tags dans la base de référence, et non pas dans osm_diff [nécessite un reboot ou d'exécuter le script "lbt-update-0.23.0.py"]

v0.22.6 (2024-01-22) **[DEV]**
- [PYTHON] lbt-get-adiff-ways : met tous les nodes des ways ajoutées et modifiées en mode modify dans le fragment OSC généré (pour avoir leur lat et lon à jour)
- [PYTHON] Ne charge pas dans osmdiff les nœuds créés ou modifiés qui n'ont aucun tag ou seulement le tag "created_by"
- [PYTHON] Ajout de l'heure dans le nom des fichiers téléchargés et des logs (permet d'appeler l'API 1 fois par heure au minimum)
- [PYTHON] lbt-load-data : Ajout de la classe CalledProcessError et de ses dépendances Python
- [PYTHON] lbt-update-db-by-osc : suppression de '--middle-schema='+str(db_ref_schema) incompatible avec d'anciennes versions de osm2pgsql
- [PYTHON] lbt-update-db-by-osc : changement du statut de l'objet dans la table lbt_validation après intégration
- [PYTHON] lbt-get-adiff-nodes : si un node modifié est présent dans la table _nodes mais pas _point, il reste à l'état modifié plutôt que forcé en créé

v0.22.5 (2024-01-17) **[RELEASE]**
- [MD] Ajout procédure de sauvegarde de l'historique pour le reboot dans le fichier REBOOT
- [PYTHON] lbt-load-data : suppression de '--middle-schema='+str(db_ref_schema) incompatible avec d'anciennes versions de osm2pgsql

v0.22.4 (2023-06-26) **[DEV]**
- [PYTHON] Gère les tags des nœuds sous-jacents associés à une validation

v0.22.3 (2023-01-30) **[DEV]**
- [SQL] Suppression des "TABLESPACE pg_default"
- [VUE] Rattachement : précision sur l'existence préalable du schéma de travail dans le formulaire
- [SQL] Suppression des "CREATE SCHEMA IF EXISTS" pour le rattachement (le schéma doit exister au préalable) car sinon erreur si le rôle n'a pas ce droit, même si le schéma existe
- [SQL] Renumérotation des fichiers d'update SQL

v0.22.2 (2023-01-13) **[DEV]**
- [VUE] Rattachement classique : charge les objets uniquement dans l'emprise affichée #114
- [VUE] Rattachement inverse : charge les objets uniquement dans l'emprise affichée #120
- [VUE] Rattachement : délai de 1500 ms lors du changement d'emprise avant de charger les objets

v0.22.1 (2022-12-14) **[DEV]**
- [VUE] Correction régression : N'affiche que les objets contenus dans l'emprise sélectionnée
- [VUE] N'affiche que les objets contenus dans l'emprise sélectionnée par défaut au premier chargement de la carte #135

v0.22.0 (2022-12-02) **[DEV]**
- [VUE] Serveurs de tuile passés en paramètre dans l'administration (nouvelle section 'Serveurs de tuiles')
- [VUE] Nouvelle section pour les flux WMTS #134
- [VUE] Gestion du mode Overlay des flux : ces flux se superposent aux autres via des cases à cocher dans la vue utilisateur #67

v0.21.9 (2022-11-14) **[DEV]**
- [PYTHON] Ajout de ST_MakeValid sur la geom de l'emprise pour éviter les erreurs de type "GEOSContains: TopologyException" lors du test ST_Within de l'objet OSM si l'emprise n'est pas valide
- [PYTHON] Ajout de ST_MakeValid sur la geom des emprises dans lbt-footprints-intersection.py

v0.21.8 (2022-10-31) **[DEV]**
- [PYTHON] Modification de la requête de découpe SQL (pgrouting) : prise en charge des polygones

v0.21.7 (2022-09-22) **[DEV]**
- [PYTHON] Modification de la requête de découpe SQL (pgrouting) : ajout 'highway=>construction' et 'highway=>proposed', suppression cycleway=* (highway=>cycleway suffit)

v0.21.6 (2022-09-02) **[DEV]**
- [PYTHON] Remplacement de "osm2pgrouting" par du SQL direct car la procédure de MAJ du fichier OSM intermédiaire crée des objets anormaux visibles dans pgrouting #138
- [PYTHON] Journalisation de la commande "osm2pgsql" et arrêt en cas d'erreur (via une nouvelle classe CalledProcessError) #140

v0.21.5 (2022-03-22) **[DEV]**
- [PYTHON] Ajout d'un tableau pour les géométries complexes "relationGeom"
- [VUE] [PYTHON] Option de validation automatique des groupes d'objets
- [VUE] Bouton de déclenchement manuel du calcul de la relation entre les objets et 1 emprise

v0.21.4 (2022-03-01) **[DEV]**

- [PYTHON] Truncate et remplissage quotidien de lbt_osmdiff_footprint
- [SQL] Prise en compte de old_geom des objets lors de l'intersection des objets avec les emprises
- [VUE][SQL] ST_Intersects asynchrone via une table de relations lbt_osmdiff_footprint (amélioration des performances des emprises)

v0.21.3 (2022-02-22) **[DEV]**

- [VUE] Activation/Désactivation rapide des groupes d'objets en Admin
- [VUE] Correction du formulaire de dates pour l'export #132
- [SQL] Opérateur != (différent) pris en charge dans la requête des Groupes d'objets (themeObject.js.translateQuery ; InputTagJSON.vue.translateQuery_ ; InputTagJSON.recursiveTranslateSQL_ ; lbtExport.js.translateQuery_) #129
- [VUE] Aide remplacée par un lien vers le wiki https://wiki.lebontag.fr
- [SQL] Désactive la compression de certaines colonnes géométriques pour accélérer les requêtes spatiales (performances)

v0.21.2 (2022-02-14) **[DEV]**

- [JS] Ne met pas à jour le JSON de l'emprise s'il n'a pas été modifié lors de l'édition d'une emprise
- [SQL] ST_CollectionExtract sur les emprises avant intersection (performances)

v0.21.1 (2022-02-07) **[DEV]**

- [VUE] Correction régression #128

v0.21.0 (2022-01-28) **[RELEASE]**

- [VUE] Gestion des GeoJSON complexes et simples
- [VUE] Ajout du nouveau module "Emprises" #106 #66
- [VUE] Liste déroulante des emprises sur la carte
- [SQL] Requêtes SQL d'interrogation des données optimisées
- [SQL] Nouvelles tables pour la gestion des emprises : 18_update_0.20.1_to_0.21.sql,02_lbt_tables.sql,03_lbt_data.sql
- [PYTHON] Récupère la geom de l'emprise par défaut : lbt-get-adiff-nodes.py,lbt-get-adiff-relations.py,lbt-get-adiff-ways.py,lbt-demo.py
- [VUE] Remplacement du paramètre "Emprise géographique" par une liste déroulante des emprises : Settings.vue
- [VUE] Utilisation de l'emprise par défaut comme emprise lors de l'affichage de la carte : OsmMap.vue

v0.20.1 (2021-12-02) **[RELEASE]**

- [CRON] Déclenchement de la validation automatique après osmdiff

v0.20.0 (2021-12-02) **[DEV]**

- [VUE] [PYTHON] [CRON] Validation automatique de contributeurs spécifiés #107
- [PYTHON] sys.exit(0) pour les sorties normales
- [PYTHON] Correction de l'exécution de pgrouting #124
- [VUE] Surveillance de la géométrie #44
- [VUE] Surveillance de tous les tags #105
- [VUE] EXPORT : saisie manuelle du calendrier #55
- [VUE] EXPORT : nom du fichier ZIP de l'export #56

v0.19.1 (2021-10-18) **[DEV]**

- [MD] Détails sur le paramètre "fichier POLY de découpe des données OSM de référence" dans la documentation

v0.19.0 (2021-10-07) **[DEV]**

- [VUE] Si rattachement classique suivi d'un rattachement inverse, n'insère pas les géométries inutilisées à la clôture du rattachement classique.

v0.18.0 (2021-09-06) **[RELEASE]**

- [CRON] Ajout de scripts Batch pour exécution des scripts Python sous Windows
- [PYTHON] Compatibilité Windows des scripts Python
- [EXE] Ajout d'exécutables Windows dans "/tools"
- [MD] Ajout du document "README-PROD-Windows.md" et autres fichiers suffixés par "-Linux"
- [HTML] Ajout de l'export des documents "README-XXX" en version HTML
- [SQL] Rattachement inverse : amélioration de la requête SQL de création
- [VUE] Suppression des "crédits" des financeurs en pages de login et d'accueil et mise en valeur dans la page "À propos"
- [MD] Mise à jour du fichier "README.MD"

v0.17.0 (2021-04-21) **[DEV]**

- [VUE] Multi-rattachement
- [VUE] Rattachement inverse

v0.16.0 (2021-03-29) **[RELEASE]**

- [MD] Ajout du document "REBOOT.md"
- [VUE] Changement de l'icône dans la modale de déconnexion
- [VUE] Accès au module rattachement interdit aux utilisateurs non "rattacheurs" ou "administrateurs"
- [VUE] Désactivation des modules depuis les paramètres admin, avec accès interdit via leur URL directe #86
- [VUE] Route vers le module validation renommée de /map à /validation
- [VUE] Ajout page d'accueil /home avec icônes des modules et actions
- [VUE] Remplacement des chemins relatifs des include par @
- [VUE] Simplification en gérant le menu en bas à droite dans un seul fichier Menu.vue
- [VUE] [PYTHON] [SQL] Affichage des images Mapillary dans le panneau d'information des objets si tag "mapillary" présent
- [PYTHON] Chargement des nodes modifiés : ne teste pas l'ancienne géométrie si le node n'existe pas dans la base de référence
- [PYTHON] Gestion des XML Overpass contenant un élément "remark" (cas d'erreur)
- [PYTHON] Gestion d'erreur après 6 requêtes Overpass échouées
- [PYTHON] Si rappel de l'API Overpass le même jour, end_date est la date du fichier gz déjà téléchargé, pas celle de l'exécution
- [VUE] Ajout d'un paramètre pour définir le maxsize de l'API Overpass depuis l'interface

v0.15.0 (2021-02-24) **[DEV]**

- [MD] Suppression de l'installation de Shapely via PIP
- [PYTHON] Remplacement de Shapely par du SQL dans les 3 scripts adiff (car problème de "segmentation fault" avec Shapely Ubuntu)
- [PYTHON] Ajout de [maxsize:2147483648] (256 MB) dans les appels à l'API Overpass #92
- [OSM] Ajout du fichier "DEP34_4326_20210211.poly" (Département 34) et d'un fichier lisez-moi

v0.14.0 (2020-12-14) **[RELEASE]**

- [VUE] EXPORT : correction de la projection (4326) pour l'export KML
- [VUE] EXPORT : nouveau paramètre pour nommer les fichiers et couches dans le ZIP
- [VUE] EXPORT : Support des groupes d'objets avec * #83
- [VUE] RATTACHEMENT : L'arbre ne se replit plus après rattachement d'un objet #77
- [VUE] Fenêtre de déconnexion #85
- [VUE] Affichage du nombre d'objets qu'il reste à rattacher. #82
- [VUE] La spécification du thème est obligatoire pour valider la création d'un groupe d'objets
- [VUE] Désactivation des "requêtes simples" dans la définition des groupes d'objets
- [CSS] Tags à valider surlignés en jaune
- [CSS] Bordure mauve autour des panneaux
- [CSS] Coloration du bouton de logout
- [VUE] Séparation du bouton de logout
- [VUE] Ajout balise "title" sur thèmes, groupes d'objets, filtres dans l'interface user
- [HTML] Ajout balise "title" sur icônes d'action (admin & user)
- [CSS] cursor:pointer sur boutons d'action
- [CSS] Coloration des groupes d'objets et des contributeurs dans l'admin
- [VUE] Fixation du menu en bas à gauche (suppression de "retour") et regroupement des CSS dans ActionLink.vue
- [CSS] Coins arrondis des fenêtres modales
- [CSS] Correction emplacement messages et coche verte/rouge des requêtes avancés pour les GO
- [CSS] Troncature et retour à la ligne des noms de thèmes ou GO longs dans le menu

v0.13.0 (2020-11-12) **[DEV]**

- [VUE] Cases de sélection des objets à valider #27
- [VUE] Affichage du nombre d'objets à valider #15
- [VUE] Limitation du nombre d'objets chargés par groupe #63
- [VUE] Gestion '*' dans les requêtes des groupes d'objets #64
- [VUE] Éditeur de requêtes en SQL pour les groupes d'objets #65

v0.12.2 (2020-10-26) **[DEV]**

- Ajout d'un paramètre de rattachement (champs communs filaire/table geom) pour la clôture de la session
- [PYTHON] Correction absence de "osm_polyfile" #70

v0.12.1 (2020-10-09) **[DEV]**

- Corrections et améliorations du module de rattachement

v0.12.0 (2020-09-21) **[DEV]**

- Ajout du module de "rattachement"

v0.11.2 (2020-06-21) **[DEV]**

- [PYTHON] Import des nodes hors emprise pour les ways #58
- [PYTHON] Ajout "--chunk 10000000" pour le chargement pgRouting

v0.11.0 (2020-06-01) **[RELEASE]**

- [VUE] Correction persistance d'objets pourtant validés
- [VUE] Correction validation groupée et validation partielle #25

v0.10.1 (2020-04-20) **[DEV]**

- [VUE] Correction "Fluw WMS" -> "Flux WMS"
- [PYTHON] Correction régression #57
- [VUE] Ajout des dates de dernière mise à jour des objets à valider dans "A propos"
- [PYTHON] Contrôle si l'objet est déjà dans la table lbt_osmdiff avant de le traiter
- [PYTHON] Nomme les fichiers et logs osmdiff avec la date sans l'heure
- [VUE] Filtre des objets à afficher a posteriori de la requête SQL (map.js & Map.vue) #52

v0.10.0 (2020-02-25) **[RELEASE]**

- [VUE] Ajout entrée Export et réorganisation du menu #8
- [CSS] Ajout "overflow-y:scroll" et "safe center" dans la fenêtre modale (Modal.vue) #46
- [VUE] Lien vers le profil OSM du contributeur dans le panneau d'informations (ObjectInfo.vue) #48
- [VUE] Gestion des OU et AND dans la définition avancée des groupes d'objets #13
- [VUE] Génération d'exports des groupes d'objets via ogr2ogr en GPKG, KML et SHP. #8
        **ogr2ogr doit être installé (apt-get install -y gdal-bin python-gdal)**


v0.9.1 (2020-02-07) **[DEV]**

- [SQL] Tri naturel des objets OSM à valider #42
- [CSS] Coin supérieur droit du panneau d'info arrondi (ObjectInfo.vue)
- [CSS] Légère transparence panneau d'info (ObjectInfo.vue)
- [DOC] Ajout CHANGELOG.md
- [VUE LEAFLET] URL des WMS Stamen en HTTPS (OsmMap.vue)
- [SQL] URL des WMS mundialis en HTTPS
- [VUE LEAFLET] Représentation des nodes avec fill semi-transparent comme sur www.openstreetmap.org (OsmMap.vue)
- [VUE LEAFLET] Modification de la représentation des géométries supprimées pour plus de lisibilité : dasharray plus fin, couleur rouge forcée, fill très transparent (OsmMap.vue) #33
- [VUE LEAFLET] En cas de dézoom important, point de localisation + rectangle supplémentaire (meilleurs visibilité et positionnement pour les longs objets comme les lignes de bus) (OsmMap.vue) 

 v0.9.0 (2020-02-05) **[RELEASE]**

- Première bêta publique
