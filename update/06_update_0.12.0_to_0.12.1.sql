UPDATE lebontag.lbt_setting SET s_value = '0.12.1' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20201009' WHERE s_name = 'app_release_date';

ALTER TABLE lebontag.lbt_rattachment_session ADD COLUMN rtsession_tolerance DECIMAL;
ALTER TABLE lebontag.lbt_rattachment_session ADD COLUMN rtsession_schema_temp TEXT;
