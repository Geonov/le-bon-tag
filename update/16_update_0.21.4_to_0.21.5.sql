UPDATE lebontag.lbt_setting SET s_value = '0.21.5' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20220322' WHERE s_name = 'app_release_date';

-- Booléen pour la fonction de validation automatique des groupes d'objets
ALTER TABLE lebontag.lbt_objectgroup ADD COLUMN og_auto BOOLEAN default FALSE;
