UPDATE lebontag.lbt_setting SET s_value = '0.17.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20210421' WHERE s_name = 'app_release_date';

ALTER TABLE lebontag.lbt_rattachment_session ADD COLUMN rtsession_isreverse boolean;
ALTER TABLE lebontag.lbt_rattachment_session ADD COLUMN rtsession_tablefil_where text;
UPDATE lebontag.lbt_rattachment_session SET rtsession_isreverse=false;
