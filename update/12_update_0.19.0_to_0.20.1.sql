UPDATE lebontag.lbt_setting SET s_value = '0.20.1' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20210202' WHERE s_name = 'app_release_date';

ALTER TABLE lebontag.lbt_right ADD COLUMN r_verif_geom boolean;
UPDATE lebontag.lbt_right SET r_verif_geom=true;

ALTER TABLE lebontag.lbt_right ADD COLUMN r_verif_all_tags boolean;
UPDATE lebontag.lbt_right SET r_verif_all_tags=false;

ALTER TABLE lebontag.lbt_contributor ADD COLUMN ct_auto_validation boolean;
UPDATE lebontag.lbt_contributor SET ct_auto_validation=false;
