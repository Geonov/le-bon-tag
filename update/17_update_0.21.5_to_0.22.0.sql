UPDATE lebontag.lbt_setting SET s_value = '0.22.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20221202' WHERE s_name = 'app_release_date';

ALTER TABLE lebontag.lbt_wms ADD wms_type TEXT;
UPDATE lebontag.lbt_wms SET wms_type = 'base';
ALTER TABLE lebontag.lbt_wms ALTER COLUMN wms_type SET NOT NULL;

INSERT INTO lebontag.lbt_wms (wms_name,wms_url,wms_layer,wms_format,wms_attribution,wms_maxzoom,wms_alpha,wms_type) VALUES 
('Topographie 450m','https://ows.mundialis.de/services/service?','TOPO-WMS','image/png','Mundialis',20,false,'base'),
('Topographie 450m avec toponymie','https://ows.mundialis.de/services/service?','TOPO-OSM-WMS','image/png','Mundialis',20,false,'base');

CREATE TABLE lebontag.lbt_wmts
(
	wmts_id serial PRIMARY KEY,
	wmts_name TEXT UNIQUE NOT NULL,
	wmts_url TEXT NOT NULL,
	wmts_layer TEXT NOT NULL,
	wmts_format TEXT NOT NULL,
	wmts_matrix_set TEXT NOT NULL,
	wmts_style TEXT NOT NULL,
	wmts_attribution TEXT,
	wmts_maxzoom INTEGER,
	wmts_type TEXT NOT NULL,
	wmts_protocol TEXT NOT NULL
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_wmts OWNER TO osm;

CREATE INDEX wmts_id_idx ON lebontag.lbt_wmts USING btree (wmts_id);
CREATE INDEX wmts_name_idx ON lebontag.lbt_wmts USING btree (wmts_name);

INSERT INTO lebontag.lbt_wmts (wmts_name,wmts_url,wmts_layer,wmts_format,wmts_matrix_set,wmts_style,wmts_attribution,wmts_maxzoom,wmts_type,wmts_protocol) VALUES 
('IGN - Cadastre PCI vecteur','https://wxs.ign.fr/essentiels/geoportail/wmts?SERVICE=WMTS','CADASTRALPARCELS.PARCELLAIRE_EXPRESS','image/png','PM','PCI vecteur','IGN-F/Geoportail',NULL,'overlay','url'),
('IGN - Orthophotos','https://wxs.ign.fr/essentiels/geoportail/wmts?SERVICE=WMTS','ORTHOIMAGERY.ORTHOPHOTOS','image/jpeg','PM','normal','IGN-F/Geoportail',NULL,'base','url'),
('IGN - Plan','https://wxs.ign.fr/essentiels/geoportail/wmts?SERVICE=WMTS','GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2','image/png','PM','normal','IGN-F/Geoportail',NULL,'base','url'),
('USGSHydroCached','https://basemap.nationalmap.gov/arcgis/rest/services/USGSHydroCached/MapServer/WMTS','USGSHydroCached','image/jpgpng','default028mm','default','USGS',8,'base','rest');

CREATE TABLE lebontag.lbt_tile
(
	tile_id serial PRIMARY KEY,
	tile_name TEXT UNIQUE NOT NULL,
	tile_url TEXT NOT NULL,
	tile_layer TEXT NOT NULL,
	tile_format TEXT NOT NULL,
	tile_attribution TEXT,
	tile_maxzoom INTEGER,
	tile_alpha BOOLEAN,
	tile_visible BOOLEAN
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_tile OWNER TO osm;

CREATE INDEX tile_id_idx ON lebontag.lbt_tile USING btree (tile_id);
CREATE INDEX tile_name_idx ON lebontag.lbt_tile USING btree (tile_name);

INSERT INTO lebontag.lbt_tile (tile_name,tile_url,tile_layer,tile_format,tile_attribution,tile_maxzoom,tile_alpha,tile_visible) VALUES
('Stamen (watercolor)','https://stamen-tiles-{s}.a.ssl.fastly.net','watercolor','image/jpg','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC BY 3.0 par Stamen Design',18,false,false),
('Stamen (toner)','https://stamen-tiles-{s}.a.ssl.fastly.net','toner-lite','image/png','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC BY 3.0 par Stamen Design',19,false,false),
('Stamen (terrain)','https://stamen-tiles-{s}.a.ssl.fastly.net','terrain','image/jpg','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC BY 3.0 par Stamen Design',18,false,false),
('OpenTopoMap','https://{s}.tile.opentopomap.org/','','image/png','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC-BY-SA',16,false,false),
('OpenStreetMap France','https://tilesfr.lebontag.fr/','osmfr','image/png','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC-BY-SA',19,false,false),
('OpenStreetMap','https://tiles.lebontag.fr','','image/png','Données cartographiques par les contributeurs © OpenStreetMap, carte sous licence CC-BY-SA',19,false,true);
