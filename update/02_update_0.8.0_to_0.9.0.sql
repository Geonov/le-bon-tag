UPDATE lebontag.lbt_setting SET s_value = '0.9.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20200205' WHERE s_name = 'app_release_date';
DELETE FROM lebontag.lbt_setting WHERE s_name IN ('db_ref_date','osm_diffdata_date','ldap_tls_cert');
INSERT INTO lebontag.lbt_setting (s_name,s_value) VALUES
 ('osm_diff_nodes_date',''),
 ('osm_diff_ways_date',''),
 ('osm_diff_relations_date',''),
 ('osm_diff_nodes_enable','1'),
 ('osm_diff_ways_enable','1'),
 ('osm_diff_relations_enable','1'),
 ('objects_limit','15'),
 ('ldap_cert_validity','true');

ALTER TABLE lebontag.lbt_osmdiff ADD COLUMN od_id_members BIGINT[];
ALTER TABLE lebontag.lbt_osmdiff_history ADD COLUMN od_id_members BIGINT[];

ALTER TABLE lebontag.lbt_osmdiff ADD COLUMN od_extent text;
ALTER TABLE lebontag.lbt_osmdiff_history ADD COLUMN od_extent text;

UPDATE lebontag.lbt_osmdiff od
SET od_extent = sub.extent_text
FROM
(
WITH extent_req AS ( SELECT od_id, ST_Extent(ST_Collect(od_new_geom,od_old_geom)) AS geom FROM lebontag.lbt_osmdiff GROUP BY od_id)
SELECT od_id, CONCAT_WS(';',ST_XMin(geom), ST_YMin(geom), ST_XMax(geom), ST_YMax(geom)) AS extent_text
FROM extent_req
) AS sub
WHERE od.od_id = sub.od_id;

ALTER TABLE lebontag.lbt_osmdiff DROP COLUMN od_new_geom_center, DROP COLUMN od_old_geom_center;
ALTER TABLE lebontag.lbt_osmdiff_history DROP COLUMN od_new_geom_center, DROP COLUMN od_old_geom_center;

CREATE INDEX osmdiff_id_version_idx ON lebontag.lbt_osmdiff USING btree (od_id,od_new_version);
CREATE INDEX osmdiffhist_id_version_idx ON lebontag.lbt_osmdiff USING btree (od_id,od_new_version);
