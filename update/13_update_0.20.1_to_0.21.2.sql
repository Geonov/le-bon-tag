UPDATE lebontag.lbt_setting SET s_value = '0.21.2' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20220214' WHERE s_name = 'app_release_date';

-- Table lbt_footprint

CREATE TABLE lebontag.lbt_footprint
(
	fp_id SERIAL PRIMARY KEY,
	fp_name TEXT UNIQUE NOT NULL,
	fp_geom GEOMETRY NOT NULL,
	CONSTRAINT enforce_dims_fp_geom CHECK (st_ndims(fp_geom) = 2),
	CONSTRAINT enforce_srid_fp_geom CHECK (st_srid(fp_geom) = 4326)
)
WITH ( OIDS = FALSE );

ALTER TABLE lebontag.lbt_footprint OWNER TO osm;

CREATE INDEX footprint_id_idx ON lebontag.lbt_footprint USING btree (fp_id);
CREATE INDEX footprint_geom_idx ON lebontag.lbt_footprint USING gist (fp_geom);

-- Table lbt_user_footprint

CREATE TABLE lebontag.lbt_user_footprint
(
	u_id INTEGER references lebontag.lbt_user(u_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	fp_id INTEGER references lebontag.lbt_footprint(fp_id) ON DELETE CASCADE ON UPDATE RESTRICT,
	UNIQUE (u_id,fp_id)
);

ALTER TABLE lebontag.lbt_user_footprint OWNER TO osm;

CREATE INDEX user_footprint_uid_idx ON lebontag.lbt_user_footprint USING btree (u_id);
CREATE INDEX user_footprint_fpid_idx ON lebontag.lbt_user_footprint USING btree (fp_id);

-- Insertion de l'emprise par défaut GeoJSON dans la table lbt_footprint

INSERT INTO lebontag.lbt_footprint (fp_name,fp_geom) VALUES ('Emprise par défaut',(select ST_GeomFromGeoJSON(s_value) from lebontag.lbt_setting where s_name = 'osm_footprint'));

-- Déclaration de l'emprise par défaut

UPDATE lebontag.lbt_setting SET s_value = (SELECT fp_id FROM lebontag.lbt_footprint WHERE fp_name = 'Emprise par défaut') WHERE s_name = 'osm_footprint';
