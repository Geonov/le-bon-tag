UPDATE lebontag.lbt_setting SET s_value = '0.21.3' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20220222' WHERE s_name = 'app_release_date';

-- Désactive la compression de certaines géométries pour accélérer les requêtes spatiales
ALTER TABLE lebontag.lbt_osmdiff ALTER COLUMN od_new_geom SET STORAGE EXTERNAL;
UPDATE lebontag.lbt_osmdiff SET od_new_geom = ST_SetSRID(od_new_geom, 4326);
ALTER TABLE lebontag.lbt_footprint ALTER COLUMN fp_geom SET STORAGE EXTERNAL;
UPDATE lebontag.lbt_footprint SET fp_geom = ST_SetSRID(fp_geom, 4326);

-- Booléen pour la fonction de désactivation des groupes d'objets
ALTER TABLE lebontag.lbt_objectgroup ADD COLUMN og_active BOOLEAN default TRUE;
