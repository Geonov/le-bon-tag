UPDATE lebontag.lbt_setting SET s_value = '0.10.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '202002025' WHERE s_name = 'app_release_date';
UPDATE lebontag.lbt_wms SET wms_url = 'https://ows.mundialis.de/services/service?' WHERE wms_name = 'Topographie 450m';
UPDATE lebontag.lbt_wms SET wms_url = 'https://ows.mundialis.de/services/service?' WHERE wms_name = 'Topographie 450m avec toponymie';

CREATE TABLE lebontag.lbt_export
(
	exp_id SERIAL PRIMARY KEY,
	exp_name TEXT NOT NULL,
	exp_query JSON,
	exp_shared_query BOOLEAN DEFAULT 'FALSE',
	exp_u_id INTEGER NOT NULL REFERENCES lebontag.lbt_user(u_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	UNIQUE (exp_name,exp_u_id)
)
WITH ( OIDS = FALSE );
ALTER TABLE lebontag.lbt_export OWNER TO osm;
CREATE INDEX export_id_idx ON lebontag.lbt_export USING btree (exp_id);

ALTER TABLE lebontag.lbt_objectgroup ADD COLUMN og_query_json text;

UPDATE lebontag.lbt_objectgroup SET og_query_json = '{"[op.and]":[{"amenity":"bicycle_parking"}]}' WHERE og_name = 'Arceaux vélo';
UPDATE lebontag.lbt_objectgroup SET og_query_json = null WHERE og_name = 'Bande cyclable';
UPDATE lebontag.lbt_objectgroup SET og_query_json = '{"[op.and]":[{"highway":"cycleway"}]}' WHERE og_name = 'Piste cyclable';
UPDATE lebontag.lbt_objectgroup SET og_query_json = null WHERE og_name = 'Filaire';
UPDATE lebontag.lbt_objectgroup SET og_query_json = '{"[op.and]":[{"school:FR":"primaire"}]}' WHERE og_name = 'Écoles primaires';
UPDATE lebontag.lbt_objectgroup SET og_query_json = '{"[op.and]":[{"type":"route"},{"route":"bus"}]}' WHERE og_name = 'Lignes de bus';

UPDATE lebontag.lbt_contributor SET ct_name = 'Mattéo Delabre' WHERE ct_id = 3233506;
UPDATE lebontag.lbt_contributor SET ct_name = 'Bruno Adelé' WHERE ct_id = 2461;
