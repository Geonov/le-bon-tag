UPDATE lebontag.lbt_setting SET s_value = '0.19.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20211007' WHERE s_name = 'app_release_date';

ALTER TABLE lebontag.lbt_rattachment_session ADD COLUMN rtsession_isfollowedbyreverse boolean;
UPDATE lebontag.lbt_rattachment_session SET rtsession_isfollowedbyreverse=true;
