UPDATE lebontag.lbt_setting SET s_value = '0.11.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20200601' WHERE s_name = 'app_release_date';

-- Fonction qui échappe tous les caractères spéciaux des regex
-- Exemple : select escape_regex('+33698563257');
DROP FUNCTION IF EXISTS lebontag.escape_regex(text);
CREATE FUNCTION lebontag.escape_regex(text)
RETURNS TEXT AS $$
DECLARE 
	special_chars varchar[] := array['.', '*', '+', '?', '^', '$', '{', '}', '(', ')', '|', '[', ']'];
	x text;
BEGIN
  FOREACH x IN ARRAY special_chars
  LOOP
    $1 = replace($1, x, '\' || x);
  END LOOP;
  RETURN $1;
END;
$$ LANGUAGE plpgsql;
