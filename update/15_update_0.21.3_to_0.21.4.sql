UPDATE lebontag.lbt_setting SET s_value = '0.21.4' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20220301' WHERE s_name = 'app_release_date';

-- Table lbt_osmdiff_footprint

CREATE TABLE lebontag.lbt_osmdiff_footprint
(
	od_id BIGINT,
	od_new_version INTEGER,
	fp_id INTEGER,
	UNIQUE (od_id,od_new_version,fp_id)
);

ALTER TABLE lebontag.lbt_osmdiff_footprint ADD CONSTRAINT lbt_osmdiff_footprint_odidodnew_fkey FOREIGN KEY (od_id,od_new_version) REFERENCES lebontag.lbt_osmdiff(od_id,od_new_version) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE lebontag.lbt_osmdiff_footprint ADD CONSTRAINT lbt_osmdiff_footprint_fpid_fkey FOREIGN KEY (fp_id) REFERENCES lebontag.lbt_footprint(fp_id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE lebontag.lbt_osmdiff_footprint OWNER TO osm;

WITH footprints AS ( SELECT fp_id,(st_dump(ST_CollectionExtract(fp_geom,3))).geom FROM lebontag.lbt_footprint ) INSERT INTO lebontag.lbt_osmdiff_footprint (od_id,od_new_version,fp_id) SELECT d.od_id,d.od_new_version,f.fp_id FROM lebontag.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_new_geom, f.geom) WHERE d.od_new_geom IS NOT NULL UNION SELECT d.od_id,d.od_new_version,f.fp_id FROM lebontag.lbt_osmdiff AS d INNER JOIN footprints AS f ON ST_Intersects(d.od_old_geom, f.geom) WHERE d.od_old_geom IS NOT NULL;

CREATE INDEX osmdiff_footprint_odidv_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id,od_new_version);
CREATE INDEX osmdiff_footprint_odid_idx ON lebontag.lbt_osmdiff_footprint USING btree (od_id);
CREATE INDEX osmdiff_footprint_fpid_idx ON lebontag.lbt_osmdiff_footprint USING btree (fp_id);
