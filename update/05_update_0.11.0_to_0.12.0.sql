UPDATE lebontag.lbt_setting SET s_value = '0.12.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20200921' WHERE s_name = 'app_release_date';

-- Nouveau rôle
INSERT INTO lebontag.lbt_userrole (ur_name,ur_desc_fr) VALUES ('rattacher','Rattacheur');

-- Table lebontag.lbt_rattachment_session
CREATE TABLE lebontag.lbt_rattachment_session
(
	rtsession_id SERIAL PRIMARY KEY,
	rtsession_name TEXT UNIQUE NOT NULL,
	rtsession_host TEXT NOT NULL,
	rtsession_port INTEGER NOT NULL,
	rtsession_db TEXT NOT NULL,
	rtsession_user TEXT,
	rtsession_password TEXT,
	rtsession_tablefil TEXT NOT NULL,
	rtsession_tableattr TEXT NOT NULL,
	rtsession_tablegeom TEXT NOT NULL,
	rtsession_fid TEXT NOT NULL,
	rtsession_fgeom TEXT NOT NULL,
	rtsession_flist TEXT NOT NULL,
	rtsession_epsg INTEGER NOT NULL,
	rtsession_tableattr_temp TEXT NOT NULL,
	rtsession_tablegeom_temp TEXT NOT NULL,
	rtsession_startdate TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	rtsession_enddate TIMESTAMP WITH TIME ZONE,
	rtsession_close BOOLEAN DEFAULT 'FALSE'
)
WITH ( OIDS = FALSE );
ALTER TABLE lebontag.lbt_rattachment_session OWNER TO osm;
CREATE INDEX rattachment_id_idx ON lebontag.lbt_rattachment_session USING btree (rtsession_id);
