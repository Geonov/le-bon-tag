UPDATE lebontag.lbt_setting SET s_value = '0.16.0' WHERE s_name = 'app_build';
UPDATE lebontag.lbt_setting SET s_value = '20210329' WHERE s_name = 'app_release_date';

INSERT INTO lebontag.lbt_setting (s_name,s_value) VALUES ('api_max_size','2147483648');
INSERT INTO lebontag.lbt_setting (s_name,s_value) VALUES ('mod_validation_enable','1');
INSERT INTO lebontag.lbt_setting (s_name,s_value) VALUES ('mod_export_enable','1');
INSERT INTO lebontag.lbt_setting (s_name,s_value) VALUES ('mod_rattachment_enable','1');

ALTER TABLE lebontag.lbt_osmdiff ADD COLUMN od_mapillary text;
ALTER TABLE lebontag.lbt_osmdiff_history ADD COLUMN od_mapillary text;
UPDATE lebontag.lbt_osmdiff SET od_mapillary = replace(od_new_tags -> 'mapillary','https://www.mapillary.com/map/im/','');
UPDATE lebontag.lbt_osmdiff_history SET od_mapillary = replace(od_new_tags -> 'mapillary','https://www.mapillary.com/map/im/','');
